<?php
/**
 * Created by PhpStorm.
 * User: 刘海明（GadflyBSD@gmail.com）
 * Date: 2014/12/16
 * Time: 15:58
 */

class Weixin {
	/* 消息类型常量 */
	const MSG_TYPE_TEXT     = 'text';
	const MSG_TYPE_IMAGE    = 'image';
	const MSG_TYPE_VOICE    = 'voice';
	const MSG_TYPE_VIDEO    = 'video';
	const MSG_TYPE_MUSIC    = 'music';
	const MSG_TYPE_NEWS     = 'news';
	const MSG_TYPE_LOCATION = 'location';
	const MSG_TYPE_LINK     = 'link';
	const MSG_TYPE_EVENT    = 'event';

	/* 事件类型常量 */
	const MSG_EVENT_SUBSCRIBE         = 'subscribe';
	const MSG_EVENT_SCAN              = 'SCAN';
	const MSG_EVENT_LOCATION          = 'LOCATION';
	const MSG_EVENT_CLICK             = 'CLICK';
	const MSG_EVENT_MASSSENDJOBFINISH = 'MASSSENDJOBFINISH';

	protected $conf;
	protected $token;      // 获取到的access_token
	protected $appid;      // 微信开发者申请的appID
	protected $secret;     // 微信开发者申请的appSecret
	protected $url, $curl, $post;
	public $data = array();  // 微信推送过来的数据

	public function Weixin($conf = 'WEIXIN', $curl = true){
		$this->conf = $conf;
		$this->token = C($conf.'.token');
		$this->appid = C($conf.'.appid');
		$this->secret = C($conf.'.appsecret');
		$this->curl = ($curl)?'Curl':'Snoopy';
	}

	/**
	 * 获得微信JS-SDK接口注入权限验证配置信息
	 * @param $url
	 *
	 * @return array
	 */
	public function  get_wxconfig($url){
		$return = array(
			'appId'     => $this->appid,
			'timestamp' => time(),
			'nonceStr'  => randstr(16, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),
			'ticket'    => $this->get_jsapi_ticket(false),
		);
		$signature = $this->get_signature($return['ticket'], $return['timestamp'], $return['nonceStr'], $url);
		$return['signature'] = $signature['signature'];
		$return['sha'] = $signature['sha'];
		return $return;
	}

	public function debug(){
		return array(
			'token'     => $this->token,
			'access'    => $this->access_token(),
			'appid'     => $this->appid,
			'secret'    => $this->secret,
			'curl'      => $this->curl,
			'url'       => $this->url,
			'post'      => $this->post,
			'data'      => $this->data,
			'conf'      => $this->conf,
			'info'      => session('weixinInfo'),
		);
	}

	/**
	 * 获得缓存过的微信签名认证access_token,
	 * @param bool $cache   是否缓存数据,
	 * @param bool $clean   是否清理缓存数据
	 *
	 * @return mixed|string|void    返回微信签名认证access_token(字符串类型),获取失败时返回一个错误数组
	 */
	public function access_token($cache = true, $clean = false){
		if($clean) S($this->conf.'AccessToken', null);
		if($cache){
			if(!S($this->conf.'AccessToken')){
				$data = $this->get_access_token();
				if($data['access_token']){
					S($this->conf.'AccessToken', $data['access_token'], $data['expires_in']);
					return S($this->conf.'AccessToken');
				}else{
					return $data;
				}
			}else{
				return S($this->conf.'AccessToken');
			}
		}else{
			return $this->get_access_token();
		}
	}

	/**
	 * 验证开发者微信服务器URL有效性
	 * @throws Exception
	 */
	public function valid(){
		if($this->checkSignature()){
			echo $_GET['echostr'];
		}
	}

	/**
	 * 获得微信版本号
	 * @return string
	 */
	public function getWeixinVerion(){
		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		preg_match("/micromessenger\/(\d+\.\d+\.\d+\.\d+)/",$agent,$f);
		if($f){
			return $f[1];
		}else{
			preg_match("/micromessenger\/(\d+\.\d+\.\d+)/",$agent,$f);
			if($f){
				return $f[1].'.00';
			}else{
				preg_match("/micromessenger\/(\d+\.\d+)/",$agent,$f);
				if($f){
					return $f[1].'.0.00';
				}
			}
		}
		return '6.0.0.00';
	}

	/**
	 * 验证浏览器是否是微信浏览器
	 * @return bool
	 */
	public function isWechat(){
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') === false) return false;
		else return true;
	}

	/**
	 * 获取微信推送的数据
	 * @return array 转换为数组后的数据
	 */
	public function request(){
		$xml = file_get_contents("php://input");
		$xml = new SimpleXMLElement($xml);
		$xml || exit;
		foreach($xml as $key => $value){
			$this->data[$key] = strval($value);
		}
		return $this->data;
	}

	/**
	 * * 响应微信发送的信息（自动回复）
	 * @param  array  $content 回复信息，文本信息为string类型
	 * @param  string $type    消息类型
	 */
	public function response($content, $type = self::MSG_TYPE_TEXT){
		/* 基础数据 */
		$data = array(
			'ToUserName'   => $this->data['FromUserName'],
			'FromUserName' => $this->data['ToUserName'],
			'CreateTime'   => time(),
			'MsgType'      => $type,
		);

		/* 按类型添加额外数据 */
		//$this->post = array_merge($message, array(strtolower($type) => call_user_func(array(self, $type), $content)));
		$content = call_user_func(array(self, $type), $content);
		if($type == self::MSG_TYPE_TEXT || $type == self::MSG_TYPE_NEWS){
			$data = array_merge($data, $content);
		} else {
			$data[ucfirst($type)] = $content;
		}
		//Log::write('接收到的Resp数据类型：'.json_encode($data), Log::DEBUG);

		/* 转换数据为XML */
		$xml = new SimpleXMLElement('<xml></xml>');
		self::data2xml($xml, $data);
		exit($xml->asXML());
	}

	public function push_message($openid, $content, $type = self::MSG_TYPE_TEXT){
		$data = array(
			'ToUserName'   => $openid,
			'FromUserName' => 'System 系统发送',
			'CreateTime'   => time(),
			'MsgType'      => $type,
		);
		$content = call_user_func(array(self, $type), $content);
		if($type == self::MSG_TYPE_TEXT || $type == self::MSG_TYPE_NEWS){
			$data = array_merge($data, $content);
		} else {
			$data[ucfirst($type)] = $content;
		}
		$xml = new SimpleXMLElement('<xml></xml>');
		self::data2xml($xml, $data);
		exit($xml->asXML());
	}

	public function custom_button($type, $option){
		$access_token = $this->access_token(true, true);
		switch($type){
			case 'create':
				$this->url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token='.$access_token;
				$this->post = $option;
				return json_decode($this->CurlData($this->url, json_encode_plus($this->post), 'POST'), true);
				break;
			case 'get':
				$this->url = 'https://api.weixin.qq.com/cgi-bin/menu/get';
				$this->post = 'access_token='.$access_token;
				return json_decode($this->CurlData($this->url, $this->post), true);
			case 'delete':
				$this->url = 'https://api.weixin.qq.com/cgi-bin/menu/delete';
				$this->post = 'access_token='.$access_token;
				return json_decode($this->CurlData($this->url, $this->post), true);
			case 'selfmenu':
				$this->url = 'https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info';
				$this->post = 'access_token='.$access_token;
				return json_decode($this->CurlData($this->url, $this->post), true);
		}

	}

	/**
	 * 获取第三方网页授权页面链接
	 * @param string $redirect  //第三方网页授权后重定向的回调链接地址
	 * @param string $scope     //应用授权作用域，默认是：snsapi_userinfo
	 *                      snsapi_base: 不弹出授权页面，直接跳转，只能获取用户openid；
	 *                      snsapi_userinfo：弹出授权页面，可通过openid拿到昵称、性别、所在地。
	 * @param string $state     //重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值
	 * @return string           //通过微信浏览器访问该地址，如果用户同意授权，页面将跳转至 redirect/?code=CODE&state=STATE。
	 */
	public function get_auth2_url($redirect, $scope = 'snsapi_userinfo', $state='STATE'){
		$this->url = 'https://open.weixin.qq.com/connect/oauth2/authorize';
		$this->post = 'appid='.$this->appid.'&redirect_uri='.urlencode($redirect).'&response_type=code&scope='.$scope.'&state='.$state.'#wechat_redirect';
		return $this->url.'?'.$this->post;
	}

	public function get_auth2_userinfo($code, $session='weixin', $refresh=false){
		$weixin = session($session);
		if(!session('?'.$session) || !$weixin['access_token'] || $refresh){
			$token = $this->get_auth2_access_token($code);
			$return = array_merge($token, array('userinfo' => $this->pull_auth2_userinfo($token['access_token'], $token['openid'])));
			return $return;
		}else{
			return $weixin;
		}
	}

	/**
	 * 第三方页面授权后通过code换取网页授权access_token
	 * @param $code     //填写访问$this->get_auth2_url()所获取的code参数
	 * @return mixed    //正确时返回的数据包,数据结构如下：
	 *      array(
	 *          'access_token'  => "ACCESS_TOKEN",  // 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 *          'expires_in'    => 7200,            // access_token接口调用凭证超时时间，单位（秒）
	 *          'refresh_token' => "REFRESH_TOKEN", // 用户刷新access_token
	 *          'openid'        => "OPENID",        // 用户唯一标识
	 *          'scope'         => "SCOPE"          // 用户授权的作用域，使用逗号（,）分隔
	 *      );
	 */
	private function get_auth2_access_token($code){
		$this->url = 'https://api.weixin.qq.com/sns/oauth2/access_token';
		$this->post = 'appid='.$this->appid.'&secret='.$this->secret.'&code='.$code.'&grant_type=authorization_code';
		return json_decode($this->CurlData($this->url, $this->post), true);
	}

	/**
	 * 第三方页面授权后刷新access_token
	 * @param $access_token    // 填写通过access_token获取到的refresh_token参数
	 * @return mixed            // 正确时返回的数据结构如下：
	 *      array(
	 *          'access_token'  => "ACCESS_TOKEN",  // 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 *          'expires_in'    => 7200,            // access_token接口调用凭证超时时间，单位（秒）
	 *          'refresh_token' => "REFRESH_TOKEN", // 用户刷新access_token
	 *          'openid'        => "OPENID",        // 用户唯一标识
	 *          'scope'         => "SCOPE"          // 用户授权的作用域，使用逗号（,）分隔
	 *      );
	 */
	private function get_auth2_refresh_token($access_token){
		$this->url = 'https://api.weixin.qq.com/sns/oauth2/refresh_token';
		$this->post = 'appid='.$this->appid.'&grant_type=refresh_token&refresh_token='.$access_token;
		return json_decode($this->CurlData($this->url, $this->post), true);
	}

	/**
	 * 第三方页面检验授权凭证（access_token）是否有效
	 * @param $access_token     //网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 * @param $openid           //用户的唯一标识
	 *
	 * @return mixed            ////正确时返回的数据包,数据结构如下：{ "errcode":0,"errmsg":"ok"}
	 */
	private function check_access_token($access_token, $openid){
		$url = 'https://api.weixin.qq.com/sns/auth';
		$data = 'access_token='.$access_token.'&openid='.$openid;
		return json_decode($this->CurlData($url, $data), true);
	}

	/**
	 * 如果网页授权作用域为snsapi_userinfo，则可以通过openid拉取用户信息
	 * @param $openid   // 用户唯一标识
	 * @return mixed    // 正确时返回的数据结构如下：
	 *      array(
	 *          'openid'    => "OPENID",    // 用户的唯一标识
	 *          'nickname'  => "NICKNAME",  // 用户昵称
	 *          'sex'       => 1,           // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	 *          'province'  => "PROVINCE",  // 用户个人资料填写的省份
	 *          'city'      => "CITY"       // 普通用户个人资料填写的城市
	 *          'country'   => "COUNTRY"    // 国家，如中国为CN
	 *          'headimgurl'=> "HEADIMGURL",// 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
	 *          'privilege  => [privilege1, privilege2]     // 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	 *          'unionid'   => 'o6_bmasdasdsad6_2sgVt7hMZOPfL'  // 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	 *      );
	 */
	private function pull_auth2_userinfo($access, $openid){
		$this->url = 'https://api.weixin.qq.com/sns/userinfo';
		$this->post = 'access_token='.$access.'&openid='.$openid.'&lang=zh_CN';
		return json_decode($this->CurlData($this->url, $this->post),true);
	}

	/**
	 * 获取关注者列表
	 * @return mixed    // 正确时返回的数据结构如下：
	 */
	public function get_oponid_list(){
		$this->url = 'https://api.weixin.qq.com/cgi-bin/user/get';
		$this->post = 'access_token='.$this->access_token();
		return json_decode($this->CurlData($this->url, $this->post),true);
	}

	/**
	 * 获取用户基本信息(UnionID机制)
	 * @param $openid
	 * @return mixed    // 正确时返回的数据结构如下：
	 */
	public function get_openid_info($openid){
		$this->url = 'https://api.weixin.qq.com/cgi-bin/user/info';
		$this->post = 'access_token='.$this->access_token().'&openid='.$openid.'&lang=zh_CN';
		return json_decode($this->CurlData($this->url, $this->post),true);
	}

	/**
	 * 向指定用户发送消息
	 * @param        $openid
	 * @param        $content
	 * @param string $type
	 */
	public function send_message($openid, $content, $type = self::MSG_TYPE_TEXT){
		$this->url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token='.$this->access_token();
		$message = array(
			'touser'    => $openid,
			'msgtype'   => $type,
		);
		/* 按类型添加额外数据 */
		//$content = call_user_func(array(self, $type), $content);

		if($type == self::MSG_TYPE_TEXT || $type == self::MSG_TYPE_NEWS){
			//$this->post = array_merge($message, $content);
			$this->post = array_merge($message, array(strtolower($type) => call_user_func(array(self, $type), $content)));
		} else {
			$this->post = array_merge($message, $content);
		}
		return $this->CurlData($this->url, json_encode_plus($this->post), 'POST');
	}

	/**
	 * 发送模板消息前通过模板库中模板的编号获得消息模板ID
	 * @param $template_id_short    模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
	 *
	 * @return mixed    返回JSON数据包, 正常时的返回JSON数据包示例：{"errcode":0, "errmsg":"ok", "template_id":"Doclyl5uP7Aciu-qZ7mJNPtWkbkYnWBWVja26EGbNyk"}
	 */
	private function get_msg_template_id($template_id_short){
		$url = 'https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token='.$this->access_token();
		return json_decode($this->CurlData($url, json_encode_plus(array('template_id_short' => $template_id_short)), 'POST'), true);
	}

	/**
	 * 向指定用户发送模板消息
	 * @param $openid               指定用户的openid
	 * @param $template_id_short    模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
	 * @param $data                 发送数据, JSON数据格式如下:
	 *                                  {"first": {"value":"恭喜你购买成功！", "color":"#173177"},
	 *                                   "keynote1":{"value":"巧克力", "color":"#173177"},
	 *                                   "keynote2":{"value":"39.8元", "color":"#173177"},
	 *                                   "keynote3":{"value":"2014年9月22日", "color":"#173177"},
	 *                                   "remark":{"value":"欢迎再次购买！", "color":"#173177"}}
	 * @param $href                 详细页面的链接地址
	 *
	 * @return mixed    在调用模板消息接口后，会返回JSON数据包。正常时的返回JSON数据包示例：
	 *                   {"errcode":0, "errmsg":"ok", "msgid":200228332}
	 */
	public function send_template_message($openid, $template_id, $data, $href, $isShortID = false){
		$url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.$this->access_token();
		if($isShortID){
			$template = $this->get_msg_template_id($template_id);
			if($template['errcode'] != 0)
				return $template;
			else
				$templateID = $template['template_id'];
		}else{
			$templateID = $template_id;
		}
		$post = array(
			'touser'        => $openid,
			'template_id'   => $templateID,
			'url'           => $href,
			'data'          => $data,
		);
		return json_decode($this->CurlData($url, json_encode_plus($post), 'POST'), true);
	}

	public function get_media($media_id, $target){
		$url = 'http://file.api.weixin.qq.com/cgi-bin/media/get';
		$data = 'access_token='.$this->access_token().'&media_id='.$media_id;
		$ch = curl_init($url.'?'.$data);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_NOBODY, 0);    //对body进行输出。
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$package = curl_exec($ch);
		$httpinfo = curl_getinfo($ch);
		curl_close($ch);
		$media = array_merge(array('mediaBody' => $package), $httpinfo);
		$mediaBody = json_decode($media['mediaBody'], true);
		if(is_array($mediaBody) && is_int($mediaBody['errcode'])){
			return array('type' => 'error', 'content' => '微信媒体下载出错, 请与管理员联系!'.$media['mediaBody'].'--'.$media_id, 'data' => $mediaBody);
		}else{
			preg_match('/\w\/(\w+)/i', $media["content_type"], $extmatches);
			$fileExt = $extmatches[1];
			$filename = substr(md5($media_id.time()), 8, 16).'_'.time().".{$fileExt}";
			$targetPath = $target.date('/Ym/d/');
			if(!file_exists(realpath('../static/').$targetPath))
				mkdirs(realpath('../static/').$targetPath, 0755, true);
			if(file_put_contents(realpath('../static/').$targetPath.'/'.$filename, $media['mediaBody']) > 0)
				return array('type' => 'success', 'content' => '文件写入成功!', 'url' => $targetPath.$filename);
			else
				return array('type' => 'error', 'content' => '文件写入错误, 请与管理员联系!');
		}
	}

	/**
	 * 回复文本消息
	 * @param  string $text   回复的文字
	 */
	public function replyText($text){
		return $this->response($text, self::MSG_TYPE_TEXT);
	}

	/**
	 * 回复图片消息
	 * @param  string $media_id 图片ID
	 */
	public function replyImage($media_id){
		return $this->response($media_id, self::MSG_TYPE_IMAGE);
	}

	/**
	 * 回复语音消息
	 * @param  string $media_id 音频ID
	 */
	public function replyVoice($media_id){
		return $this->response($media_id, self::MSG_TYPE_VOICE);
	}

	/**
	 * 回复视频消息
	 * @param  string $media_id    视频ID
	 * @param  string $title       视频标题
	 * @param  string $discription 视频描述
	 */
	public function replyVideo($media_id, $title, $discription){
		return $this->response(func_get_args(), self::MSG_TYPE_VIDEO);
	}

	/**
	 * 回复音乐消息
	 * @param  string $title          音乐标题
	 * @param  string $discription    音乐描述
	 * @param  string $musicurl       音乐链接
	 * @param  string $hqmusicurl     高品质音乐链接
	 * @param  string $thumb_media_id 缩略图ID
	 */
	public function replyMusic($title, $discription, $musicurl, $hqmusicurl, $thumb_media_id){
		return $this->response(func_get_args(), self::MSG_TYPE_MUSIC);
	}

	/**
	 * 回复图文消息，一个参数代表一条信息
	 * @param  array  $news   图文内容 [标题，描述，URL，缩略图]
	 * @param  array  $news1  图文内容 [标题，描述，URL，缩略图]
	 * @param  array  $news2  图文内容 [标题，描述，URL，缩略图]
	 *                ...     ...
	 * @param  array  $news9  图文内容 [标题，描述，URL，缩略图]
	 */
	public function replyNews($news, $news1, $news2, $news3){
		return $this->response(func_get_args(), self::MSG_TYPE_NEWS);
	}

	/**
	 * 回复一条图文消息
	 * @param  string $title       文章标题
	 * @param  string $discription 文章简介
	 * @param  string $url         文章连接
	 * @param  string $picurl      文章缩略图
	 */
	public function replyNewsOnce($title, $discription, $url, $picurl){
		return $this->response(array(func_get_args()), self::MSG_TYPE_NEWS);
	}

	/**
	 * 数据XML编码
	 * @param  object $xml  XML对象
	 * @param  mixed  $data 数据
	 * @param  string $item 数字索引时的节点名称
	 * @return string
	 */
	private static function data2xml($xml, $data, $item = 'item') {
		foreach ($data as $key => $value) {
			/* 指定默认的数字key */
			is_numeric($key) && $key = $item;

			/* 添加子元素 */
			if(is_array($value) || is_object($value)){
				$child = $xml->addChild($key);
				self::data2xml($child, $value, $item);
			} else {
				if(is_numeric($value)){
					$child = $xml->addChild($key, $value);
				} else {
					$child = $xml->addChild($key);
					$node  = dom_import_simplexml($child);
					$cdata = $node->ownerDocument->createCDATASection($value);
					$node->appendChild($cdata);
				}
			}
		}
	}

	/**
	 * 获取调用微信JS-SDK接口的临时票据jsapi_ticket（已做全局缓存）
	 * @return mixed|string|void
	 */
	private function get_jsapi_ticket($cache=true){
		$url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket';
		if($cache){
			if(!S($this->conf.'Ticket')){
				$data = 'type=jsapi&access_token='.$this->access_token();
				$return = json_decode($this->CurlData($url, $data),true);
				S($this->conf.'Ticket', $return['ticket'], 7000);
			}
			return S($this->conf.'Ticket');
		}else{
			$data = 'type=jsapi&access_token='.$this->access_token(true, true);
			$return = json_decode($this->CurlData($url, $data),true);
			return $return['ticket'];
		}
	}

	/**
	 * 根据随机字符串、时间截、当前网页的URL获得微信JS-SDK有效签名（内置获取临时票据jsapi_ticket）
	 * @param $timestamp
	 * @param $nonceStr
	 * @param $url
	 *
	 * @return string
	 */
	private function get_signature($ticket, $timestamp, $nonceStr, $url){
		$signature = array(
			'jsapi_ticket='.$ticket,
			'noncestr='.$nonceStr,
			'timestamp='.$timestamp,
			'url='.$url,
		);
		return array('signature' => sha1(implode('&', $signature)), 'sha' => implode('&', $signature));
	}

	/**
	 * 构造文本信息
	 * @param  string $content 要回复的文本
	 */
	private static function text($content){
		$data['content'] = $content;
		return $data;
	}

	/**
	 * 构造图片信息
	 * @param  integer $media 图片ID
	 */
	private static function image($media){
		$data['media_id'] = $media;
		return $data;
	}

	/**
	 * 构造音频信息
	 * @param  integer $media 语音ID
	 */
	private static function voice($media){
		$data['media_id'] = $media;
		return $data;
	}

	/**
	 * 构造视频信息
	 * @param  array $video 要回复的视频 [视频ID，标题，说明]
	 */
	private static function video($video){
		$data = array();
		list(
			$data['media_id'],
			$data['thumb_media_id'],
			$data['title'],
			$data['description'],
			) = $video;

		return $data;
	}

	/**
	 * 构造音乐信息
	 * @param  array $music 要回复的音乐[标题，说明，链接，高品质链接，缩略图ID]
	 */
	private static function music($music){
		$data = array();
		list(
			$data['title'],
			$data['description'],
			$data['musicurl'],
			$data['hqmusicurl'],
			$data['thumb_media_id'],
			) = $music;

		return $data;
	}

	/**
	 * 构造图文信息
	 * @param  array $news 要回复的图文内容
	 * [
	 *      0 => 第一条图文信息[标题，说明，图片链接，全文连接]，
	 *      1 => 第二条图文信息[标题，说明，图片链接，全文连接]，
	 *      2 => 第三条图文信息[标题，说明，图片链接，全文连接]，
	 * ]
	 */
	private static function news($news){
		$articles = array();
		foreach ($news as $key => $value) {
			$articles[$key] = $value;
			if($key >= 9) break; //最多只允许10条图文信息
		}
		$data['ArticleCount'] = count($articles);
		$data['Articles']     = $articles;
		return $data;
	}

	private static function link($link){
		$data = array();
		list($data['Title'], $data['Description'], $data['Url']) = $link;
		return $data;
	}

	/**
	 * 对数据进行签名认证，确保是微信发送的数据
	 * @param  string $token 微信开放平台设置的TOKEN
	 * @return boolean       true-签名正确，false-签名错误
	 */
	public function checkSignature(){
		if (!$this->token)
			throw new Exception('TOKEN is not defined!');
		$tmpArr = array($this->token, $_GET["timestamp"], $_GET["nonce"]);
		sort($tmpArr, SORT_STRING);
		$tmpStr = sha1(implode($tmpArr));
		return ($tmpStr == $_GET["signature"])?true:false;
	}

	/**
	 * 获取并缓存微信接口调用凭据
	 * @return mixed|string|void
	 */
	private function get_access_token(){
		$this->url= 'https://api.weixin.qq.com/cgi-bin/token';
		$this->post = 'grant_type=client_credential&appid='.$this->appid.'&secret='.$this->secret;
		return json_decode($this->CurlData($this->url, $this->post), true);
	}

	/**
	 * 通过CURL或者封装过的Snoopy方式像微信服务器发送指令,GET或者POST方法提交数据返回结果
	 * @param        $url       提交数据的接收地址,如果是GET方法,该地址不包含?后的数据
	 * @param        $data      提交的数据GET方式为?后的部分,POST为一个表单的JSON数据
	 * @param string $method    数据提交的方法,GET(默认)或者POST
	 * @param bool   $ssl       是否SSL加密,默认为True
	 *
	 * @return string   返回服务器返回的结果
	 */
	private function CurlData($url, $data, $method='GET', $ssl=true){
		switch($this->curl){
			case 'Curl':
				switch($method){
					case 'GET':
						$getUrl = $url.'?'.$data;
						$ch = curl_init($getUrl);
						curl_setopt($ch, CURLOPT_URL,$getUrl);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						break;
					case 'POST':
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						break;
				}
				curl_setopt($ch, CURLOPT_TIMEOUT, 500);
				if($ssl){
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				}
				$results= curl_exec($ch);
				curl_close($ch);
				return $results;
			case 'Snoopy':
				Vendor('Snoopy');
				$Snoopy = new Snoopy();
				switch($method){
					case 'GET':
						$Snoopy->fetch($url.'?'.$data);
						break;
					case 'POST':
						$Snoopy->submit($url, $data);
						break;
				}
				return $Snoopy->results;
		}
	}
}