<?php
/**
 * Created by PhpStorm.
 * User: GadflyBSD
 * Date: 15/3/5
 * Time: 上午11:33
 */
class CtcSms{
	private $timestamp, $mobile, $code, $accessToken;
	private $app_id 			= '187412130000250368';							//'336151150000039245';
	private $app_secret 		= '3e2a8a70a0fb2b47c45549924128d4cc';		//'5119e52d164d4f304720414d9ed4cba4';
	private $codelen 			= 6;
	private $charset 			= '0123456789';
	private $exp_time 			= 10;
	private $access_token_url 	= 'https://oauth.api.189.cn/emp/oauth2/v3/access_token';
	private $token_url 			= 'http://api.189.cn/v2/dm/randcode/token';
	private $send_url 			= 'http://api.189.cn/v2/dm/randcode/sendSms';
	private $template_url 		= 'http://api.189.cn/v2/emp/templateSms/sendSms';
	private $grant_type 		= 'client_credentials';
	private $model_table 		= 'WebappSmsLog';

	/**
	 * 构造方法
	 * @param $phone
	 */
	public function __construct($model='', $refresh=false){
		if(is_array(C('CTCSMS'))){
			foreach(C('CTCSMS') AS $key => $val){
				$this->$key = $val;
			}
		}
		date_default_timezone_set('Asia/Shanghai'); //设置时区，避免8小时的时间差
		$this->timestamp = date('Y-m-d H:i:s');
		$this->code = $this->randstr($this->codelen, $this->charset);      //生成随机验证码
		$this->accessToken = ($refresh)?$this->refreshAccessToken():$this->cacheAccessToken();
	}

	/**
	 * 验证短信验证码是否通过验证
	 * @param array $param array('mobile' => '手机号码', 'code' => '验证码')
	 * @param string $param //'验证码'
	 * @return bool
	 */
	public function verify($param) {
		$SmsCode = session('SmsCode');
		session('SmsCode', null);
		if(is_array($param))
			return ($param['mobile'] == $SmsCode['mobile'] && $param['code'] == $SmsCode['code'] && $SmsCode['timestamp'] > time())?true:false;
		else
			return ($param == $SmsCode['code'] && $SmsCode['timestamp'] > time())?true:false;
	}

	/**
	 * 发送手机验证码
	 * @param $mobile
	 *
	 * @return array
	 * Array ( [statuas] => success [mobile] => 手机号码 [res_code] => 0 [identifier] => mg9966 [create_at] => 1425526157 )
	 */
	public function send($mobile, $uid=0, $uuid=null, $action='register') {
		if(!$this->checkMobile($mobile)){
			return array(
				'errcode'   => 70010,
				'type'      => 'error',
				'mobile'    => $mobile,
				'title'     => '发送手机格式错误!',
				'content'   => '发送失败，发送手机格式错误!',
				'timestamp' => $this->timestamp,
			);
		}else{
			$this->mobile = $mobile;
			$param['app_id']= "app_id=".$this->app_id;
			$param['access_token'] = "access_token=".$this->accessToken;
			$param['timestamp'] = "timestamp=".$this->timestamp;
			$param['token'] = "token=".$this->getToken();
			$param['phone'] = "phone=".$this->mobile;
			$param['randcode'] = "randcode=".$this->code;
			//if(!empty($this->exp_time)) $param['exp_time'] = "exp_time=".$this->exp_time;
			//$param['exp_time'] = "exp_time=10";
			ksort($param);
			$plaintext = implode("&",$param);
			$param['sign'] = "sign=".urlencode(base64_encode(hash_hmac("sha1", $plaintext, $this->app_secret, $raw_output=True)));
			ksort($param);
			$str = implode("&",$param);
			$result = json_decode($this->curlPost($this->send_url, $str), true);
			if($result['res_code'] == 0){
				session('SmsCode', array('mobile' => $this->mobile, 'code' => $this->code, 'timestamp' => time()+$this->exp_time*60));
				$return = array(
					'errcode'   => 0,
					'type'      => 'success',
					'mobile'    => $this->mobile,
					'content'   => '已成功发送手机验证码，请稍后查看您的手机短信！',
					'title'     => '成功发送短信验证码!',
				);
			}else{
				$return =  array(
					'errcode'   => '10002',
					'type'      => 'error',
					'mobile'    => $this->mobile,
					'title'     => '发送短信验证码出错!',
					'content'   => '发送失败，发送短信验证码出错!',
					'timestamp' => $this->timestamp,
				);
			}
			$this->smsLog($mobile, $this->code, array_merge($param, $result), $uid, $uuid, $action);
			return array_merge($return, $result);
		}
	}

	/**
	 * 发送模板短信接口
	 * @param string $param['mobile']  接收方号码,不支持0打头的号码
	 * @param string $param['template_id']   短信模板ID，到短信模板申请页面查看
	 * @param Array $param['template_param']   模板匹配参数二维数组
	 *
	 * @return array
	 */
	public function templateSMS($param, $uid=0, $action='invest'){
		if(!$this->checkMobile($param['mobile'])){
			return array(
				'type'      => 'error',
				'mobile'    => $this->mobile,
				'title'     => '发送手机格式错误!',
				'content'   => '发送失败，发送手机格式错误!',
				'timestamp' => $this->timestamp,
			);
		}else{
			$access['app_id']= "app_id=".$this->app_id;
			$access['access_token'] = "access_token=".$this->accessToken;
			$access['timestamp'] = "timestamp=".$this->timestamp;
			$access['acceptor_tel'] = "acceptor_tel=".$param['mobile'];
			$access['template_id'] = "template_id=".$param['template_id'];
			$access['template_param'] = "template_param=".json_encode($param['template_param']);
			ksort($access);
			$plaintext = implode("&",$access);
			$access['sign'] = "sign=".urlencode(base64_encode(hash_hmac("sha1", $plaintext, $this->app_secret, $raw_output=True)));
			ksort($access);
			$str = implode("&",$access);
			$result = json_decode($this->curlPost($this->template_url, $str), true);
			if($result['res_code'] == 0){
				$return = array(
					'status'   => 'success',
					'mobile'    => $param['mobile'],
					'result'    => $result,
				);
			}else{
				$return =  array(
					'status'    => 'error',
					'mobile'    => $param['mobile'],
					'content'   => '发送失败: ',
					'timestamp' => $this->timestamp,
				);
			}
			$this->smsLog($param['mobile'], $param['template_id'], array_merge($access, $result), $uid, $action, 'Temp');
			return array_merge($return, $result);
		}
	}

	private function smsLog($mobile, $code, $result, $uid=0, $uuid=null, $action='register', $type='Code'){
		$username = ($uid==0)?'新注册用户':M('UcenterMembers')->where('uid='.$uid)->getField('username');
		$data = array(
			'uid'           => $uid,
			'uuid'          => $uuid,
			'username'      => $username,
			'mobile'        => $mobile,
			'sms_type'      => $type,
			'actions'       => $action,
			'code'          => $code,
			'dateline'      => strtotime($this->timestamp),
			'res_code'      => $result['res_code'],
			'identifier'    => json_encode(array_merge($result, array('accessToken' => $this->accessToken))),
			'res_message'   => $result['res_message'],
			'create_at'     => ($type=='Code')?$result['create_at']:time(),
		);
		M($this->model_table)->add($data);
	}

	/**
	 * 服务器缓存AccessToken，缓存期限20天
	 * @return mixed|string|void
	 */
	private function cacheAccessToken(){
		if(!S('ChinaTelecomAccessToken')){
			$accessToken = $this->getAccessToken();
			if($accessToken['res_message'] == 'Success'){
				S('ChinaTelecomAccessToken', $accessToken['access_token'], 60*60*24*15);
				return S('ChinaTelecomAccessToken');
			}else{
				return $accessToken;
			}
		}else{
			return S('ChinaTelecomAccessToken');
		}
	}

	/**
	 * 获取access_token
	 * @return mixed
	 */
	private function getAccessToken() {
		$param['grant_type']= $this->grant_type;
		$param['app_id'] = $this->app_id;
		$param['app_secret'] = $this->app_secret;
		return json_decode($this->curlPost($this->access_token_url, $param), true);
	}

	private function refreshAccessToken() {
		S('ChinaTelecomAccessToken', NULL);
		return $this->cacheAccessToken();
	}

	public function setCache(){
		S('ChinaTelecomAccessToken', NULL);
		return $this->cacheAccessToken();
	}

	/**
	 * 获取Token
	 * @param $access_token
	 * @return mixed
	 */
	private function getToken() {
		$param['app_id']= "app_id=".$this->app_id;
		$param['access_token'] = "access_token=".$this->accessToken;
		$param['timestamp'] = "timestamp=".$this->timestamp;
		ksort($param);
		$plaintext = implode("&",$param);
		$param['sign'] = "sign=".urlencode(base64_encode(hash_hmac("sha1", $plaintext, $this->app_secret, $raw_output=True)));
		ksort($param);
		$result = $this->curlGet($this->token_url.'?'.implode("&",$param));
		$obj = json_decode($result, true);
		return ($obj['res_code'] == 0)?$obj['token']:$obj;
	}

	/**
	 * 检查手机号码格式
	 * @param $mobile
	 *
	 * @return bool
	 */
	private function checkMobile($mobile){
		if (preg_match('/^1[3|4|5|7|8][0-9]\d{4,8}$/', $mobile))
			return true;
		else
			return false;
	}

	private function randstr($len=6, $chars='abcdefghijklmnopqrstuvwxyz0123456789'){
		mt_srand((double)microtime()*1000000*getmypid());
		$randstr='';
		while(strlen($randstr)<$len)
			$randstr.=substr($chars,(mt_rand()%strlen($chars)),1);
		return $randstr;
	}

	private function curlPost($url, $postdata, $options=array()){
		$url .= '?'.http_build_query($postdata);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		if (!empty($options)) curl_setopt_array($ch, $options);
		$data = curl_exec($ch);
		curl_close($ch);
		if($data) return $data;
	}

	/**
	 * CURL GET方式获取http页面数据，非SSL加密
	 * @param string $url
	 * @return mixed
	 */
	private function curlGet($url, $ssl=false){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 500);
		if($ssl){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}