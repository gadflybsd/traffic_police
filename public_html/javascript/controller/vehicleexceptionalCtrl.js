/**
 * Created by gadflybsd on 2017/5/16.
 */
angular.module('trafficApp').controller('vehicleexceptionalCtrl', function($rootScope, $scope, $state, $uibModal, $sce, service, configs, ngSwal){
	$scope.nav = $state.current.data.nav;
	$scope.searchType = $state.current.data.searchType;
	$scope.search = $state.current.data.search;
	$scope.$emit('nav', $scope.nav);
	$scope.searchSelect = function(param){
		$scope.search = {
			type: param.value,
			label: param.label
		}
	};
	service.httpGetLists({model: configs.model.vehicleexceptional, data: {page: 1}}, function(result){
		$scope.lists = result.data.data;
		$scope.page = result.data.page;
	});
	$scope.pageChanged = function() {
		service.httpGetLists({model: configs.model.vehicleexceptional, data: {page: $scope.page.currentPage.value}}, function(result){
			$scope.lists = result.data.data;
			$scope.page = result.data.page;
		});
	};
	$scope.action = function(ac, pk){
		"新增或编辑按钮";
		switch (ac){
			case 'search':
				service.httpGetSearch({model: configs.model.vehicleexceptional, data: {page: 1, search: $scope.search}}, function(result){
					$scope.lists = result.data.data;
					$scope.page = result.data.page;
				});
				break;
			case 'del':
				"删除按钮";
				ngSwal.confirm({
					type: 'warning',
					text: '您确认要删除这条记录吗?',
				}, function(){
					service.httpPostDel({model: configs.model.vehicleexceptional, data:{id: pk}}, function (result) {
						$state.go(configs.state.vehicleexceptional, {}, {reload: true});
					});
				});
				break;
			case 'show':
				"显示异常列表按钮";
				$uibModal.open({
					animation: true,
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: 'showModalContent.html',
					controllerAs: 'ctrl',
					size: 'lg',
					controller: function ($uibModalInstance) {
						var ctrl = this;
						ctrl.cancel = function () {
							$uibModalInstance.dismiss('cancel');
						};
						ctrl.vehicleexceptional = pk;
						var url = 'http://map.baidu.com/?latlng='+pk.latitude+','+pk.longitude+'&title=车辆稽查位置&content=';
						url += pk.vehicle_license_main_owner + ',车牌:'+pk.vehicle_license_main_plate_num+'&autoOpen=true&l';
						ctrl.mapUrl = $sce.trustAsResourceUrl(url);
						ctrl.idcards_slides = (pk.idcard_count>0)?pk.idcard_url.split(","):[];
						ctrl.faces_slides = (pk.face_count>0)?pk.face_url.split(","):[];
						ctrl.photos_slides = (pk.photo_count>0)?pk.photo_url.split(","):[];
						ctrl.opts = {
							centerAndZoom: {
								longitude: pk.longitude,
								latitude: pk.latitude,
								zoom: 15
							},
						}
						ctrl.markerOpts = {
							offset: {
								width: 300,
								height: 150
							},
							icon: {
								url: 'img/markericon.png',
								size: {
									width: 49,
									height: 60
								}
							}
						};
						ctrl.point = {
							longitude: pk.longitude,
							latitude: pk.latitude,
						};
						ctrl.showWindow = function(e, marker, map) {
							map.openInfoWindow(new BMap.InfoWindow(pk.address, {
								title: pk.vehicle_license_main_owner + ',车牌:'+pk.vehicle_license_main_plate_num,
								offset: {
									width: 0,
									height: -30
								}
							}), marker.getPosition());
						};
					}
				});
				break;
			default:
				$state.go(configs.state.vehicleexceptional_action, {ac: ac, pk: pk}, {reload: true});
		}
	}
});