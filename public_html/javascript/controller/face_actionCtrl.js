/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('face_actionCtrl', function ($rootScope, $scope, $state, $stateParams, service, Upload, configs) {
	$scope.nav = $state.current.data.nav;
	$scope.$emit('nav', $scope.nav);
	if($stateParams.ac == 'add')
		$scope.formTitle = '添加';
	else
		$scope.formTitle = '编辑';
	if($stateParams.pk == 0){
		$scope.face = {}
	}else{
		service.httpGetInfo({model: configs.model.face, pk: $stateParams.pk}, function(result){
			$scope.face = result.data.data;
			$scope.face.name = result.data.data.realname;
		});
	}
	$scope.submit = function() {
		if($stateParams.ac == 'add')
			service.httpPostAdd({model: configs.model.face, pk: $stateParams.pk, data: $scope.face}, function(result){
				if(result.type == 'success') $state.go(configs.state.face, {}, {reload: true});
			});
		else
			service.httpPostEdit({model: configs.model.face, pk: $stateParams.pk, data: $scope.face}, function(result){
				if(result.type == 'success') $state.go(configs.state.face, {}, {reload: true});
			});
	};
	$scope.upload = function () {
		Upload.upload({
			url: configs.url.upload,
			data: {file: $scope.file, type: 'face', uid: 1}
		}).then(function (resp) {
			console.log("resp.data是：%o",resp.data);
			// 上传成功并接收识别数据
			$scope.face = resp.data.ocr;
			//$scope.face.face_headimg = resp.data.ocr.head_portrait.image;
			$scope.face.recip = '0.0.0.0';
			$scope.face.longitude = '98';
			$scope.face.latitude = '125';
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log(evt);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};
});