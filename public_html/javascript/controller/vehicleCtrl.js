/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('vehicleCtrl', function($rootScope, $scope, $state, $uibModal, $sce, service, configs, ngSwal){
	$scope.nav = $state.current.data.nav;
	$scope.searchType = $state.current.data.searchType;
	$scope.search = $state.current.data.search;
	$scope.$emit('nav', $scope.nav);
	$scope.searchSelect = function(param){
		$scope.search = {
			type: param.value,
			label: param.label
		}
	};
	service.httpGetLists({model: configs.model.vehicle, data: {page: 1}}, function(result){
		$scope.lists = result.data.data;
		$scope.page = result.data.page;
	});
	$scope.pageChanged = function() {
		service.httpGetLists({model: configs.model.vehicle, data: {page: $scope.page.currentPage.value}}, function(result){
			$scope.lists = result.data.data;
			$scope.page = result.data.page;
		});
	};
	$scope.action = function(ac, pk){
		"新增或编辑按钮";
		switch (ac){
			case 'search':
				service.httpGetSearch({model: configs.model.vehicle, data: {page: 1, search: $scope.search}}, function(result){
					$scope.lists = result.data.data;
					$scope.page = result.data.page;
				});
				break;
			case 'del':
				"删除按钮";
				ngSwal.confirm({
					type: 'warning',
					text: '您确认要删除这条记录吗?',
				}, function(){
					service.httpPostDel({model: configs.model.vehicle, data:{id: pk}}, function (result) {
						$state.go(configs.state.vehicleexceptional, {}, {reload: true});
					});
				});
				break;
			case 'show':
				"显示异常列表按钮";
				$uibModal.open({
					animation: true,
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: 'showModalContent.html',
					controllerAs: 'ctrl',
					size: 'lg',
					controller: function ($uibModalInstance) {
						var ctrl = this;
						ctrl.cancel = function () {
							$uibModalInstance.dismiss('cancel');
						};
						ctrl.vehicle = pk;
						ctrl.opts = {
							centerAndZoom: {
								longitude: pk.longitude,
								latitude: pk.latitude,
								zoom: 15
							},
						}
						ctrl.markerOpts = {
							offset: {
								width: 300,
								height: 150
							},
							icon: {
								url: 'img/markericon.png',
								size: {
									width: 49,
									height: 60
								}
							}
						};
						ctrl.point = {
							longitude: pk.longitude,
							latitude: pk.latitude,
						};
						ctrl.showWindow = function(e, marker, map) {
							map.openInfoWindow(new BMap.InfoWindow(pk.address, {
								title: pk.vehicle_license_main_owner + ',车牌:'+pk.vehicle_license_main_plate_num,
								offset: {
									width: 0,
									height: -30
								}
							}), marker.getPosition());
						};
						ctrl.src = 'http://api.map.baidu.com/marker?location='+pk.longitude+','+pk.latitude+'&title=车辆稽查位置&content='+pk.vehicle_license_main_owner + ',车牌:'+pk.vehicle_license_main_plate_num+'&output=html'
					}
				});
				break;
			default:
				$state.go(configs.state.vehicle_action, {ac: ac, pk: pk}, {reload: true});
		}
	}
});