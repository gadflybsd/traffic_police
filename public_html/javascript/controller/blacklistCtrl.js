/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('blacklistCtrl', function($rootScope, $scope, $state, $uibModal, service, configs, ngSwal){
	$scope.nav = $state.current.data.nav;
	$scope.searchType = $state.current.data.searchType;
	$scope.search = $state.current.data.search;
	$scope.$emit('nav', $scope.nav);
	$scope.searchSelect = function(param){
		$scope.search = {
			type: param.value,
			label: param.label
		}
	};
	service.httpGetLists({model: configs.model.blacklist, data: {page: 1}}, function(result){
		$scope.lists = result.data.data;
		$scope.page = result.data.page;
	});
	$scope.pageChanged = function() {
		service.httpGetLists({model: configs.model.blacklist, data: {page: $scope.page.currentPage.value}}, function(result){
			$scope.lists = result.data.data;
			$scope.page = result.data.page;
		});
	};
	$scope.action = function(ac, pk){
		"新增或编辑按钮";
		switch (ac){
			case 'search':
				service.httpGetSearch({model: configs.model.blacklist, data: {page: 1, search: $scope.search}}, function(result){
					$scope.lists = result.data.data;
					$scope.page = result.data.page;
				});
				break;
			case 'del':
				"删除按钮";
				ngSwal.confirm({
					type: 'warning',
					text: '您确认要删除这条记录吗?',
				}, function(){
					service.httpPostDel({model: configs.model.blacklist, data:{id: pk}}, function (result) {
						$state.go(configs.state.vehicleexceptional, {}, {reload: true});
					});
				})
				break;
			case 'show':
				"显示异常列表按钮";
				$uibModal.open({
					animation: true,
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: 'showModalContent.html',
					controllerAs: 'ctrl',
					size: 'lg',
					controller: function ($uibModalInstance) {
						var ctrl = this;
						ctrl.cancel = function () {
							$uibModalInstance.dismiss('cancel');
						};
						ctrl.blacklist = pk;
					}
				});
				break;
			default:
				$state.go(configs.state.blacklist_action, {ac: ac, pk: pk}, {reload: true});
		}
	}
});