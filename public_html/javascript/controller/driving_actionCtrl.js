/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('driving_actionCtrl', function ($rootScope, $scope, $state, $stateParams, service, Upload, configs) {
	$scope.nav = $state.current.data.nav;
	$scope.$emit('nav', $scope.nav);
	if($stateParams.ac == 'add')
		$scope.formTitle = '添加';
	else
		$scope.formTitle = '编辑';
	if($stateParams.pk == 0){
		$scope.driving = {}
	}else{
		service.httpGetInfo({model: configs.model.driving, pk: $stateParams.pk}, function(result){
			$scope.driving = result.data.data;
		});
	}
	$scope.submit = function() {
		if($stateParams.ac == 'add')
			service.httpPostAdd({model: configs.model.driving, pk: $stateParams.pk, data: $scope.face}, function(result){
				if(result.type == 'success') $state.go(configs.state.driving, {}, {reload: true});
			});
		else
			service.httpPostEdit({model: configs.model.driving, pk: $stateParams.pk, data: $scope.face}, function(result){
				if(result.type == 'success') $state.go(configs.state.driving, {}, {reload: true});
			});
	};
	$scope.upload = function () {
		Upload.upload({
			url: configs.url.upload,
			data: {file: $scope.file, type: 'driving', uid:1},
		}).then(function (resp) {
			console.log("resp.data是：%o",resp.data);
			// 上传成功并接收识别数据
			$scope.driving = resp.data.ocr;
			var valid = $scope.driving.driving_license_main_valid_from.split(' 至 ');
			$scope.driving.driving_license_main_valid_end = valid[1];
			$scope.driving.driving_license = resp.data.data.id;
			$scope.driving.driving_license_type = resp.data.ocr.type;
			$scope.driving.recip = '0.0.0.0';
			$scope.driving.longitude = '98';
			$scope.driving.latitude = '125';
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};
});