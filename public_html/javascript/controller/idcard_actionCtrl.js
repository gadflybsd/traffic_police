/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('idcard_actionCtrl', function ($rootScope, $scope, $state, $stateParams, service, Upload, configs) {
	$scope.nav = $state.current.data.nav;
	$scope.$emit('nav', $scope.nav);
	if($stateParams.ac == 'add')
		$scope.formTitle = '添加';
	else
		$scope.formTitle = '编辑';
	if($stateParams.pk == 0){
		$scope.idcard = {}
	}else{
		service.httpGetInfo({model: configs.model.idcard, pk: $stateParams.pk}, function(result){
			$scope.vehicle = result.data.data;
		});
	}
	$scope.submit = function() {
		if($stateParams.ac == 'add')
			service.httpPostAdd({model: configs.model.idcard, pk: $stateParams.pk, data: $scope.idcard}, function(result){
				if(result.type == 'success') $state.go(configs.state.idcard, {}, {reload: true});
			});
		else
			service.httpPostEdit({model: configs.model.idcard, pk: $stateParams.pk, data: $scope.idcard}, function(result){
				if(result.type == 'success') $state.go(configs.state.idcard, {}, {reload: true});
			});
	};
	$scope.upload = function () {
		Upload.upload({
			url: configs.url.upload,
			data: {file: $scope.file, type: 'idcard', uid: 1}
		}).then(function (resp) {
			// 上传成功并接收识别数据
			$scope.idcard = resp.data.ocr;
			$scope.idcard.idcard_1 = resp.data.data.id;
			$scope.idcard.headimg = resp.data.ocr.head_portrait.image;
			$scope.idcard.recip = '0.0.0.0';
			$scope.idcard.longitude = '98';
			$scope.idcard.latitude = '125';
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log(evt);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};
});