/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('blacklist_actionCtrl', function ($rootScope, $scope, $state, $stateParams, service, Upload, configs) {
	$scope.nav = $state.current.data.nav;
	$scope.$emit('nav', $scope.nav);
	if($stateParams.ac == 'add')
		$scope.formTitle = '添加';
	else
		$scope.formTitle = '编辑';
	if($stateParams.pk == 0){
		$scope.blacklist = {}
	}else{
		service.httpGetInfo({model: configs.model.blacklist, pk: $stateParams.pk}, function(result){
			$scope.blacklist = result.data.data;
		});
	}
	$scope.submit = function() {
		if($stateParams.ac == 'add')
			service.httpPostAdd({model: configs.model.blacklist, pk: $stateParams.pk, data: $scope.face}, function(result){
				if(result.type == 'success') $state.go(configs.state.blacklist, {}, {reload: true});
			});
		else
			service.httpPostEdit({model: configs.model.blacklist, pk: $stateParams.pk, data: $scope.face}, function(result){
				if(result.type == 'success') $state.go(configs.state.blacklist, {}, {reload: true});
			});
	};
	$scope.upload = function () {
		Upload.upload({
			url: configs.url.upload,
			data: {file: $scope.file, type: 'blacklist', uid: 0},
		}).then(function (resp) {
			// 上传成功并接收识别数据
			$scope.blacklist = resp.data.ocr;
			$scope.blacklist.recip = '0.0.0.0';
			$scope.blacklist.longitude = '98';
			$scope.blacklist.latitude = '125';
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};
});