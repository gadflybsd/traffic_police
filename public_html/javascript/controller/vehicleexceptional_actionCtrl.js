/**
 * Created by gadflybsd on 2017/5/14.
 */
angular.module('trafficApp').controller('vehicleexceptional_actionCtrl', function ($rootScope, $scope, $state, $stateParams, service, Upload, configs) {
	$scope.nav = $state.current.data.nav;
	$scope.$emit('nav', $scope.nav);
	if($stateParams.ac == 'add')
		$scope.formTitle = '添加';
	else
		$scope.formTitle = '编辑';
	if($stateParams.pk == 0){
		$scope.vehicleexceptional = {}
	}else{
		service.httpGetInfo({model: configs.model.vehicleexceptional, pk: $stateParams.pk}, function(result){
			$scope.vehicleexceptional = result.data.data;
		});
	}
	$scope.submit = function() {
		if($stateParams.ac == 'add')
			service.httpPostAdd({model: configs.model.vehicleexceptional, pk: $stateParams.pk, data: $scope.vehicleexceptional}, function(result){
				if(result.type == 'success') $state.go(configs.state.vehicleexceptional, {}, {reload: true});
			});
		else
			service.httpPostEdit({model: configs.model.vehicleexceptional, pk: $stateParams.pk, data: $scope.vehicleexceptional}, function(result){
				if(result.type == 'success') $state.go(configs.state.vehicleexceptional, {}, {reload: true});
			});
	};
	$scope.upload = function () {
		Upload.upload({
			url: configs.url.upload,
			data: {file: $scope.file, type: 'vehicleexceptional', uid: 0},
		}).then(function (resp) {
			// 上传成功并接收识别数据
			$scope.vehicleexceptional = resp.data.ocr;
			$scope.vehicleexceptional.driving_license = resp.data.data.id;
			$scope.vehicleexceptional.driving_license_type = resp.data.ocr.type;
			$scope.vehicleexceptional.recip = '0.0.0.0';
			$scope.vehicleexceptional.longitude = '98';
			$scope.vehicleexceptional.latitude = '125';
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	};
});