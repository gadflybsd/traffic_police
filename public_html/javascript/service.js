var app = angular.module('starter.services', []);
/**
 * # service.unit 类的描述: 常用工具类
 * @class Service.unit
 * @author GadflyBSD
 */
app.factory('unit', function(){
	var wait=60;
	return {
		/**
		 * # 发送短信验证码服务
		 * @param ele
		 */
		sendSmsTimer: function(ele){
			if (wait == 0) {
				ele.removeAttr('disabled').text("重新获取验证码");
				wait = 60;
			} else {
				ele.attr('disabled', 'disabled').text(wait + "秒后重发验证码");
				wait--;
				setTimeout(function(){this.sendSmsTimer(ele)}, 1000)
			}
		},
		isNull: function (param){
			return (typeof(param) != 'undefined' && param && param !=0)?false:true;
		},
		/**
		 * # 判断是否是空值
		 * @param e
		 * @returns {boolean}
		 */
		isEmpty: function (e) {
			if(!e || angular.isUndefined(e) || e === null)
				return true;
			else
				return false;
		},
		/**
		 * # 判断是否是空对象
		 * @param e
		 * @returns {boolean}
		 */
		isEmptyObject: function(e){
			if(angular.isUndefined(e) || e === null){
				return true;
			}else{
				var t;
				for (t in e)
					return !true;
				return !false;
			}
		},
		/**
		 * # 判断[search] 是否在[array]数组中
		 * @param {String} search
		 * @param {Array} array
		 * @returns {boolean}
		 */
		in_array: function(search, array) {
			for(var i in array){
				if(array[i].toString() == search) return true;
			}
			return false;
		},
		/**
		 * 判断是否是数组
		 * @param value
		 * @returns {*|boolean}
		 */
		is_array: function (value) {
			return value &&
				typeof value === 'object' &&
				typeof value.length === 'number' &&
				typeof value.splice === 'function' &&
				!(value.propertyIsEnumerable('length'));
		},
		analyze_sql: function(obj){
			var sql = new Array;
			for(var i in obj){
				sql.push('DROP TABLE IF EXISTS `'+ i +'`;')
				var temp = 'CREATE TABLE `'+ i +'` (';
				var field = new Array;
				for(var p in obj[i]){
					if(obj[i][p] == 'key'){
						field.push('`'+p+'` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT');
					}else{
						var isNull = (obj[i][p].null)?' NOT NULL':'';
						var defaults = (obj[i][p].defaults)?obj[i][p].defaults:'';
						field.push('`'+p+'` ' + obj[i][p].type + isNull + defaults);
					}
				}
				sql.push(temp + field.join(', ') + ');')
			}
			return sql;
		},
		analyze_data: function(obj){
			var sql = new Array;
			for(var i in obj){
				var value = new Array;
				for(var i=0; i<obj[i].data.length; i++){
					value.push('?');
				}
				sql.push({sql: 'INSERT INTO `' + i + '` (' + obj[i].field.join(', ') + ') VALUES (' + value.join(', ') + ');', data: obj[i].data});
			}
			return sql;
		}
	}
});
/**
 * # Service.ngSwal 类的描述
 * @class Service.ngSwal
 * @author GadflyBSD
 * @param {Object} defaults
 * @param {Boolean} defaults.allowOutsideClick=false 如果设置为“true”，用户可以通过点击警告框以外的区域关闭警告框。
 * @param {String} defaults.confirmButtonColor=#DD6B55 该参数用来改变确认按钮的背景颜色（必须是一个HEX值）
 * @param {String} defaults.confirmButtonText=确定 该参数用来改变确认按钮上的文字。如果设置为"true"，那么确认按钮将自动将"Confirm"替换为"OK"。
 * @param {String} defaults.type=info 窗口的类型。有4种类型的图标动画："warning", "error", "success" 和 "info".可以将它放在"type"数组或通过第三个参数传递。
 * @param {String} defaults.title 窗口的名称。可以通过对象的"title"属性或第一个参数进行传递。
 * @param {String} defaults.text 窗口的描述。可以通过对象的"text"属性或第二个参数进行传递。
 * @param {Boolean} defaults.showCancelButton=false 如果设置为“true”，“cancel”按钮将显示，点击可以关闭警告框。
 * @param {Boolean} defaults.showConfirmButton=true 如果设置为“false”，“Confirm”按钮将不显示。
 * @param {String} defaults.cancelButtonText=取消 该参数用来改变取消按钮的文字。
 * @param {Boolean} defaults.closeOnConfirm=true 如果希望以后点击了确认按钮后模态窗口仍然保留就设置为"false"。该参数在其他SweetAlert触发确认按钮事件时十分有用。
 * @param {Number} defaults.timer 警告框自动关闭的时间。单位是ms。
 */
app.factory('ngSwal', function(unit, $state, toastr, configs){
	var defaults = {
		allowOutsideClick: false,		// 如果设置为“true”，用户可以通过点击警告框以外的区域关闭警告框。
		confirmButtonColor: "#DD6B55",	// 该参数用来改变确认按钮的背景颜色（必须是一个HEX值）。
		confirmButtonText: "确定",		// 该参数用来改变确认按钮上的文字。如果设置为"true"，那么确认按钮将自动将"Confirm"替换为"OK"。
		type: 'info',					// 窗口的类型。有4种类型的图标动画："warning", "error", "success" 和 "info".可以将它放在"type"数组或通过第三个参数传递。
		title: null,					// 窗口的名称。可以通过对象的"title"属性或第一个参数进行传递。
		text: null,						// 窗口的描述。可以通过对象的"text"属性或第二个参数进行传递。
		showCancelButton: false,		// 如果设置为“true”，“cancel”按钮将显示，点击可以关闭警告框。
		showConfirmButton: true,		// 如果设置为“false”，“Confirm”按钮将不显示。
		cancelButtonText: '取消',		// 该参数用来改变取消按钮的文字。
		closeOnConfirm: true,			// 如果希望以后点击了确认按钮后模态窗口仍然保留就设置为"false"。该参数在其他SweetAlert触发确认按钮事件时十分有用。
		timer: null						// 警告框自动关闭的时间。单位是ms。
	}
	function getTitle(type, title){
		if(unit.isEmptyObject(title)){
			switch(type){
				case 'success':
					return '管理后台数据操作成功！';
				case 'error':
					return '管理后台数据操作失败！';
				case 'warning':
					return '管理后台数据操作警告！';
				case 'info':
					return '管理后台数据正在加载！';
			}
		}else{
			return title;
		}
	}
	return {
		/**
		 * # 弹出框方法
		 * @param {Object} param
		 * @param {String} param.type=success 弹出框类型 ['success', 'error', 'warning', 'info']
		 * @param {String} param.title=操作成功！ 弹出框标题
		 * @param {String} param.text 弹出框内容
		 * @param {Function} callback
		 *
		 * **使用范例**：
		 *
		 *     @example
		 *     var opts={
	     *          type: "success",
	     *          title: "成功啦!",
	     *          text: "操作成功啦!"
	     *     }
		 *     ngSwal.alert(opts, function(){
	     *          //回调函数
	     *     });
		 */
		alert: function(param, callback){
			var config = {}
			config.type = (unit.isEmptyObject(param.type))?'success':param.type.toLowerCase();
			config.title = getTitle(config.type, param.title);
			swal(angular.extend(defaults, param, config), function(){
				if(config.type == 'error'){
					$state.go(configs.state.home, {}, {reload: true});
				}else{
					if(typeof(callback) == 'function') callback();
				}
			});
		},
		/**
		 * # 用于显示一个带有指定消息和 OK 及取消按钮的对话框。
		 * @param {Object} param
		 * @param {String} param.type=warning 弹出框类型 ['success', 'error', 'warning', 'info']
		 * @param {String} param.title=操作警告！ 弹出框标题
		 * @param {String} param.text 弹出框显示内容
		 * @param {String} param.showCancelButton=true 是否显示取消按钮
		 * @param {Function} callback
		 *
		 * **使用范例**：
		 *
		 *     @example
		 *     var opts={
	     *          type: "success",
	     *          title: "成功啦!",
	     *          text: "操作成功啦!"
	     *     }
		 *     ngSwal.confirm(opts, function(){
	     *          // 点击确定按钮所执行的回调函数
	     *     }, function(){
	     *          // 点击取消按钮所执行的回调函数
	     *     });
		 */
		confirm: function(param, callback_ok, callback_cancel){
			var config = {}
			config.type = (unit.isEmptyObject(param.type))?'warning':param.type.toLowerCase();
			config.title = getTitle(config.type, param.title);
			config.showCancelButton = true;
			swal(angular.extend(defaults, param, config), function(isConfirm){
				if (isConfirm) {
					if(typeof(callback_ok) == 'function') callback_ok();
				}else{
					if(typeof(callback_cancel) == 'function') callback_cancel();
				}
			});
		},
		/**
		 * # 用于显示一个带有进度滚动条的提示框。
		 * @param {Object} param
		 * @param {String} param.width 进度滚动条百分比
		 * @param {String} param.text 弹出框显示内容
		 *
		 * **使用范例**：
		 *
		 *     @example
		 *     var opts={
	     *          width: 25,
	     *          text: "操作成功啦!"
	     *     }
		 *     ngSwal.progress(opts);
		 */
		progress: function(param){
			var config = {
				type: 'info',
				html: true,
				title: getTitle('info'),
				text: '<h5 style="color: #797979">正在 <span class="progressText">'+param.text+'</span> 请等待！</h5><div class = "progress progress-striped active">' +
				'<div class = "progress-bar progress-bar-success" role="progressbar" style="width: '+param.width+'%;"></div></div>',
				showLoaderOnConfirm: true,
			};
			swal(angular.extend(defaults, config));
			swal.disableButtons();
		},
		loading: function(param){
			var config = {
				type: 'info',
				html: true,
				title: getTitle('info'),
				text: '<h5 style="color: #797979">正在 <span class="progressText">'+param.text+'</span> 请等待！</h5>',
				showLoaderOnConfirm: true,
			};
			swal(angular.extend(defaults, config));
			swal.disableButtons();
		},
		toastr: function(data, text){
			switch(data.type.toLowerCase()){
				case 'success':
					toastr.success(data.msg, unit.isNull(data.title) ? text[0] : data.title);
					break;
				case 'error':
					toastr.error(data.msg, unit.isNull(data.title) ? text[1] : data.title);
					break;
				case 'warning':
					toastr.warning(data.msg, unit.isNull(data.title) ? '操作警告！' : data.title);
					break;
				case 'info':
				default:
					toastr.info(data.msg, unit.isNull(data.title) ? '操作提醒！' : data.title);
			}
		},
		/**
		 * # 用于显示可提示用户进行输入的对话框。
		 * @param {Object} param
		 * @param {String} param.type=input 弹出框类型 ['success', 'error', 'warning', 'info', 'input']
		 * @param {String} param.title=操作警告！ 弹出框标题
		 * @param {String} param.text 弹出框显示内容
		 * @param {String} param.showCancelButton=true 是否显示取消按钮
		 * @param {String} param.closeOnConfirm=false 如果希望以后点击了确认按钮后模态窗口仍然保留就设置为"false"。该参数在其他SweetAlert触发确认按钮事件时十分有用。
		 * @param {String} param.disableButtonsOnConfirm=true 是否显示取消按钮
		 * @param {String} param.animation="slide-from-top" 是否显示取消按钮
		 * @param callback
		 */
		prompt: function (param, callback) {
			var config ={}
			config.type = (unit.isEmptyObject(param.type))?'input':param.type.toLowerCase();
			config.showCancelButton = true;
			config.closeOnConfirm= false;
			config.disableButtonsOnConfirm = true;
			config.animation = "slide-from-top";
			swal(angular.extend(defaults, config, param), function(inputValue){
				if(inputValue) if(typeof(callback) == 'function') callback(inputValue);
			});
		},
		/**
		 * # 关闭当前弹出框
		 */
		close: function(){
			swal.close();
		},
		/**
		 * # 打开按钮
		 */
		enableButtons: function(){
			swal.enableButtons();
		},
		/**
		 * # 关闭按钮
		 */
		disableButtons: function(){
			swal.disableButtons();
		}
	}
});
/**
 * # Service.service 类的描述
 * @class Service.service
 * @author GadflyBSD
 */
app.factory('service', function($http, $q, $window, $state, $loading, configs, unit, ngSwal){
	var deferred = $q.defer();
	return {
		/**
		 * # http数据请求服务
		 * @param mothod
		 * @param url
		 * @param param
		 */
		httpRequest: function(method, url, param, collback_success, collback_error){
			var config = {
				url: url,
				cache: false,
				timeout: 1000 * configs.timeout,
			}
			param.data = JSON.stringify(param.data);
			switch(method.toLowerCase()){
				case 'get':
				case 'delete':
					config.method = method.toLowerCase();
					config.params = param;
					break;
				case 'post':
				case 'put':
				default:
					config.method = method.toLowerCase();
					config.data = param;
			}
			$http(config).then(function(result, status, headers, config){
				if(configs.debug) console.log('XMLHttpRequest Success: ', status, result, headers, config);
				if(unit.in_array(method.toLowerCase(), ['post', 'put'])){
					ngSwal.alert({
						text: result.data.msg,
						type: result.data.type.toLowerCase(),
						closeOnConfirm: true,
						confirmButtonText: '确定',
						showCancelButton: false,
					},function(){
						if(typeof collback_success == 'function') collback_success(result);
					});
				}else{
					if(typeof collback_success == 'function') collback_success(result);
				}
			}, function(result, status, headers, config){
				if(configs.debug) console.log('XMLHttpRequest Error: ', status, result, headers, config);
				ngSwal.alert({
					text: 'HTTP 请求出现错误, 请与管理员联系!',
					type: 'error',
					closeOnConfirm: true,
					confirmButtonText: '确定',
					showCancelButton: false,
				},function(){
					if(typeof collback_error == 'function') collback_error(result);
				});
			});
		},
		/**
		 * # 读取本地JSON数据文件
		 * @param files
		 * @param {Function} on_success
		 * @param {Function} on_error
		 */
		getLocalityJson: function(files, on_success, on_error){
			var sourceFileName = cordova.file.applicationDirectory + files;
			resolveLocalFileSystemURL(sourceFileName, function (fileEntry) {
				fileEntry.file(function (file) {
					var reader = new FileReader();
					reader.onloadend = function() {
						if(configs.debug) console.log('get Locality Json File Success: ', JSON.parse(this.result));
						if(typeof on_success == 'function') on_success(JSON.parse(this.result));
					};
					reader.readAsText(file);
				}, function(error){
					if(configs.debug) console.log('get Locality Json File Error: ', error);
					if(typeof on_error == 'function') on_error(error);
				});
			});
		},
		/**
		 * # 获取网络JSON数据
		 * @param url
		 * @param {Function} callback
		 */
		getJson: function (url, callback) {
			$http({url: url, method: 'get'}).success(function(resp){
				if(typeof callback == 'function') callback(resp);
			}).error(function(error){
				console.log('Error: ', error.message);
			});
		},
		/**
		 * # restful 数据请求服务
		 * @param method
		 * @param param
		 */
		restful: function(method, param, callback_success, callback_error){
			this.httpRequest(method, configs.url.restful, param, function(result, status, headers, config){
				if(result.type == 'Success'){
					if (!unit.isEmptyObject(result.localStorage)){
						if(typeof result.localStorage == 'object'){
							for (var p in result.localStorage) {
								if(typeof result.localStorage[p] == 'string' && result.localStorage[p] == 'empty'){
									$window.localStorage.removeItem(p);
								}else{
									var local = result.localStorage[p];
									$window.localStorage.setItem(p, (typeof local == 'object') ? JSON.stringify(local) : local);
								}
							}
						}
					}
					if (!unit.isEmptyObject(result.sessionStorage)){
						if(typeof result.localStorage == 'object'){
							for (var p in result.sessionStorage) {
								if(typeof result.sessionStorage[p] == 'string' && result.sessionStorage[p] == 'empty'){
									$window.sessionStorage.removeItem(p);
								}else{
									var session = result.sessionStorage[p];
									$window.sessionStorage.setItem(p, (typeof session == 'object') ? JSON.stringify(session) : session);
								}
							}
						}
					}
				}
				if(typeof callback_success == 'function') callback_success(result, status, headers, config);
			}, function(result, status, headers, config){
				if(typeof callback_error == 'function') callback_error(result, status, headers, config);
			});
		},
		/**
		 * restful 数据列表请求服务
		 * @param param
		 * @param callback_success
		 * @param callback_error
		 */
		httpGetLists: function(param, callback_success, callback_error){
			$loading.setDefaultOptions({active: true, text: '正在加载数据列表...'});
			$loading.start('lists');
			var defaults = {module: 'lists'}
			this.restful('get', angular.extend(defaults, param), 
				function(result, status, headers, config){
					ngSwal.toastr(result.data, ['数据列表加载成功！', '数据列表加载失败！']);
					if(typeof callback_success == 'function') callback_success(result, status, headers, config);
					$loading.finish('lists');
				},
				function(result, status, headers, config){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
					$loading.finish('lists');
				}
			);
		},
		/**
		 * restful 指定数据详情请求服务
		 * @param param
		 * @param callback_success
		 * @param callback_error
		 */
		httpGetInfo: function(param, callback_success, callback_error){
			$loading.setDefaultOptions({active: true, text: '正在加载数据详情...'});
			$loading.start('info');
			var defaults = {module: 'info'}
			this.restful('get', angular.extend(defaults, param),
				function(result, status, headers, config){
					ngSwal.toastr(result.data, ['指定记录数据详情加载成功！', '指定记录数据详情加载失败！']);
					if(typeof callback_success == 'function') callback_success(result, status, headers, config);
					$loading.finish('info');
				},
				function(result, status, headers, config){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
					$loading.finish('info');
				}
			);
		},
		/**
		 * restful 数据搜索请求服务
		 * @param param
		 * @param callback_success
		 * @param callback_error
		 */
		httpGetSearch: function(param, callback_success, callback_error){
			$loading.setDefaultOptions({active: true, text: '正在加载数据详情...'});
			$loading.start('lists');
			var defaults = {module: 'search'}
			this.restful('get', angular.extend(defaults, param),
				function(result, status, headers, config){
					ngSwal.toastr(result.data, ['搜索记录数据加载成功！', '搜索记录数据加载失败！']);
					if(typeof callback_success == 'function') callback_success(result, status, headers, config);
					$loading.finish('lists');
				},
				function(result, status, headers, config){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
					$loading.finish('lists');
				}
			);
		},
		/**
		 * restful 数据新增请求服务
		 * @param param
		 * @param callback_success
		 * @param callback_error
		 */
		httpPostAdd: function(param, callback_success, callback_error){
			$loading.setDefaultOptions({active: true, text: '正在向服务器新增数据...'});
			$loading.start('action');
			var defaults = {module: 'add'}
			this.restful('post', angular.extend(defaults, param),
				function(result, status, headers, config){
					ngSwal.toastr(result.data, ['指定记录数据详情新增成功！', '指定记录数据详情新增失败！']);
					if(typeof callback_success == 'function') callback_success(result, status, headers, config);
					$loading.finish('action');
				},
				function(result, status, headers, config){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
					$loading.finish('action');
				}
			);
		},
		/**
		 * restful 数据编辑请求服务
		 * @param param
		 * @param callback_success
		 * @param callback_error
		 */
		httpPostEdit: function(param, callback_success, callback_error){
			$loading.setDefaultOptions({active: true, text: '正在向服务器修改数据...'});
			$loading.start('action');
			var defaults = {module: 'edit'}
			this.restful('post', angular.extend(defaults, param),
				function(result, status, headers, config){
					ngSwal.toastr(result.data, ['指定记录数据详情修改成功！', '指定记录数据详情修改失败！']);
					if(typeof callback_success == 'function') callback_success(result, status, headers, config);
					$loading.finish('action');
				},
				function(result, status, headers, config){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
					$loading.finish('action');
				}
			);
		},
		/**
		 * restful 数据删除请求服务
		 * @param param
		 * @param callback_success
		 * @param callback_error
		 */
		httpPostDel: function(param, callback_success, callback_error){
			$loading.setDefaultOptions({active: true, text: '正在向服务器修改数据...'});
			$loading.start('list');
			var defaults = {module: 'del'}
			this.restful('post', angular.extend(defaults, param),
				function(result, status, headers, config){
					ngSwal.toastr(result.data, ['指定记录数据删除成功！', '指定记录数据删除失败！']);
					if(typeof callback_success == 'function') callback_success(result, status, headers, config);
					$loading.finish('list');
				},
				function(result, status, headers, config){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
					$loading.finish('list');
				}
			);
		},
		/**
		 * # 获取本地Storage数据服务
		 * @return
		 */
		getStorage: function(){
			var localStorage = {};
			var sessionStorage = {};
			var checkData = {};
			for(var p in $window.localStorage){
				localStorage[p] = JSON.parse($window.localStorage.getItem(p));
				if(unit.isEmptyObject(localStorage[p].md5) && unit.isEmptyObject(localStorage[p].sha1))
					checkData[p] = {md5: localStorage[p].md5, sha1: localStorage[p].sha1};
			}
			for(var p in $window.sessionStorage){
				sessionStorage[p] = JSON.parse($window.sessionStorage.getItem(p));
				if(unit.isEmptyObject(sessionStorage[p].md5) && unit.isEmptyObject(sessionStorage[p].sha1))
					checkData[p] = {md5: sessionStorage[p].md5, sha1: sessionStorage[p].sha1};
			}
			if(unit.isEmptyObject(checkData)){
				$http({
					url: url,
					cache: false,
					timeout: 30000,
					method: 'get',
					params: {action: 'Restful', module: 'getCacheData', data: checkData}
				}).success(function(resp){
					if(resp.type == 'Success'){
						if(!unit.isEmptyObject(resp.sessionStorage)){
							for (var p in resp.sessionStorage) {
								if(typeof resp.sessionStorage[p] == 'string' && resp.sessionStorage[p] == 'empty'){
									$window.sessionStorage.removeItem(p);
								}else {
									var local = resp.sessionStorage[p];
									$window.sessionStorage.setItem(p, (typeof local == 'object') ? JSON.stringify(local) : local);
								}
							}
						}
						if(!unit.isEmptyObject(resp.localStorage)){
							for (var p in resp.localStorage) {
								var local = resp.localStorage[p];
								$window.localStorage.setItem(p, (typeof local == 'object') ? JSON.stringify(local) : local);
							}
						}

					}
				});
			}
			deferred.resolve(localStorage, sessionStorage);
			return deferred.promise;
		},
		/**
		 * 第一次数据安装设置标志
		 * @param initial
		 */
		setInitialRun: function (initial) {
			$window.localStorage.setItem("initialRun", (initial ? "true" : "false"));
		},
		/**
		 * # 判断是否是第一次APP安装
		 * @return {boolean}
		 */
		isInitialRun: function () {
			var value = $window.localStorage.getItem("initialRun") || "true";
			return value == "true";
		},
		/**
		 * # 是否设置过APP登录密码
		 * @return {*|boolean}
		 */
		isSetPassword: function(){
			return unit.isEmpty($window.localStorage.getItem("app_password"));
		},
		/**
		 * # 获取最后一次登录操作时间截
		 * @return {Number}
		 */
		getLastTimestamp: function(){
			return parseInt($window.localStorage.getItem('lastTimestamp'));
		},
		/**
		 * # 设置最后一次登录操作时间截
		 */
		setLastTimestamp: function(){
			$window.localStorage.setItem('lastTimestamp', new Date().getTime());
		},
		/**
		 * # 是否锁屏
		 * @param {Function} callback
		 */
		isLockScreen: function(callback){
			if(this.isSetPassword){
				$state.go('app.appset_password')
			}else{
				if(new Date().getTime() > (this.getLastTimestamp() + configs.timeout))
					$state.go('app.lock_screen');
				else
				if(typeof callback == 'function') callback();
			}
		}
	}
});