/**
 * Created by gadflybsd on 2017/5/9.
 */
var jq = $.noConflict();
(function(){
	var ngBaiduMap = window.ngBaiduMap;
	var app = angular.module('trafficApp', [ngBaiduMap, 'ui.router', 'darthwade.dwLoading', 'toastr', 'angular-image-404', 'oc.lazyLoad', 'starter.services']);
	app.constant('configs', {
		timeout: 30,
		sqlite: false,
		url: {
			restful:'http://222.219.73.36/public_html/restful.php?s=Index/angular.html',
			upload: 'http://222.219.73.36/public_html/public_html/restful.php?s=index/uploadPicture.html',
			//restful:'http://home.hzxiansheng.com/public_html/restful.php?s=Index/angular.html',
			//upload: 'http://home.hzxiansheng.com/public_html/restful.php?s=index/uploadPicture.html',
			//restful: 'http://tp.cn/public_html/restful.php?s=index/angular.php',
			//upload: 'http://tp.cn/public_html/restful.php?s=index/uploadPicture.html',
		},
		state: {
			home: 'home',
			vehicle: 'vehicle',
			vehicle_action: 'vehicle_action',
			driving: 'driving',
			driving_action: 'driving_action',
			idcard: 'idcard',
			idcard_action: 'idcard_action',
			face: 'face',
			face_action: 'face_action',
			vehicleexceptional: 'vehicleexceptional',
			vehicleexceptional_action: 'vehicleexceptional_action',
			blacklist: 'blacklist',
			blacklist_action: 'blacklist_action',
		},
		model: {
			user: 'user',
			vehicle: 'vehicle',
			driving: 'driving',
			idcard: 'idcard',
			face: 'face',
			vehicleexceptional: 'vehicleExceptional',
			blacklist: 'blacklist',
		},
		baidu:{
			apiKey: 'sUVxgOPLFQpoNH8j3rHWUGQVtPV99O8K'
		},
		debug: true
	});
	app.config(['mapScriptServiceProvider', function(provider) {
		provider.setKey('sUVxgOPLFQpoNH8j3rHWUGQVtPV99O8K');
	}]);
	app.config(function($stateProvider, $urlRouterProvider, $httpProvider, configs, toastrConfig) {
		angular.extend(toastrConfig, {
			allowHtml: false,
			closeButton: false,
			closeHtml: '<button>&times;</button>',
			extendedTimeOut: 500,
			iconClasses: {
				error: 'toast-error',
				info: 'toast-info',
				success: 'toast-success',
				warning: 'toast-warning'
			},
			messageClass: 'toast-message',
			onHidden: null,
			onShown: null,
			onTap: null,
			progressBar: false,
			tapToDismiss: true,
			templates: {
				toast: '../node_modules/angular-toastr/src/directives/toast/toast.html',
				progressbar: '../node_modules/angular-toastr/src/directives/progressbar/progressbar.html'
			},
			timeOut: 2000,
			titleClass: 'toast-title',
			toastClass: 'toast'
		});
		$httpProvider.defaults.headers = {
			post: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			get: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			put: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			delete: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}
		$httpProvider.defaults.transformRequest = function(obj) {
			var str = [];
			for (var p in obj) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
			return str.join("&");
		};
		$stateProvider
			.state(configs.state.home, {
				url: "/" + configs.state.home,
				//cache: true,
				data: {
					nav: {
						pageTitle: '首页',
					},
				},
				templateUrl: "templates/home.html",
				controller: function($rootScope, $scope, $state){
					$scope.nav = $state.current.data.nav;
					$scope.$emit('nav', $scope.nav);
				}
			})
			.state(configs.state.vehicle, {
				url: "/" + configs.state.vehicle,
				cache: false,
				data: {
					nav: {
						pageTitle: '车辆模拟库列表',
						library: [
							{label: '车辆模拟库', url: configs.state.vehicle},
							{label: '数据列表', url: configs.state.vehicle, class: 'active'}
						]
					},
					searchType: [
						{label: '车辆所有人', value: 'owner'},
						{label: '车牌号码', value: 'num'},
						{label: '车辆识别代号', value: 'vin'},
						{label: '发动机号', value: 'no'},
					],
					search: {
						label: '车辆所有人',
						value: 'owner'
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
							'javascript/controller/vehicleCtrl.js'
						]);
					}
				},
				templateUrl: "templates/vehicle.html",
				controller: 'vehicleCtrl'
			})
			.state(configs.state.vehicle_action, {
				url: "/" + configs.state.vehicle_action + "/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '车辆模拟库数据操作',
						library: [
							{label: '车辆模拟库', url: configs.state.vehicle},
							{label: '数据操作', url: configs.state.vehicle_action, class: 'active'}
						]
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
							'../node_modules/ng-file-upload/dist/ng-file-upload.min.js',
							'javascript/controller/vehicle_actionCtrl.js'
						]);
					}
				},
				templateUrl: "templates/vehicle_action.html",
				controller: 'vehicle_actionCtrl'
			})
			.state(configs.state.driving, {
				url: "/" + configs.state.driving,
				cache: false,
				data: {
					nav: {
						pageTitle: '人口模拟库列表',
						library: [
							{label: '人口模拟库', url: configs.state.driving},
							{label: '数据列表', url: configs.state.driving, class: 'active'}
						]
					},
					searchType: [
						{label: '姓名', value: 'name'},
						{label: '身份证号码', value: 'num'},
					],
					search: {
						label: '姓名',
						value: 'name'
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
							'javascript/controller/drivingCtrl.js'
						]);
					}
				},
				templateUrl: "templates/driving.html",
				controller: 'drivingCtrl'
			})
			.state(configs.state.driving_action, {
				url: "/" + configs.state.driving_action + "/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '人口模拟库数据操作',
						library: [
							{label: '人口模拟库', url: configs.state.driving},
							{label: '数据操作', url: configs.state.driving_action, class: 'active'}
						]
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
							'../node_modules/ng-file-upload/dist/ng-file-upload.min.js',
							'javascript/controller/driving_actionCtrl.js'
						]);
					}
				},
				templateUrl: "templates/driving_action.html",
				controller: 'driving_actionCtrl'
			})
			.state(configs.state.idcard, {
				url: "/" + configs.state.idcard,
				cache: false,
				data: {
					nav: {
						pageTitle: '住户登记模拟库列表',
						library: [
							{label: '住户登记模拟库', url: configs.state.idcard},
							{label: '数据列表', url: configs.state.idcard, class: 'active'}
						]
					},
					searchType: [
						{label: '姓名', value: 'name'},
						{label: '身份证号码', value: 'num'},
					],
					search: {
						label: '姓名',
						value: 'name',
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
							'../node_modules/angular-image-404/dist/angular-image-404.min.js',
							'javascript/controller/idcardCtrl.js'
						]);
					}
				},
				templateUrl: "templates/idcard.html",
				controller: 'idcardCtrl'
			})
			.state(configs.state.idcard_action, {
				url: "/" + configs.state.idcard_action + "/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '住户登记模拟库数据操作',
						library: [
							{label: '住户登记模拟库', url: configs.state.idcard},
							{label: '数据操作', url: configs.state.idcard_action, class: 'active'}
						]
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
							'../node_modules/ng-file-upload/dist/ng-file-upload.min.js',
							'javascript/controller/idcard_actionCtrl.js'
						]);
					}
				},
				templateUrl: "templates/idcard_action.html",
				controller: 'idcard_actionCtrl'
			})
			.state(configs.state.face, {
				url: "/" + configs.state.face,
				cache: false,
				data: {
					nav: {
						pageTitle: '黑名单模拟库列表',
						library: [
							{label: '黑名单模拟库', url: configs.state.face},
							{label: '数据列表', url: configs.state.face, class: 'active'}
						]
					},
					searchType: [
						{label: '姓名', value: 'name'},
						{label: '身份证号码', value: 'num'},
					],
					search: {
						label: '姓名',
						value: 'name',
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
							//'../node_modules/angular-image-404/dist/angular-image-404.min.js',
							'javascript/controller/faceCtrl.js'
						]);
					}
				},
				templateUrl: "templates/face.html",
				controller: 'faceCtrl'
			})
			.state(configs.state.face_action, {
				url: "/" + configs.state.face_action + "/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '黑名单模拟库数据操作',
						library: [
							{label: '黑名单模拟库', url: configs.state.face},
							{label: '数据操作', url: configs.state.face_action, class: 'active'}
						]
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
							'../node_modules/ng-file-upload/dist/ng-file-upload.min.js',
							'javascript/controller/face_actionCtrl.js'
						]);
					}
				},
				templateUrl: "templates/face_action.html",
				controller: 'face_actionCtrl'
			})
			.state('unusual', {
				url: "/unusual",
				cache: false,
				data: {
					nav: {
						pageTitle: '异常信息数据列表',
						library: [
							{label: '异常信息数据', url: 'unusual'},
							{label: '数据列表', url: 'unusual', class: 'active'}
						]
					},
					searchType: [
						{label: '姓名', value: 'realname'},
						{label: '异常信息', value: 'unusual'},
					],
					search: {
						label: '姓名',
						value: 'realname'
					}
				},
				templateUrl: "templates/unusual.html",
				controller: function($rootScope, $scope, $state, service){
					$scope.nav = $state.current.data.nav;
					$scope.searchType = $state.current.data.searchType;
					$scope.search = $state.current.data.search;
					$scope.$emit('nav', $scope.nav);
					$scope.searchSelect = function(param){
						$scope.search = {
							type: param.value,
							label: param.label
						}
					};
					service.restful('get', {model: 'unusual', module: 'list'}).then(function(result){
						$scope.list = result.data;
					});
					$scope.action = function(ac, pk){
						"新增或编辑按钮";
						switch (ac){
							case 'search':
								service.restful('get', {model: 'unusual', module: 'seasrch', data: $scope.search}).then(function(resp){
									$scope.list = resp.data;
								});
								break;
							case 'del':
								"删除按钮";
								break;
							case 'show':
								"显示异常列表按钮";
								break;
							default:
								$state.go('unusual_action', {ac: ac, pk: pk}, {reload: true});
						}
					}
				}
			})
			.state('unusual_action', {
				url: "/unusual_action/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '异常信息数据操作',
						library: [
							{label: '异常信息数据', url: 'unusual'},
							{label: '数据操作', url: 'unusual_action', class: 'active'}
						]
					}
				},
				templateUrl: "templates/unusual_action.html",
				controller: function ($rootScope, $scope, $state, $stateParams, service) {
					$scope.nav = $state.current.data.nav;
					$scope.$emit('nav', $scope.nav);
					if($stateParams.ac == 'add')
						$scope.formTitle = '添加';
					else
						$scope.formTitle = '编辑';
					if($stateParams.pk == 0){
						$scope.unusual = {}
					}else{
						service.restful('get', {model: 'unusual', module: 'info', pk: $stateParams.pk}).then(function(result){
							$scope.unusual = result.data;
						});
					}
					$scope.submit = function(){
						console.log($scope.unusual);
					}
				}
			})
			.state(configs.state.vehicleexceptional, {
				url: "/" + configs.state.vehicleexceptional,
				cache: false,
				data: {
					nav: {
						pageTitle: '车辆采集数据列表',
						library: [
							{label: '车辆采集数据', url: configs.state.vehicleexceptional},
							{label: '数据列表', url: configs.state.vehicleexceptional, class: 'active'}
						]
					},
					searchType: [
						{label: '车辆所有人', value: 'owner'},
						{label: '车牌号码', value: 'vehicle'},
						{label: '驾驶人员', value: 'name'},
						{label: '驾驶证号', value: 'driving'},
					],
					search: {
						label: '车辆所有人',
						value: 'owner'
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
							//'../node_modules/angular-image-404/dist/angular-image-404.min.js',
							'javascript/controller/vehicleexceptionalCtrl.js'
						]);
					}
				},
				templateUrl: "templates/vehicleexceptional.html",
				controller: 'vehicleexceptionalCtrl'
			})
			.state(configs.state.vehicleexceptional_action, {
				url: "/" + configs.state.vehicleexceptional_action + "/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '车辆采集数据操作',
						library: [
							{label: '车辆采集数据', url: configs.state.vehicleexceptional},
							{label: '数据操作', url: configs.state.vehicleexceptional_action, class: 'active'}
						]
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
							'../node_modules/ng-file-upload/dist/ng-file-upload.min.js',
							'javascript/controller/vehicleexceptional_actionCtrl.js'
						]);
					}
				},
				templateUrl: "templates/vehicleexceptional_action.html",
				controller: 'vehicleexceptional_actionCtrl'
			})
			.state(configs.state.blacklist, {
				url: "/" + configs.state.blacklist,
				cache: false,
				data: {
					nav: {
						pageTitle: '黑名单列表',
						library: [
							{label: '黑名单数据', url: configs.state.blacklist},
							{label: '数据列表', url: configs.state.blacklist, class: 'active'}
						]
					},
					searchType: [
						{label: '身份证号码', value: 'idcard'},
						{label: '驾驶证号码', value: 'driving'},
						{label: '车牌号码', value: 'vehicle'},
					],
					search: {
						label: '身份证号码',
						value: 'idcard'
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
							'../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
							'../node_modules/angular-image-404/dist/angular-image-404.min.js',
							'javascript/controller/blacklistCtrl.js'
						]);
					}
				},
				templateUrl: "templates/blacklist.html",
				controller: 'blacklistCtrl'
			})
			.state(configs.state.blacklist_action, {
				url: "/" + configs.state.blacklist_action + "/:ac/:pk",
				cache: false,
				data: {
					nav: {
						pageTitle: '黑名单操作',
						library: [
							{label: '黑名单数据', url: configs.state.blacklist},
							{label: '数据操作', url: configs.state.blacklist_action, class: 'active'}
						]
					}
				},
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'../node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
							'../node_modules/ng-file-upload/dist/ng-file-upload.min.js',
							'javascript/controller/blacklist_actionCtrl.js'
						]);
					}
				},
				templateUrl: "templates/blacklist_action.html",
				controller: 'blacklist_actionCtrl'
			});
		$urlRouterProvider.otherwise('/' + configs.state.home);
	});
	app.controller('topCtrl', function($scope) {
		$scope.$on('nav', function(event, data) {
			$scope.nav = data;
			console.log($scope.nav);
		});
	});
}());