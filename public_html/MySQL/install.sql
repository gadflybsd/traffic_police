DROP TABLE IF EXISTS police_user;
CREATE TABLE police_user(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uuid VARCHAR(36) DEFAULT NULL COMMENT '用户UUID',
  police_name VARCHAR(45) DEFAULT NULL COMMENT '用户姓名',
  police_sn VARCHAR(45) DEFAULT NULL COMMENT '警官号码',
  headimg INT DEFAULT 0 COMMENT '用户头像',
  mobile VARCHAR(11) NOT NULL COMMENT '手机号码',
  password VARCHAR(41) DEFAULT NULL COMMENT '用户密码',
  clear_password VARCHAR(32) NOT NULL COMMENT '明文密码',
  register_recip VARCHAR(15) DEFAULT NULL COMMENT '用户注册时IP',
  register_dateline INT DEFAULT 0 COMMENT '用户注册时间截',
  last_changes VARCHAR(45) DEFAULT NULL COMMENT '最后一次变更类型',
  last_dateline INT DEFAULT 0 COMMENT '最后一次登录时间截',
  last_recip VARCHAR(15) NOT NULL COMMENT '最后一次操作者ip',
  last_related VARCHAR(45) DEFAULT NULL COMMENT '最后一次变更关联数据表名',
  last_pk INT DEFAULT 0 COMMENT '最后一次变更数据表主键',
  last_info TEXT DEFAULT NULL COMMENT '最后一次变更说明',
  last_remark TEXT DEFAULT NULL COMMENT '其他备注, 用于审核说明',
  status TINYINT(2) DEFAULT 1 COMMENT '用户启用状态：0-->禁用， 1-->启用',
  PRIMARY KEY (id, mobile),
  UNIQUE KEY mobile (mobile)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='APP用户信息表';

DROP TRIGGER IF EXISTS police_user_BEFORE_INSERT;
CREATE TRIGGER police_user_BEFORE_INSERT BEFORE INSERT ON police_user FOR EACH ROW
  BEGIN
    SET NEW.password = password(NEW.clear_password);
    SET NEW.register_dateline = UNIX_TIMESTAMP(now());
    SET NEW.register_recip = NEW.last_recip;
    SET NEW.last_related = 'user';
    SET NEW.last_changes = 'register';
    SET NEW.last_info = CONCAT('新警官用户【', NEW.mobile, '】成功注册成为用户!');
  END;

DROP TRIGGER IF EXISTS police_user_AFTER_INSERT;
CREATE TRIGGER police_user_AFTER_INSERT AFTER INSERT ON police_user FOR EACH ROW
  BEGIN
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.id, NEW.last_related, NEW.id, NEW.last_changes, NEW.last_info, NEW.last_recip, NEW.register_dateline);
  END;

DROP TRIGGER IF EXISTS police_user_BEFORE_UPDATE;
CREATE TRIGGER opolice_user_BEFORE_UPDATE BEFORE UPDATE ON police_user FOR EACH ROW
  BEGIN
    CASE NEW.last_changes
      WHEN 'setHeadimg' THEN
      SET NEW.last_info = 'APP 用户自定义头像操作！';
      WHEN 'setNickname' THEN
      SET NEW.last_info = 'APP 用户设定昵称操作！';
      WHEN 'setPassword' THEN
      SET NEW.password = password(NEW.clear_password);
      SET NEW.last_info = 'APP 用户变更登录密码操作！';
      WHEN 'forgotPassword' THEN
      SET NEW.password = password(NEW.clear_password);
      SET NEW.last_info = 'APP 用户通过手机重置密码操作！';
      WHEN 'setMobile' THEN
      SET NEW.last_info = 'APP 用户变更手机号码操作！';
      WHEN 'loginSystem' THEN
      SET NEW.last_info = '用户登录APP系统操作!';
      WHEN 'logoutSystem' THEN
      SET NEW.uuid = null;
      SET NEW.last_info = '用户退出APP系统操作!';
      WHEN 'changeStatus' THEN
      SET NEW.uuid = null;
      CASE NEW.status
        WHEN 0 THEN
        SET NEW.last_info = '变更用户状态为禁用操作成功！';
        WHEN 1 THEN
        SET NEW.last_info = '变更用户状态为启用操作成功！';
      ELSE
        SET NEW.last_info = '变更用户状态的其它操作！';
      END CASE;
    END CASE;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.id, 'user', NEW.id, NEW.last_changes, NEW.last_info, NEW.last_recip, UNIX_TIMESTAMP(now()));
  END;

DROP TABLE IF EXISTS police_log;
CREATE TABLE police_log(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  related VARCHAR(30) DEFAULT 'user' COMMENT '关联表:face/picture/idcard/driving/vehicle/vehicle_exceptional等,如果没有则为NULL',
  related_pk INT NOT NULL COMMENT '关联表id或主键',
  changes VARCHAR(20) DEFAULT NULL COMMENT '影响操作类型',
  info TEXT DEFAULT NULL COMMENT '日志说明',
  recip VARCHAR(15) DEFAULT '0.0.0.0' COMMENT '操作者ip',
  remark TEXT DEFAULT NULL COMMENT '其他备注, 用于审核说明/充值体现说明等',
  checkuid INT DEFAULT 0 COMMENT '处理人uid',
  checkip VARCHAR(15) DEFAULT '0.0.0.0' COMMENT '处理人ip',
  dateline INT DEFAULT 0 COMMENT '数据变动时间',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='APP用户系统日志记录表';


DROP TABLE IF EXISTS police_picture;
CREATE TABLE police_picture(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  type VARCHAR(15) NOT NULL COMMENT '上传文件类型',
  path VARCHAR(255) DEFAULT NULL COMMENT '路径',
  md5 VARCHAR(32) DEFAULT NULL COMMENT '文件md5',
  sha1 VARCHAR(40) DEFAULT NULL COMMENT '文件 sha1编码',
  recip VARCHAR(15) NOT NULL COMMENT '操作者ip',
  status TINYINT(2) DEFAULT 0 NOT NULL COMMENT '状态',
  dateline INT DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='数据图片上传记录表';

DROP VIEW IF EXISTS police_picture_view;
CREATE VIEW police_picture_view AS
  SELECT
    police_picture.id,
    police_picture.uid,
    police_picture.type,
    police_picture.path,
    CONCAT('http://222.219.73.36', police_picture.path) AS url,
    police_picture.md5,
    police_picture.sha1,
    police_picture.recip,
    police_picture.status,
    police_picture.dateline,
    FROM_UNIXTIME(police_picture.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_picture LEFT JOIN police_user ON ( police_picture.uid = police_user.id);

DROP VIEW IF EXISTS police_user_view;
CREATE VIEW police_user_view AS
  SELECT
    police_user.id AS uid,
    police_user.uuid,
    police_user.police_name,
    police_user.police_sn,
    police_user.headimg,
    if(police_user.headimg = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_user.headimg))
    ) AS headimg_url,
    police_user.mobile,
    police_user.register_recip,
    police_user.register_dateline,
    FROM_UNIXTIME(police_user.register_dateline, '%Y-%m-%d %h:%i:%s') AS register_time,
    police_user.last_dateline,
    FROM_UNIXTIME(police_user.last_dateline, '%Y-%m-%d %h:%i:%s') AS last_time
  FROM police_user;

DROP VIEW IF EXISTS police_log_view;
CREATE VIEW police_log_view AS
  SELECT
    police_log.id,
    police_log.uid,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg AS police_headimg_url,
    police_log.related,
    police_log.related_pk,
    police_log.changes,
    police_log.info,
    police_log.recip,
    police_log.remark,
    police_log.checkuid,
    police_log.checkip,
    police_log.dateline,
    FROM_UNIXTIME(police_log.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_log LEFT JOIN police_user_view ON ( police_log.uid = police_user_view.uid);

DROP TRIGGER IF EXISTS police_picture_BEFORE_INSERT;
CREATE TRIGGER police_picture_BEFORE_INSERT BEFORE INSERT ON police_picture FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_picture_AFTER_INSERT;
CREATE TRIGGER police_picture_AFTER_INSERT AFTER INSERT ON police_picture FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    CASE NEW.type
      WHEN 'headimg' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传头像图片!');
      WHEN 'face' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象人脸图片!');
      WHEN 'idcard' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象身份证图片!');
      WHEN 'driving' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象驾驶证图片!');
      WHEN 'vehicle' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象行驶证图片!');
      WHEN 'picture' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象嫌疑物品图片!');
    ELSE SET @info = CONCAT('警官用户【', @mobile, '】什么都没有操作!');
    END CASE;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'picture', NEW.id, NEW.type, @info, NEW.recip, NEW.dateline);
  END;

DROP TABLE IF EXISTS police_face;
CREATE TABLE police_face(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  name VARCHAR(20) DEFAULT NULL COMMENT '真实姓名',
  id_number VARCHAR(18) DEFAULT NULL COMMENT '身份证号码',
  face_headimg TEXT DEFAULT NULL COMMENT '人脸图片Base64',
  face_license INT DEFAULT 0 COMMENT '人脸图片地址',
  resultcount INT DEFAULT 0 COMMENT '匹配人脸个数',
  result TEXT DEFAULT NULL COMMENT '人脸匹配结果集',
  recip VARCHAR(15) DEFAULT NULL COMMENT '记录创建者ip',
  longitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  latitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  is_blacklist TINYINT(2) DEFAULT 0 COMMENT '是否列入黑名单',
  dateline INT DEFAULT 0 COMMENT '记录创建时间截',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='人脸数据记录表';

DROP TRIGGER IF EXISTS police_face_BEFORE_INSERT;
CREATE TRIGGER police_face_BEFORE_INSERT BEFORE INSERT ON police_face FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_face_AFTER_INSERT;
CREATE TRIGGER police_face_AFTER_INSERT AFTER INSERT ON police_face FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的人脸图片成功进行OCR识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'face', NEW.id, 'ocr', @info, NEW.recip, NEW.dateline);
  END;

DROP VIEW IF EXISTS police_face_view;
CREATE VIEW police_face_view AS
  SELECT
    police_face.id,
    police_face.uid,
    police_face.name AS realname,
    police_face.id_number,
    police_face.face_license,
    if(police_face.face_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_face.face_license))
    ) AS face_headimg_url,
    police_face.resultcount,
    police_face.result,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg_url AS police_headimg_url,
    police_face.recip,
    police_face.longitude,
    police_face.latitude,
    police_face.dateline,
    FROM_UNIXTIME(police_face.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_face LEFT JOIN police_user_view ON (police_face.uid = police_user_view.uid);

DROP TABLE IF EXISTS police_idcard;
CREATE TABLE police_idcard(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  name VARCHAR(20) NOT NULL COMMENT '真实姓名',
  id_number VARCHAR(18) NOT NULL COMMENT '身份证号码',
  birthday VARCHAR(18) DEFAULT NULL COMMENT '出生年月日',
  sex VARCHAR(6) DEFAULT NULL COMMENT '性别',
  people VARCHAR(20) DEFAULT NULL COMMENT '民族',
  address VARCHAR(100) DEFAULT NULL COMMENT '地址',
  type VARCHAR(6) DEFAULT NULL COMMENT '身份证类型',
  headimg TEXT DEFAULT NULL COMMENT '身份证头像',
  idcard_1 INT DEFAULT 0 COMMENT '身份证正面图片地址',
  idcard_2 INT DEFAULT 0 COMMENT '身份证背面图片地址',
  recip VARCHAR(15) DEFAULT NULL COMMENT '记录创建者ip',
  longitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  latitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  is_blacklist TINYINT(2) DEFAULT 0 COMMENT '是否列入黑名单',
  dateline INT DEFAULT 0 COMMENT '记录创建时间截',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='身份证数据记录表';

DROP TRIGGER IF EXISTS police_idcard_BEFORE_INSERT$$
CREATE TRIGGER police_idcard_BEFORE_INSERT BEFORE INSERT ON police_idcard FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_idcard_AFTER_INSERT;
CREATE TRIGGER police_idcard_AFTER_INSERT AFTER INSERT ON police_idcard FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的身份证「', NEW.id_number, '」图片成功进行OCR证件识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'ocr', NEW.id, 'idcard', @info, NEW.recip, NEW.dateline);
  END;

DROP VIEW IF EXISTS police_idcard_view;
CREATE VIEW police_idcard_view AS
  SELECT
    police_idcard.id,
    police_idcard.uid,
    police_idcard.name AS realname,
    police_idcard.id_number,
    police_idcard.birthday,
    police_idcard.sex,
    police_idcard.people,
    police_idcard.address,
    police_idcard.type,
    police_idcard.headimg,
    if(police_idcard.headimg = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_idcard.headimg))
    ) AS headimg_url,
    police_idcard.idcard_1,
    if(police_idcard.idcard_1 = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_idcard.idcard_1))
    ) AS idcard_url_1,
    police_idcard.idcard_2,
    if(police_idcard.idcard_2 = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_idcard.idcard_2))
    ) AS idcard_url_2,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg_url AS police_headimg_url,
    police_idcard.recip,
    police_idcard.longitude,
    police_idcard.latitude,
    police_idcard.dateline,
    FROM_UNIXTIME(police_idcard.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_idcard LEFT JOIN police_user_view ON (police_idcard.uid = police_user_view.uid);

DROP TABLE IF EXISTS police_driving_license;
CREATE TABLE police_driving_license(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  name VARCHAR(20) NOT NULL COMMENT '真实姓名',
  driving_license_main_number varchar(18) NOT NULL COMMENT '驾驶证号码',
  birthday VARCHAR(18) DEFAULT NULL COMMENT '出生年月日',
  sex VARCHAR(6) DEFAULT NULL COMMENT '性别',
  driving_license_main_nationality varchar(20) DEFAULT NULL COMMENT '国籍',
  driving_license_main_valid_from varchar(100) DEFAULT NULL COMMENT '有效期限',
  driving_license_main_valid_end DATE DEFAULT NULL COMMENT '到期日期',
  issue_date VARCHAR(18) DEFAULT NULL COMMENT '初次领证日期',
  drivetype VARCHAR(20) NOT NULL COMMENT '准驾类型',
  driving_license_type varchar(100) NOT NULL COMMENT '驾驶证证件名称',
  driving_license INT DEFAULT 0 COMMENT '驾驶证图片地址',
  recip VARCHAR(15) DEFAULT NULL COMMENT '记录创建者ip',
  longitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  latitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  is_blacklist TINYINT(2) DEFAULT 0 COMMENT '是否列入黑名单',
  dateline INT DEFAULT 0 COMMENT '记录创建时间截',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='驾驶证数据记录表';

DROP TRIGGER IF EXISTS police_driving_license_BEFORE_INSERT$$
CREATE TRIGGER police_driving_license_BEFORE_INSERT BEFORE INSERT ON police_driving_license FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_driving_license_AFTER_INSERT;
CREATE TRIGGER police_driving_license_AFTER_INSERT AFTER INSERT ON police_driving_license FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的驾驶证「', NEW.driving_license_main_number, '」图片成功进行OCR证件识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'ocr', NEW.id, 'driving', @info, NEW.recip, NEW.dateline);
  END;

DROP VIEW IF EXISTS police_driving_license_view;
CREATE VIEW police_driving_license_view AS
  SELECT
    police_driving_license.id,
    police_driving_license.uid,
    police_driving_license.name,
    police_driving_license.driving_license_main_number,
    police_driving_license.birthday,
    police_driving_license.sex,
    police_driving_license.driving_license_main_nationality,
    police_driving_license.driving_license_main_valid_from,
    police_driving_license.driving_license_main_valid_end,
    police_driving_license.issue_date,
    police_driving_license.drivetype,
    police_driving_license.driving_license_type,
    police_driving_license.driving_license,
    if(police_driving_license.driving_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_driving_license.driving_license))
    ) AS driving_license_url,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg_url AS police_headimg_url,
    police_driving_license.recip,
    police_driving_license.longitude,
    police_driving_license.latitude,
    police_driving_license.dateline,
    FROM_UNIXTIME(police_driving_license.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_driving_license LEFT JOIN police_user_view ON (police_driving_license.uid = police_user_view.uid);

DROP TABLE IF EXISTS police_vehicle_license;
CREATE TABLE police_vehicle_license(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  vehicle_license_main_vehicle_type VARCHAR(20) DEFAULT NULL COMMENT '车辆类型',
  vehicle_license_main_plate_num VARCHAR(10) DEFAULT NULL COMMENT '车辆号牌号码',
  vehicle_license_main_plate_ids INT DEFAULT 0 COMMENT '车辆号牌图片ID',
  vehicle_license_main_owner VARCHAR(20) DEFAULT NULL COMMENT '车辆所有人',
  address VARCHAR(200) DEFAULT NULL COMMENT '住址',
  vehicle_license_main_user_character VARCHAR(50) DEFAULT NULL COMMENT '使用性质',
  vehicle_license_main_model VARCHAR(50) DEFAULT NULL COMMENT '品牌型号',
  vehicle_license_main_vin VARCHAR(50) DEFAULT NULL COMMENT '车辆识别代号',
  vehicle_license_main_engine_no VARCHAR(50) DEFAULT NULL COMMENT '发动机号码',
  vehicle_license_main_register_date DATE DEFAULT NULL COMMENT '注册日期',
  issue_date VARCHAR(18) DEFAULT NULL COMMENT '初次领证日期',
  vehicle_license INT DEFAULT 0 COMMENT '驾驶证图片ID',
  recip VARCHAR(15) DEFAULT NULL COMMENT '记录创建者ip',
  longitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  latitude DECIMAL(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  is_blacklist TINYINT(2) DEFAULT 0 COMMENT '是否列入黑名单',
  dateline INT DEFAULT 0 COMMENT '记录创建时间截',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='行驶证数据记录表';

DROP TRIGGER IF EXISTS police_vehicle_license_BEFORE_INSERT;
CREATE TRIGGER police_vehicle_license_BEFORE_INSERT BEFORE INSERT ON police_vehicle_license FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_vehicle_license_AFTER_INSERT;
CREATE TRIGGER police_vehicle_license_AFTER_INSERT AFTER INSERT ON police_vehicle_license FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的行驶证「', NEW.vehicle_license_main_plate_num, '」图片成功进行OCR证件识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'ocr', NEW.id, 'vehicle', @info, NEW.recip, NEW.dateline);
  END;

DROP VIEW IF EXISTS police_vehicle_license_view;
CREATE VIEW police_vehicle_license_view AS
  SELECT
    police_vehicle_license.id,
    police_vehicle_license.uid,
    police_vehicle_license.vehicle_license_main_vehicle_type,
    police_vehicle_license.vehicle_license_main_plate_num,
    police_vehicle_license.vehicle_license_main_plate_ids,
    if(police_vehicle_license.vehicle_license_main_plate_ids = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_vehicle_license.vehicle_license_main_plate_ids))
    ) AS vehicle_license_main_plate_url,
    police_vehicle_license.vehicle_license_main_owner,
    police_vehicle_license.address,
    police_vehicle_license.vehicle_license_main_user_character,
    police_vehicle_license.vehicle_license_main_model,
    police_vehicle_license.vehicle_license_main_vin,
    police_vehicle_license.vehicle_license_main_engine_no,
    police_vehicle_license.vehicle_license_main_register_date,
    police_vehicle_license.issue_date,
    police_vehicle_license.vehicle_license,
    if(police_vehicle_license.vehicle_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_vehicle_license.vehicle_license))
    ) AS vehicle_license_url,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg_url AS police_headimg_url,
    police_vehicle_license.recip,
    police_vehicle_license.longitude,
    police_vehicle_license.latitude,
    police_vehicle_license.dateline,
    FROM_UNIXTIME(police_vehicle_license.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_vehicle_license LEFT JOIN police_user_view ON (police_vehicle_license.uid = police_user_view.uid);

DROP TABLE IF EXISTS police_vehicle_exceptional_log;
CREATE TABLE police_vehicle_exceptional_log(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  driving_id INT DEFAULT 0 COMMENT '驾驶证信息ID',
  vehicle_id INT DEFAULT 0 COMMENT '行驶证信息ID',
  idcard_ids TEXT DEFAULT NULL COMMENT '随车人员ID,多张以,分割',
  idcard_url TEXT DEFAULT NULL COMMENT '随车人员证件地址,多张以,分割',
  idcard_count INT DEFAULT 0 COMMENT '随车人员数量',
  face_ids TEXT DEFAULT NULL COMMENT '随车人员人脸ID,多张以,分割',
  face_url TEXT DEFAULT NULL COMMENT '随车人员人脸地址,多张以,分割',
  face_count INT DEFAULT 0 COMMENT '随车人员人脸数量',
  photo_ids TEXT DEFAULT NULL COMMENT '可疑物品照片ID,多张以,分割',
  photo_url TEXT DEFAULT NULL COMMENT '可疑物品照片地址,多张以,分割',
  photo_count INT DEFAULT 0 COMMENT '可疑物品数量',
  description TEXT DEFAULT NULL COMMENT '异常情况描述',
  longitude DECIMAL(18,13) DEFAULT NULL COMMENT '异常情况数据记录经度值',
  latitude DECIMAL(18,13) DEFAULT NULL COMMENT '异常情况数据记录维度值',
  recip VARCHAR(15) DEFAULT NULL COMMENT '记录创建者ip',
  is_blacklist TINYINT(2) DEFAULT 0 COMMENT '是否列入黑名单',
  dateline INT DEFAULT 0 COMMENT '记录创建时间截',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='车辆稽查情况数据记录表';

DROP TRIGGER IF EXISTS police_vehicle_exceptional_log_BEFORE_INSERT$$
CREATE TRIGGER police_vehicle_exceptional_log_BEFORE_INSERT BEFORE INSERT ON police_vehicle_exceptional_log FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_vehicle_exceptional_log_AFTER_INSERT;
CREATE TRIGGER police_vehicle_exceptional_log_AFTER_INSERT AFTER INSERT ON police_vehicle_exceptional_log FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SELECT vehicle_license_main_plate_num INTO @vehicle_num FROM police_vehicle_license WHERE police_vehicle_license.id = NEW.vehicle_id;
    SET @info = CONCAT('警官用户【', @mobile, '】成功创建车辆「', @vehicle_num, '」稽查情况数据记录！');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'exceptional', NEW.id, 'vehicle', @info, NEW.recip, NEW.dateline);
  END;

DROP VIEW IF EXISTS police_vehicle_exceptional_log_view;
CREATE VIEW police_vehicle_exceptional_log_view AS
  SELECT
    police_vehicle_exceptional_log.id,
    police_vehicle_exceptional_log.uid,
    police_vehicle_exceptional_log.driving_id,
    police_driving_license.name AS realname,
    police_driving_license.driving_license_main_number,
    police_driving_license.birthday,
    police_driving_license.sex,
    police_driving_license.driving_license_main_nationality,
    police_driving_license.driving_license_main_valid_from,
    police_driving_license.driving_license_main_valid_end,
    police_driving_license.issue_date AS driving_issue_date,
    police_driving_license.drivetype,
    police_driving_license.driving_license_type,
    if(police_driving_license.driving_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_driving_license.driving_license))
    ) AS driving_license_url,
    police_vehicle_exceptional_log.vehicle_id,
    police_vehicle_license.vehicle_license_main_vehicle_type,
    police_vehicle_license.vehicle_license_main_plate_num,
    police_vehicle_license.vehicle_license_main_plate_ids,
    if(police_vehicle_license.vehicle_license_main_plate_ids = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_vehicle_license.vehicle_license_main_plate_ids))
    ) AS vehicle_license_main_plate_url,
    police_vehicle_license.vehicle_license_main_owner,
    police_vehicle_license.address,
    police_vehicle_license.vehicle_license_main_user_character,
    police_vehicle_license.vehicle_license_main_model,
    police_vehicle_license.vehicle_license_main_vin,
    police_vehicle_license.vehicle_license_main_engine_no,
    police_vehicle_license.vehicle_license_main_register_date,
    police_vehicle_license.issue_date AS vehicle_issue_date,
    police_vehicle_license.vehicle_license,
    if(police_vehicle_license.vehicle_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_vehicle_license.vehicle_license))
    ) AS vehicle_license_url,
    police_vehicle_exceptional_log.idcard_ids,
    police_vehicle_exceptional_log.idcard_url,
    police_vehicle_exceptional_log.idcard_count,
    police_vehicle_exceptional_log.face_ids,
    police_vehicle_exceptional_log.face_url,
    police_vehicle_exceptional_log.face_count,
    police_vehicle_exceptional_log.photo_ids,
    police_vehicle_exceptional_log.photo_url,
    police_vehicle_exceptional_log.photo_count,
    police_vehicle_exceptional_log.description,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg_url AS police_headimg_url,
    police_vehicle_exceptional_log.longitude,
    police_vehicle_exceptional_log.latitude,
    police_vehicle_exceptional_log.recip,
    police_vehicle_exceptional_log.dateline,
    FROM_UNIXTIME(police_vehicle_exceptional_log.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM ((police_vehicle_exceptional_log
    LEFT JOIN police_user_view ON (police_vehicle_exceptional_log.uid = police_user_view.uid)
    LEFT JOIN police_driving_license ON (police_vehicle_exceptional_log.driving_id = police_driving_license.id))
    LEFT JOIN police_vehicle_license ON (police_vehicle_exceptional_log.vehicle_id = police_vehicle_license.id));

DROP TABLE IF EXISTS police_vehicle_together_men;
DROP VIEW IF EXISTS police_vehicle_together_men_view;

DROP TABLE IF EXISTS police_blacklist;
CREATE TABLE police_blacklist(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  idcard_id INT DEFAULT 0 COMMENT '身份证信息ID',
  driving_id INT DEFAULT 0 COMMENT '驾驶证信息ID',
  vehicle_id INT DEFAULT 0 COMMENT '行驶证信息ID',
  vehicle_exceptional_id INT DEFAULT 0 COMMENT '车辆稽查情况信息ID',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  recip VARCHAR(15) NOT NULL COMMENT '操作者ip',
  dateline INT NOT NULL COMMENT '数据稽查时间',
  checkuid INT DEFAULT '0' COMMENT '数据处理人uid',
  checkip VARCHAR(15) DEFAULT '0.0.0.0' COMMENT '数据处理人ip',
  checkdateline INT NOT NULL COMMENT '数据变动时间',
  status TINYINT(2) DEFAULT 1 COMMENT '启用状态：0-->禁用， 1-->启用',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='黑名单记录表';

DROP TRIGGER IF EXISTS police_blacklist_BEFORE_INSERT;
CREATE TRIGGER police_blacklist_BEFORE_INSERT BEFORE INSERT ON police_blacklist FOR EACH ROW
  BEGIN
    SET NEW.checkdateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_blacklist_AFTER_INSERT;
CREATE TRIGGER police_blacklist_AFTER_INSERT AFTER INSERT ON police_blacklist FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    IF (NEW.idcard_id <> 0) THEN
      SELECT id_number INTO @id_number FROM police_idcard WHERE police_idcard.id = NEW.idcard_id;
      UPDATE police_idcard SET is_blacklist = 1 WHERE police_idcard.id_number = @id_number ;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定身份证「', @id_number, '」黑名单数据记录！');
    ELSEIF (NEW.driving_id <> 0) THEN
      SELECT driving_license_main_number INTO @driving_id FROM police_driving_license WHERE police_driving_license.id = NEW.driving_id;
      UPDATE police_driving_license SET is_blacklist = 1 WHERE police_driving_license.driving_license_main_number = @driving_id;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定驾驶证「', @driving_id, '」黑名单数据记录！');
    ELSEIF (NEW.vehicle_id <> 0) THEN
      SELECT vehicle_license_main_plate_num INTO @vehicle_id FROM police_vehicle_license WHERE police_vehicle_license.id = NEW.vehicle_id;
      UPDATE police_vehicle_license SET is_blacklist = 1 WHERE police_vehicle_license.vehicle_license_main_plate_num = @vehicle_id;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定车辆「', @vehicle_id, '」黑名单数据记录！');
    ELSEIF (NEW.vehicle_exceptional_id <> 0) THEN
      SELECT driving_license_main_number, vehicle_license_main_plate_num INTO @driving_id, @vehicle_id 
        FROM police_vehicle_exceptional_log_view WHERE id = NEW.vehicle_exceptional_id;
      SELECT id_number INTO @id_number FROM police_vehicle_together_men_view WHERE exceptional_id = NEW.vehicle_exceptional_id;
      UPDATE police_idcard SET is_blacklist = 1 WHERE id_number = @id_number;
      UPDATE police_vehicle_exceptional_log SET is_blacklist = 1 WHERE id = NEW.vehicle_exceptional_id;
      UPDATE police_driving_license SET is_blacklist = 1 WHERE driving_license_main_number = @driving_id;
      UPDATE police_vehicle_license SET is_blacklist = 1 WHERE vehicle_license_main_plate_num = @vehicle_id;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定车辆稽查数据ID「', NEW.vehicle_exceptional_id, '」黑名单数据记录！');
    END IF ;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'blacklist', NEW.id, 'set', @info, NEW.recip, NEW.checkdateline);
  END;

DROP TRIGGER IF EXISTS police_blacklist_BEFORE_UPDATE;
CREATE TRIGGER police_blacklist_BEFORE_UPDATE BEFORE UPDATE ON police_blacklist FOR EACH ROW
  BEGIN
    IF(NEW.status <> OLD.status) THEN
      IF(NEW.status = 1) THEN
        SET @set = '设定';
      ELSE 
        SET @set = '取消设定';
      END IF;
      SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
      IF (NEW.idcard_id <> 0) THEN
        SELECT id_number INTO @id_number FROM police_idcard WHERE police_idcard.id = NEW.idcard_id;
        UPDATE police_idcard SET is_blacklist = NEW.status WHERE police_idcard.id_number = @id_number ;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '身份证「', @id_number, '」黑名单数据记录！');
      ELSEIF (NEW.driving_id <> 0) THEN
        SELECT driving_license_main_number INTO @driving_num FROM police_driving_license WHERE police_driving_license.id = NEW.driving_id;
        UPDATE police_driving_license SET is_blacklist = NEW.status WHERE police_driving_license.driving_license_main_number = @driving_num;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '驾驶证「', @driving_num, '」黑名单数据记录！');
      ELSEIF (NEW.vehicle_id <> 0) THEN
        SELECT vehicle_license_main_plate_num INTO @vehicle_num FROM police_vehicle_license WHERE police_vehicle_license.id = NEW.vehicle_id;
        UPDATE police_vehicle_license SET is_blacklist = NEW.status WHERE police_vehicle_license.vehicle_license_main_plate_num = @vehicle_num;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '车辆「', @vehicle_num, '」黑名单数据记录！');
      ELSEIF (NEW.vehicle_exceptional_id <> 0) THEN
        SELECT driving_license_main_number, vehicle_license_main_plate_num INTO @driving_id, @vehicle_id
          FROM police_vehicle_exceptional_log_view WHERE id = NEW.vehicle_exceptional_id;
        SELECT id_number INTO @id_number FROM police_vehicle_together_men_view WHERE exceptional_id = NEW.vehicle_exceptional_id;
        UPDATE police_idcard SET is_blacklist = NEW.status WHERE id_number = @id_number;
        UPDATE police_vehicle_exceptional_log SET is_blacklist = NEW.status WHERE id = NEW.vehicle_exceptional_id;
        UPDATE police_driving_license SET is_blacklist = NEW.status WHERE driving_license_main_number = @driving_id;
        UPDATE police_vehicle_license SET is_blacklist = NEW.status WHERE vehicle_license_main_plate_num = @vehicle_id;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '车辆稽查数据ID「', NEW.vehicle_exceptional_id, '」黑名单数据记录！');
      END IF ;
    ELSE
      SET @info = '用户未做任何操作!';
    END IF;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'blacklist', NEW.id, 'changeSet', @info, NEW.recip, NEW.dateline);
  END;

DROP VIEW IF EXISTS police_blacklist_view;
CREATE VIEW police_blacklist_view AS
  SELECT
    police_blacklist.id,
    police_blacklist.idcard_id,
    police_idcard.name AS idcard_realname,
    police_idcard.id_number,
    police_idcard.birthday AS idcard_birthday,
    police_idcard.sex AS idcard_sex,
    police_idcard.people,
    police_idcard.address AS idcard_address,
    police_idcard.type,
    police_idcard.headimg,
    if(police_idcard.headimg = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_idcard.headimg))
    ) AS headimg_url,
    police_idcard.idcard_1,
    if(police_idcard.idcard_1 = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_idcard.idcard_1))
    ) AS idcard_url_1,
    police_idcard.idcard_2,
    if(police_idcard.idcard_2 = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_idcard.idcard_2))
    ) AS idcard_url_2,
    police_blacklist.driving_id,
    police_driving_license.name AS driving_realname,
    police_driving_license.driving_license_main_number,
    police_driving_license.birthday AS driving_birthday,
    police_driving_license.sex AS driving_sex,
    police_driving_license.driving_license_main_nationality,
    police_driving_license.driving_license_main_valid_from,
    police_driving_license.driving_license_main_valid_end,
    police_driving_license.issue_date AS driving_issue_date,
    police_driving_license.drivetype,
    police_driving_license.driving_license_type,
    police_driving_license.driving_license,
    if(police_driving_license.driving_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_blacklist.vehicle_id))
    ) AS driving_license_url,
    police_blacklist.vehicle_id,
    police_vehicle_license.vehicle_license_main_vehicle_type,
    police_vehicle_license.vehicle_license_main_plate_num,
    police_vehicle_license.vehicle_license_main_owner,
    police_vehicle_license.address AS vehicle_address,
    police_vehicle_license.vehicle_license_main_user_character,
    police_vehicle_license.vehicle_license_main_model,
    police_vehicle_license.vehicle_license_main_vin,
    police_vehicle_license.vehicle_license_main_engine_no,
    police_vehicle_license.vehicle_license_main_register_date,
    police_vehicle_license.issue_date AS vehicle_issue_date,
    police_vehicle_license.vehicle_license,
    if(police_vehicle_license.vehicle_license = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_blacklist.vehicle_id))
    ) AS vehicle_license_url,
    police_blacklist.status
  FROM ((police_blacklist
    LEFT JOIN police_idcard ON (police_blacklist.idcard_id = police_idcard.id)
    LEFT JOIN police_driving_license ON (police_blacklist.driving_id = police_driving_license.id))
    LEFT JOIN police_vehicle_license ON (police_blacklist.vehicle_id = police_vehicle_license.id));