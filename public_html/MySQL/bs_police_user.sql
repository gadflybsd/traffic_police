DROP TABLE IF EXISTS police_user;
CREATE TABLE police_user(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uuid VARCHAR(36) DEFAULT NULL COMMENT '用户UUID',
  police_name VARCHAR(45) DEFAULT NULL COMMENT '用户姓名',
  police_sn VARCHAR(45) DEFAULT NULL COMMENT '警官号码',
  headimg INT DEFAULT 0 COMMENT '用户头像',
  mobile VARCHAR(11) NOT NULL COMMENT '手机号码',
  password VARCHAR(41) DEFAULT NULL COMMENT '用户密码',
  clear_password VARCHAR(32) NOT NULL COMMENT '明文密码',
  register_recip VARCHAR(15) DEFAULT NULL COMMENT '用户注册时IP',
  register_dateline INT DEFAULT 0 COMMENT '用户注册时间截',
  last_changes VARCHAR(45) DEFAULT NULL COMMENT '最后一次变更类型',
  last_dateline INT DEFAULT 0 COMMENT '最后一次登录时间截',
  last_recip VARCHAR(15) NOT NULL COMMENT '最后一次操作者ip',
  last_related VARCHAR(45) DEFAULT NULL COMMENT '最后一次变更关联数据表名',
  last_pk INT DEFAULT 0 COMMENT '最后一次变更数据表主键',
  last_info TEXT DEFAULT NULL COMMENT '最后一次变更说明',
  last_remark TEXT DEFAULT NULL COMMENT '其他备注, 用于审核说明',
  status TINYINT(2) DEFAULT 1 COMMENT '用户启用状态：0-->禁用， 1-->启用',
  PRIMARY KEY (id, mobile),
  UNIQUE KEY (police_sn, mobile)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='APP用户信息表';

DROP TRIGGER IF EXISTS police_user_BEFORE_INSERT;
CREATE TRIGGER police_user_BEFORE_INSERT BEFORE INSERT ON police_user FOR EACH ROW
  BEGIN
    SET NEW.password = password(NEW.clear_password);
    SET NEW.register_dateline = UNIX_TIMESTAMP(now());
    SET NEW.last_dateline = NEW.register_dateline;
    SET NEW.register_recip = NEW.last_recip;
    SET NEW.last_related = 'user';
    SET NEW.last_changes = 'register';
    SET NEW.last_info = CONCAT('新警官用户【', NEW.mobile, '】成功注册成为用户!');
  END;

DROP TRIGGER IF EXISTS police_user_AFTER_INSERT;
CREATE TRIGGER police_user_AFTER_INSERT AFTER INSERT ON police_user FOR EACH ROW
  BEGIN
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.id, NEW.last_related, NEW.id, NEW.last_changes, NEW.last_info, NEW.last_recip, NEW.register_dateline);
  END;

DROP TRIGGER IF EXISTS police_user_BEFORE_UPDATE;
CREATE TRIGGER opolice_user_BEFORE_UPDATE BEFORE UPDATE ON police_user FOR EACH ROW
  BEGIN
    SET NEW.last_dateline = UNIX_TIMESTAMP(now());
    CASE NEW.last_changes
      WHEN 'setHeadimg' THEN
        SET NEW.last_info = '用户设定自定义头像操作！';
      WHEN 'setNickname' THEN
        SET NEW.last_info = '用户设定真实姓名操作！';
      WHEN 'setPoliceSn' THEN
        SET NEW.last_info = '用户设定警官号操作！';
      WHEN 'setPassword' THEN
        SET NEW.password = password(NEW.clear_password);
        SET NEW.last_info = '用户变更登录密码操作！';
      WHEN 'forgotPassword' THEN
        SET NEW.password = password(NEW.clear_password);
        SET NEW.last_info = '用户通过手机重置密码操作！';
      WHEN 'setMobile' THEN
        SET NEW.last_info = '用户变更警务通号码操作！';
      WHEN 'loginSystem' THEN
        SET NEW.last_info = '用户登录系统操作!';
      WHEN 'logoutSystem' THEN
        SET NEW.uuid = null;
        SET NEW.last_info = '用户退出系统操作!';
      WHEN 'changeStatus' THEN
        SET NEW.uuid = null;
        CASE NEW.status
          WHEN 0 THEN
          SET NEW.last_info = '变更用户状态为禁用操作成功！';
          WHEN 1 THEN
          SET NEW.last_info = '变更用户状态为启用操作成功！';
        ELSE
          SET NEW.last_info = '变更用户状态的其它操作！';
        END CASE;
      ELSE
        SET NEW.last_info = '系统的其他操作!';
    END CASE;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.id, 'user', NEW.id, NEW.last_changes, NEW.last_info, NEW.last_recip, NEW.last_dateline);
  END;

DROP TABLE IF EXISTS `police_rule`;
CREATE TABLE `police_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '规则类型',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证, 规则附件条件,满足附加条件的规则,才认为是有效的规则',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='认证规则表';

DROP TABLE IF EXISTS `police_group`;
CREATE TABLE `police_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `rules` char(80) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则","隔开',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户组表';

DROP TABLE IF EXISTS `police_group_access`;
CREATE TABLE `police_group_access` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

DROP TABLE IF EXISTS police_log;
CREATE TABLE police_log(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  related VARCHAR(30) DEFAULT 'user' COMMENT '关联表:face/picture/idcard/driving/vehicle/vehicle_exceptional等,如果没有则为NULL',
  related_pk INT NOT NULL COMMENT '关联表id或主键',
  changes VARCHAR(20) DEFAULT NULL COMMENT '影响操作类型',
  info TEXT DEFAULT NULL COMMENT '日志说明',
  recip VARCHAR(15) DEFAULT '0.0.0.0' COMMENT '操作者ip',
  remark TEXT DEFAULT NULL COMMENT '其他备注, 用于审核说明/充值体现说明等',
  checkuid INT DEFAULT 0 COMMENT '处理人uid',
  checkip VARCHAR(15) DEFAULT '0.0.0.0' COMMENT '处理人ip',
  dateline INT DEFAULT 0 COMMENT '数据变动时间',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='APP用户系统日志记录表';


DROP TABLE IF EXISTS police_picture;
CREATE TABLE police_picture(
  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  uid INT DEFAULT 0 COMMENT '操作警官uid',
  type VARCHAR(15) NOT NULL COMMENT '上传文件类型',
  path VARCHAR(255) DEFAULT NULL COMMENT '路径',
  md5 VARCHAR(32) DEFAULT NULL COMMENT '文件md5',
  sha1 VARCHAR(40) DEFAULT NULL COMMENT '文件 sha1编码',
  recip VARCHAR(15) NOT NULL COMMENT '操作者ip',
  status TINYINT(2) DEFAULT 0 NOT NULL COMMENT '状态',
  dateline INT DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据图片上传记录表';

DROP TABLE IF EXISTS `police_sms_log`;
CREATE TABLE `police_sms_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` INT DEFAULT 0 COMMENT '用户uid',
  `mobile` varchar(11) NOT NULL COMMENT '接收手机号码',
  `sms_type` varchar(20) DEFAULT 'Code' COMMENT '短信类型：（Code）验证码或（Temp）模板短信',
  `actions` varchar(20) DEFAULT 'register' COMMENT '用户操作类型: (register)注册或(forgot_password)找回密码',
  `code` varchar(20) NOT NULL COMMENT '短信验证码或者模板短信ID',
  `datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建的日期时间',
  `res_code` INT NOT NULL COMMENT '发送返回代码',
  `res_message` varchar(200) DEFAULT NULL COMMENT '响应信息',
  `identifier` TEXT COMMENT '随机短信标识',
  `create_at` INT NOT NULL COMMENT '短信下发的Unix时间戳',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='短信发送日志';

DROP VIEW IF EXISTS police_picture_view;
CREATE VIEW police_picture_view AS
  SELECT
    police_picture.id,
    police_picture.uid,
    police_picture.type,
    police_picture.path,
    CONCAT('http://222.219.73.36', police_picture.path) AS url,
    police_picture.md5,
    police_picture.sha1,
    police_picture.recip,
    police_picture.status,
    police_picture.dateline,
    FROM_UNIXTIME(police_picture.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_picture LEFT JOIN police_user ON ( police_picture.uid = police_user.id);

DROP VIEW IF EXISTS police_user_view;
CREATE VIEW police_user_view AS
  SELECT
    police_user.id AS uid,
    police_user.uuid,
    police_user.police_name,
    police_user.police_sn,
    police_user.headimg,
    if(police_user.headimg = 0, 'img/no-astar.jpg',
       (SELECT police_picture_view.url FROM police_picture_view WHERE (police_picture_view.id = police_user.headimg))
    ) AS headimg_url,
    police_user.mobile,
    police_user.register_recip,
    police_user.register_dateline,
    FROM_UNIXTIME(police_user.register_dateline, '%Y-%m-%d %h:%i:%s') AS register_time,
    police_user.last_dateline,
    police_user.last_recip,
    FROM_UNIXTIME(police_user.last_dateline, '%Y-%m-%d %h:%i:%s') AS last_time
  FROM police_user;

DROP VIEW IF EXISTS police_log_view;
CREATE VIEW police_log_view AS
  SELECT
    police_log.id,
    police_log.uid,
    police_user_view.police_name,
    police_user_view.police_sn,
    police_user_view.mobile AS police_mobile,
    police_user_view.headimg AS police_headimg,
    police_user_view.headimg AS police_headimg_url,
    police_log.related,
    police_log.related_pk,
    police_log.changes,
    police_log.info,
    police_log.recip,
    police_log.remark,
    police_log.checkuid,
    police_log.checkip,
    police_log.dateline,
    FROM_UNIXTIME(police_log.dateline, '%Y-%m-%d %h:%i:%s') AS create_time
  FROM police_log LEFT JOIN police_user_view ON ( police_log.uid = police_user_view.uid);

DROP TRIGGER IF EXISTS police_picture_BEFORE_INSERT;
CREATE TRIGGER police_picture_BEFORE_INSERT BEFORE INSERT ON police_picture FOR EACH ROW
  BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END;

DROP TRIGGER IF EXISTS police_picture_AFTER_INSERT;
CREATE TRIGGER police_picture_AFTER_INSERT AFTER INSERT ON police_picture FOR EACH ROW
  BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    CASE NEW.type
      WHEN 'headimg' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传头像图片!');
      WHEN 'face' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象人脸图片!');
      WHEN 'idcard' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象身份证图片!');
      WHEN 'driving' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象驾驶证图片!');
      WHEN 'vehicle' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象行驶证图片!');
      WHEN 'picture' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象嫌疑物品图片!');
    ELSE SET @info = CONCAT('警官用户【', @mobile, '】什么都没有操作!');
    END CASE;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'picture', NEW.id, NEW.type, @info, NEW.recip, NEW.dateline);
  END;

DROP PROCEDURE IF EXISTS user_register;
CREATE PROCEDURE user_register(
  IN uuid VARCHAR(50),
  IN mobile VARCHAR(11),
  IN pasd VARCHAR(50),
  IN code VARCHAR(10),
  IN recip VARCHAR(15)
)
  BEGIN
    SET @uuid = uuid;
    SET @mobile = mobile;
    SET @pasd = pasd;
    SET @code = code;
    SET @recip = recip;
    SET @msg = '警官用户登记或者注册操作成功!';
    SELECT COUNT(*) INTO @mobile_num FROM police_user WHERE police_user.mobile = @mobile;
    IF (@mobile_num = 0) THEN
      SELECT COUNT(*) INTO @num FROM police_sms_log WHERE police_sms_log.mobile = @mobile AND police_sms_log.sms_type = 'Code' AND police_sms_log.actions = 'register' AND police_sms_log.create_at > (UNIX_TIMESTAMP(now()) - 10*60);
      IF (ISNULL(@code)) THEN
        IF ((@num = 0)) THEN
          INSERT INTO police_user (uuid, mobile, clear_password, last_recip) VALUES(@uuid, @mobile, @pasd, @recip);
          SELECT 'Success' AS result, @msg AS msg, police_user_view.* FROM police_user_view WHERE police_user_view.mobile = @mobile;
        ELSE
          SELECT 0 AS uid, 'Error' AS result, '用户注册短信验证码不能为空!' AS msg;
        END IF;
      ELSE
        IF ((@num = 0)) THEN
          SELECT 0 AS uid, 'Error' AS result, '系统发送短信验证码有误, 请与管理员联系或者重新发送!' AS msg;
        ELSE
          SELECT police_sms_log.code INTO @sms_code FROM police_sms_log WHERE police_sms_log.mobile = @mobile AND police_sms_log.sms_type = 'Code' AND police_sms_log.actions = 'register' AND police_sms_log.create_at > (UNIX_TIMESTAMP(now()) - 10*60) ORDER BY create_at DESC LIMIT 1;
          IF (@code = @sms_code) THEN
            SELECT COUNT(*) INTO @mobile_num FROM police_user WHERE police_user.mobile = @mobile;
            IF (@mobile_num = 0) THEN
              INSERT INTO police_user (uuid, mobile, clear_password, last_recip) VALUES(@uuid, @mobile, @pasd, @recip);
              SELECT 'Success' AS result, @msg AS msg, police_user_view.* FROM police_user_view WHERE police_user_view.mobile = @mobile;
            ELSE
              SELECT 0 AS uid, 'Error' AS result, '系统中已经存在相同的警务通号码, 请不用重复注册!' AS msg;
            END IF;
          ELSE
            SELECT 0 AS uid, 'Error' AS result, '用户短信验证码输入错误!' AS msg;
          END IF;
        END IF;
      END IF;
    ELSE
      SELECT 0 AS uid, 'Error' AS result, '系统中已经存在相同的警务通号码, 请不用重复注册!' AS msg;
    END IF;
  END;

DROP PROCEDURE IF EXISTS user_login;
CREATE PROCEDURE user_login(
  IN uuid VARCHAR(50),
  IN mobile VARCHAR(11),
  IN pasd VARCHAR(50),
  IN recip VARCHAR(15)
)
  BEGIN
    SET @uuid = uuid;
    SET @mobile = mobile;
    SET @pasd = pasd;
    SET @recip = recip;
    SET @msg = '警官用户登录系统平台操作成功!';
    SELECT COUNT(*) INTO @num FROM police_user WHERE police_user.mobile = @mobile AND police_user.password = password(@pasd);
    IF (@num > 0) THEN
      UPDATE police_user SET uuid = @uuid, last_changes = 'loginSystem', last_recip = @recip WHERE police_user.mobile = @mobile;
      SELECT 'Success' AS result, @msg AS msg, police_user_view.* FROM police_user_view WHERE police_user_view.mobile = @mobile;
    ELSE
      SELECT 0 AS uid, 'Error' AS result, '用户警务通号码或密码输入错误!' AS msg;
    END IF;
  END;

DROP PROCEDURE IF EXISTS user_set_password;
CREATE PROCEDURE user_set_password(
  IN uid INT,
  IN old_password VARCHAR(50),
  IN new_password VARCHAR(50),
  IN recip VARCHAR(15)
)
  BEGIN
    SET @uid = uid;
    SET @old_password = old_password;
    SET @new_password = new_password;
    SET @recip = recip;
    SET @msg = '用户重新设定密码操作成功!';
    SELECT COUNT(*) INTO @num FROM police_user WHERE police_user.id = @uid AND police_user.password = password(@old_password);
    IF (@num > 0) THEN
      IF (@old_password = @new_password) THEN
        SELECT @uid AS uid, 'Error' AS result, '用户原密码与新密码相同,修改无效!' AS msg;
      ELSE
        UPDATE police_user SET clear_password = @new_password, last_changes = 'setPassword', last_recip = @recip WHERE police_user.id = @uid;
        SELECT 'Success' AS result, @msg AS msg, police_user_view.* FROM police_user_view WHERE police_user_view.uid = @uid;
      END IF;
    ELSE
      SELECT @uid AS uid, 'Error' AS result, '用户原密码输入错误!' AS msg;
    END IF;
  END;

DROP PROCEDURE IF EXISTS user_forgot_password;
CREATE PROCEDURE user_forgot_password(
  IN uid INT,
  IN code VARCHAR(6),
  IN recip VARCHAR(15)
)
  BEGIN
    SET @uid = uid;
    SET @code = code;
    SET @recip = recip;
    SET @msg = '新的初始密码是【x123456】, 请及时修改初始密码, 用户找回密码操作成功!';
    SELECT COUNT(*) INTO @num FROM police_sms_log
    WHERE police_sms_log.mobile = @mobile AND
          police_sms_log.sms_type = 'Code' AND
          police_sms_log.actions = 'register' AND
          police_sms_log.create_at > (UNIX_TIMESTAMP(now()) - 10*60);
    IF (ISNULL(@code)) THEN
      IF ((@num = 0)) THEN
        UPDATE police_user SET clear_password = 'x123456', last_changes = 'forgotPassword', last_recip = @recip WHERE police_user.id = @uid;
        SELECT 'Success' AS result, @msg AS msg, 'x123456' AS clear_password, police_user_view.* FROM police_user_view WHERE police_user_view.uid = @uid;
      ELSE
        SELECT 0 AS uid, 'Error' AS result, '用户找回密码时的短信验证码不能为空!' AS msg;
      END IF;
    ELSE
      IF ((@num = 0)) THEN
        SELECT 0 AS uid, 'Error' AS result, '系统发送短信验证码有误, 请与管理员联系或者重新发送!' AS msg;
      ELSE
        SELECT police_sms_log.code INTO @sms_code FROM police_sms_log
        WHERE police_sms_log.uid = @uid
              AND police_sms_log.sms_type = 'Code'
              AND police_sms_log.actions = 'forgot_password'
              AND police_sms_log.create_at > (UNIX_TIMESTAMP(now()) - 10*60)
        ORDER BY police_sms_log.create_at DESC LIMIT 1;
        IF (@sms_code = @code) THEN
          UPDATE police_user SET clear_password = 'x123456', last_changes = 'forgotPassword', last_recip = @recip WHERE police_user.id = @uid;
          SELECT 'Success' AS result, @msg AS msg, 'x123456' AS clear_password, police_user_view.* FROM police_user_view WHERE police_user_view.uid = @uid;
        ELSE
          SELECT @uid AS uid, 'Error' AS result, '用户验证码输入错误!' AS msg;
        END IF;
      END IF;
    END IF;
  END;

DROP PROCEDURE IF EXISTS user_set_mobile;
CREATE PROCEDURE user_set_mobile(
  IN uid INT,
  IN mobile VARCHAR(11),
  IN recip VARCHAR(15)
)
  BEGIN
    SET @uid = uid;
    SET @mobile = mobile;
    SET @recip = recip;
    SET @msg = '警官用户警务通号设定修改操作成功!';
    SELECT COUNT(*) INTO @num FROM police_user WHERE police_user.mobile = @mobile;
    IF (@num > 0) THEN
      SELECT @uid AS uid, 'Error' AS type, '新设定的警务通号码已经存在!' AS msg;
    ELSE
      UPDATE police_user SET mobile = @mobile, last_changes = 'setMobile', last_recip = @recip WHERE police_user.id = @uid;
      SELECT 'Success' AS result, @msg AS msg, police_user_view.* FROM police_user_view WHERE police_user_view.uid = @uid;
    END IF;
  END;

DROP PROCEDURE IF EXISTS user_set_police_sn;
CREATE PROCEDURE user_set_police_sn(
  IN uid INT,
  IN police_sn VARCHAR(11),
  IN recip VARCHAR(15)
)
  BEGIN
    SET @uid = uid;
    SET @police_sn = police_sn;
    SET @recip = recip;
    SET @msg = '警官用户警官号修改操作成功!';
    SELECT COUNT(*) INTO @num FROM police_user WHERE police_user.police_sn = @police_sn;
    IF (@num > 0) THEN
      SELECT @uid AS uid, 'Error' AS result, '新设定的警官号已经存在!' AS msg;
    ELSE
      UPDATE police_user SET police_sn = @police_sn, last_changes = 'setPoliceSn', last_recip = @recip WHERE police_user.id = @uid;
      SELECT 'Success' AS result, @msg AS msg, police_user_view.* FROM police_user_view WHERE police_user_view.uid = @uid;
    END IF;
  END;