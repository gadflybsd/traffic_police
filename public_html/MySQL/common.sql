DROP FUNCTION IF EXISTS get_split_string_total;
CREATE FUNCTION get_split_string_total(
  f_string varchar(1000),f_delimiter varchar(5)
) RETURNS int(11)
  BEGIN
    return 1+(length(f_string) - length(replace(f_string,f_delimiter,'')));
  END;

DROP FUNCTION IF EXISTS get_split_string;
CREATE FUNCTION get_split_string(
  f_string varchar(1000),f_delimiter varchar(5),f_order int) RETURNS varchar(255) CHARSET utf8
  BEGIN
    declare result varchar(255) default '';
    set result = reverse(substring_index(reverse(substring_index(f_string,f_delimiter,f_order)),f_delimiter,1));
    return result;
  END;

DROP PROCEDURE IF EXISTS get_split_result;
CREATE PROCEDURE get_split_result(
  IN f_string varchar(1000),
  IN f_delimiter varchar(5)
)
  BEGIN
    declare cnt int default 0;
    declare i int default 0;
    set cnt = get_split_string_total(f_string,f_delimiter);
    DROP TABLE IF EXISTS tmp_split;
    create temporary table tmp_split(id int not null);
    while i < cnt
    do
      set i = i + 1;
      Insert into tmp_split (id) values (get_split_string(f_string,f_delimiter,i));
    end while;
    SELECT * From tmp_split;
  END;

DROP FUNCTION IF EXISTS json_decode;
CREATE FUNCTION json_decode(
  in_JsonArray VARCHAR(4096),#JSON数组字符串  
  in_Index TINYINT, #JSON对象序号，序号从1开始  
  in_KeyName VARCHAR(64)#键名  
) RETURNS VARCHAR(512) CHARSET utf8
  BEGIN
    DECLARE vs_return VARCHAR(4096);
    DECLARE vs_JsonArray, vs_Json, vs_KeyName VARCHAR(4096);
    DECLARE vi_pos1, vi_pos2 SMALLINT UNSIGNED;
    SET vs_JsonArray = TRIM(in_JsonArray);
    SET vs_KeyName = TRIM(in_KeyName);
    IF vs_JsonArray = '' OR vs_JsonArray IS NULL
       OR vs_KeyName = '' OR vs_KeyName IS NULL
       OR in_Index <= 0 OR in_Index IS NULL THEN
      SET vs_return = NULL;
    ELSE
      #去掉方括号  
      SET vs_JsonArray = REPLACE(REPLACE(vs_JsonArray, '[', ''), ']', '');
      #取指定的JSON对象  
      SET vs_json = SUBSTRING_INDEX(SUBSTRING_INDEX(vs_JsonArray,'}', in_index),'}',-1);
      IF vs_json = '' OR vs_json IS NULL THEN
        SET vs_return = NULL;
      ELSE
        SET vs_KeyName = CONCAT('"', vs_KeyName, '":');
        SET vi_pos1 = INSTR(vs_json, vs_KeyName);
        IF vi_pos1 > 0 THEN
          #如果键名存在  
          SET vi_pos1 = vi_pos1 + CHAR_LENGTH(vs_KeyName);
          SET vi_pos2 = LOCATE(',', vs_json, vi_pos1);
          IF vi_pos2 = 0 THEN
            #最后一个元素没有','分隔符，也没有结束符'}'  
            SET vi_pos2 = CHAR_LENGTH(vs_json) + 1;
          END IF;
          SET vs_return = REPLACE(MID(vs_json, vi_pos1, vi_pos2 - vi_pos1), '"', '');
        END IF;
      END IF;
    END IF;
    RETURN(vs_return);
  END;

call get_split_result('1,2,44,5,666,29232',',');
select * FROM @pp;

DROP PROCEDURE IF EXISTS get_ehicle_exceptional;
CREATE PROCEDURE get_ehicle_exceptional(
  IN id INT,
  OUT ids INT
)
  BEGIN
    PREPARE stmt0 FROM 'SELECT idcard_ids, face_ids, photos INTO @idcard, @face, @photo FROM police_vehicle_exceptional_log_view WHERE id = ?';
    SET @id = id;
    EXECUTE stmt0 USING @id;
    DEALLOCATE PREPARE stmt0;
    PREPARE stmt1 FROM 'SELECT GROUP_CONCAT(idcard_1) INTO @idcard_ids FROM police_card WHERE id = ?';
    EXECUTE stmt1 USING @idcard;
    DEALLOCATE PREPARE stmt1;
    PREPARE stmt2 FROM 'SELECT GROUP_CONCAT(face_license) INTO @face_ids FROM police_face WHERE id = ?';
    EXECUTE stmt2 USING @face;
    DEALLOCATE PREPARE stmt2;
    PREPARE stmt2 FROM 'SELECT GROUP_CONCAT(face_license) INTO @face_ids FROM police_face WHERE id = ?';
    EXECUTE stmt2 USING @face;
    DEALLOCATE PREPARE stmt2;
  END;

CALL get_ehicle_exceptional(65, @pp);
select @pp;