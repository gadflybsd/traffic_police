/*
 Navicat Premium Data Transfer

 Source Server         : lisa
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost
 Source Database       : police

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : utf-8

 Date: 05/19/2017 02:23:13 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `police_blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `police_blacklist`;
CREATE TABLE `police_blacklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `idcard_id` int(11) DEFAULT '0' COMMENT '身份证信息ID',
  `driving_id` int(11) DEFAULT '0' COMMENT '驾驶证信息ID',
  `vehicle_id` int(11) DEFAULT '0' COMMENT '行驶证信息ID',
  `vehicle_exceptional_id` int(11) DEFAULT '0' COMMENT '车辆稽查情况信息ID',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `recip` varchar(15) NOT NULL COMMENT '操作者ip',
  `dateline` int(11) NOT NULL COMMENT '数据稽查时间',
  `checkuid` int(11) DEFAULT '0' COMMENT '数据处理人uid',
  `checkip` varchar(15) DEFAULT '0.0.0.0' COMMENT '数据处理人ip',
  `checkdateline` int(11) NOT NULL COMMENT '数据变动时间',
  `status` tinyint(2) DEFAULT '1' COMMENT '启用状态：0-->禁用， 1-->启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='黑名单记录表';

-- ----------------------------
--  Table structure for `police_driving_license`
-- ----------------------------
DROP TABLE IF EXISTS `police_driving_license`;
CREATE TABLE `police_driving_license` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `name` varchar(20) NOT NULL COMMENT '真实姓名',
  `driving_license_main_number` varchar(18) NOT NULL COMMENT '驾驶证号码',
  `birthday` varchar(18) DEFAULT NULL COMMENT '出生年月日',
  `sex` varchar(6) DEFAULT NULL COMMENT '性别',
  `driving_license_main_nationality` varchar(20) DEFAULT NULL COMMENT '国籍',
  `driving_license_main_valid_from` varchar(100) DEFAULT NULL COMMENT '有效期限',
  `driving_license_main_valid_end` date DEFAULT NULL COMMENT '到期日期',
  `issue_date` varchar(18) DEFAULT NULL COMMENT '初次领证日期',
  `drivetype` varchar(20) NOT NULL COMMENT '准驾类型',
  `driving_license_type` varchar(100) NOT NULL COMMENT '驾驶证证件名称',
  `driving_license` int(11) DEFAULT '0' COMMENT '驾驶证图片地址',
  `recip` varchar(15) DEFAULT NULL COMMENT '记录创建者ip',
  `longitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  `latitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  `is_blacklist` tinyint(2) DEFAULT '0' COMMENT '是否列入黑名单',
  `dateline` int(11) DEFAULT '0' COMMENT '记录创建时间截',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='驾驶证数据记录表';

-- ----------------------------
--  Table structure for `police_face`
-- ----------------------------
DROP TABLE IF EXISTS `police_face`;
CREATE TABLE `police_face` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `id_number` varchar(18) DEFAULT NULL COMMENT '身份证号码',
  `face_headimg` text COMMENT '人脸图片Base64',
  `face_license` int(11) DEFAULT '0' COMMENT '人脸图片地址',
  `resultcount` int(11) DEFAULT '0' COMMENT '匹配人脸个数',
  `result` text COMMENT '人脸匹配结果集',
  `recip` varchar(15) DEFAULT NULL COMMENT '记录创建者ip',
  `longitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  `latitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  `is_blacklist` tinyint(2) DEFAULT '0' COMMENT '是否列入黑名单',
  `dateline` int(11) DEFAULT '0' COMMENT '记录创建时间截',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='人脸数据记录表';

-- ----------------------------
--  Table structure for `police_idcard`
-- ----------------------------
DROP TABLE IF EXISTS `police_idcard`;
CREATE TABLE `police_idcard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `name` varchar(20) NOT NULL COMMENT '真实姓名',
  `id_number` varchar(18) NOT NULL COMMENT '身份证号码',
  `birthday` varchar(18) DEFAULT NULL COMMENT '出生年月日',
  `sex` varchar(6) DEFAULT NULL COMMENT '性别',
  `people` varchar(20) DEFAULT NULL COMMENT '民族',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `type` varchar(6) DEFAULT NULL COMMENT '身份证类型',
  `headimg` text COMMENT '身份证头像',
  `idcard_1` int(11) DEFAULT '0' COMMENT '身份证正面图片地址',
  `idcard_2` int(11) DEFAULT '0' COMMENT '身份证背面图片地址',
  `recip` varchar(15) DEFAULT NULL COMMENT '记录创建者ip',
  `longitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  `latitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  `is_blacklist` tinyint(2) DEFAULT '0' COMMENT '是否列入黑名单',
  `dateline` int(11) DEFAULT '0' COMMENT '记录创建时间截',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='身份证数据记录表';

-- ----------------------------
--  Table structure for `police_log`
-- ----------------------------
DROP TABLE IF EXISTS `police_log`;
CREATE TABLE `police_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `related` varchar(30) DEFAULT 'user' COMMENT '关联表:face/picture/idcard/driving/vehicle/vehicle_exceptional等,如果没有则为NULL',
  `related_pk` int(11) NOT NULL COMMENT '关联表id或主键',
  `changes` varchar(20) DEFAULT NULL COMMENT '影响操作类型',
  `info` text COMMENT '日志说明',
  `recip` varchar(15) DEFAULT '0.0.0.0' COMMENT '操作者ip',
  `remark` text COMMENT '其他备注, 用于审核说明/充值体现说明等',
  `checkuid` int(11) DEFAULT '0' COMMENT '处理人uid',
  `checkip` varchar(15) DEFAULT '0.0.0.0' COMMENT '处理人ip',
  `dateline` int(11) DEFAULT '0' COMMENT '数据变动时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='APP用户系统日志记录表';

-- ----------------------------
--  Records of `police_log`
-- ----------------------------
BEGIN;
INSERT INTO `police_log` VALUES ('26', '1', 'user', '1', 'register', '新警官用户【18100882834】成功注册成为用户!', '127.0.0.1', null, '0', '0.0.0.0', '1495131508'), ('27', '2', 'user', '2', 'register', '新警官用户【15911734054】成功注册成为用户!', '127.0.0.1', null, '0', '0.0.0.0', '1495131508');
COMMIT;

-- ----------------------------
--  Table structure for `police_picture`
-- ----------------------------
DROP TABLE IF EXISTS `police_picture`;
CREATE TABLE `police_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `type` varchar(15) NOT NULL COMMENT '上传文件类型',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `url` varchar(255) DEFAULT NULL COMMENT '图片链接',
  `md5` varchar(32) DEFAULT NULL COMMENT '文件md5',
  `sha1` varchar(40) DEFAULT NULL COMMENT '文件 sha1编码',
  `recip` varchar(15) NOT NULL COMMENT '操作者ip',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='数据图片上传记录表';

-- ----------------------------
--  Records of `police_picture`
-- ----------------------------
BEGIN;
INSERT INTO `police_picture` VALUES ('13', '1', 'idcard', '/Uploads/Picture/2017-05-19/591de3fe69fa7.png', 'http://tp.cn/Uploads/Picture/2017-05-19/591de3fe69fa7.png', 'c42d7879e9831ed501bca1008334d3f8', 'b4763694066229d443cdf88ec98f7761ca7d5b9a', '127.0.0.1', '1', '1495131134'), ('12', '1', 'driving', '/Uploads/Picture/2017-05-19/591de3e1166d3.png', 'http://tp.cn/Uploads/Picture/2017-05-19/591de3e1166d3.png', '8a4d35d6a497b20833c9a2c65124bcaa', '0a9ebe178e11e8d00283d32ee75bd94d37e14bbc', '127.0.0.1', '1', '1495131105');
COMMIT;

-- ----------------------------
--  Table structure for `police_user`
-- ----------------------------
DROP TABLE IF EXISTS `police_user`;
CREATE TABLE `police_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uuid` varchar(36) DEFAULT NULL COMMENT '用户UUID',
  `police_name` varchar(45) DEFAULT NULL COMMENT '用户姓名',
  `police_sn` varchar(45) DEFAULT NULL COMMENT '警官号码',
  `headimg` int(11) DEFAULT '0' COMMENT '用户头像',
  `mobile` varchar(11) NOT NULL COMMENT '手机号码',
  `password` varchar(41) DEFAULT NULL COMMENT '用户密码',
  `clear_password` varchar(32) NOT NULL COMMENT '明文密码',
  `register_recip` varchar(15) DEFAULT NULL COMMENT '用户注册时IP',
  `register_dateline` int(11) DEFAULT NULL COMMENT '用户注册时间截',
  `last_changes` varchar(45) DEFAULT NULL COMMENT '最后一次变更类型',
  `last_recip` varchar(15) NOT NULL COMMENT '最后一次操作者ip',
  `last_related` varchar(45) DEFAULT NULL COMMENT '最后一次变更关联数据表名',
  `last_pk` int(11) DEFAULT '0' COMMENT '最后一次变更数据表主键',
  `last_info` text COMMENT '最后一次变更说明',
  `last_remark` text COMMENT '其他备注, 用于审核说明',
  `status` tinyint(2) DEFAULT '1' COMMENT '用户启用状态：0-->禁用， 1-->启用',
  PRIMARY KEY (`id`,`mobile`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='APP用户信息表';

-- ----------------------------
--  Records of `police_user`
-- ----------------------------
BEGIN;
INSERT INTO `police_user` VALUES ('1', '1234566690987654321', '刘小思', null, '0', '18100882834', '*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9', '123456', '127.0.0.1', '1495131508', 'register', '127.0.0.1', 'user', '0', '新警官用户【18100882834】成功注册成为用户!', null, '1'), ('2', '1234567890987654321', '刘思思', null, '0', '15911734054', '*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9', '123456', '127.0.0.1', '1495131508', 'register', '127.0.0.1', 'user', '0', '新警官用户【15911734054】成功注册成为用户!', null, '1');
COMMIT;

-- ----------------------------
--  Table structure for `police_vehicle_exceptional_log`
-- ----------------------------
DROP TABLE IF EXISTS `police_vehicle_exceptional_log`;
CREATE TABLE `police_vehicle_exceptional_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `driving_id` int(11) DEFAULT '0' COMMENT '驾驶证信息ID',
  `vehicle_id` int(11) DEFAULT '0' COMMENT '行驶证信息ID',
  `idcard_ids` text COMMENT '随车人员ID,多张以,分割',
  `face_ids` text COMMENT '随车人员人脸ID,多张以,分割',
  `photos` text COMMENT '可疑物品照片ID,多张以,分割',
  `description` text COMMENT '异常情况描述',
  `longitude` decimal(18,13) DEFAULT NULL COMMENT '异常情况数据记录经度值',
  `latitude` decimal(18,13) DEFAULT NULL COMMENT '异常情况数据记录维度值',
  `recip` varchar(15) DEFAULT NULL COMMENT '记录创建者ip',
  `is_blacklist` tinyint(2) DEFAULT '0' COMMENT '是否列入黑名单',
  `dateline` int(11) DEFAULT '0' COMMENT '记录创建时间截',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='车辆稽查情况数据记录表';

-- ----------------------------
--  Table structure for `police_vehicle_license`
-- ----------------------------
DROP TABLE IF EXISTS `police_vehicle_license`;
CREATE TABLE `police_vehicle_license` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `uid` int(11) DEFAULT '0' COMMENT '操作警官uid',
  `vehicle_license_main_vehicle_type` varchar(20) NOT NULL COMMENT '车辆类型',
  `vehicle_license_main_plate_num` varchar(10) NOT NULL COMMENT '车辆号牌号码',
  `vehicle_license_main_owner` varchar(20) NOT NULL COMMENT '车辆所有人',
  `address` varchar(200) DEFAULT NULL COMMENT '住址',
  `vehicle_license_main_user_character` varchar(50) DEFAULT NULL COMMENT '使用性质',
  `vehicle_license_main_model` varchar(50) DEFAULT NULL COMMENT '品牌型号',
  `vehicle_license_main_vin` varchar(50) DEFAULT NULL COMMENT '车辆识别代号',
  `vehicle_license_main_engine_no` varchar(50) DEFAULT NULL COMMENT '发动机号码',
  `vehicle_license_main_register_date` date DEFAULT NULL COMMENT '注册日期',
  `issue_date` varchar(18) DEFAULT NULL COMMENT '初次领证日期',
  `vehicle_license` int(11) DEFAULT '0' COMMENT '驾驶证图片地址',
  `recip` varchar(15) DEFAULT NULL COMMENT '记录创建者ip',
  `longitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点经度值',
  `latitude` decimal(18,13) DEFAULT NULL COMMENT '数据记录点维度值',
  `is_blacklist` tinyint(2) DEFAULT '0' COMMENT '是否列入黑名单',
  `dateline` int(11) DEFAULT '0' COMMENT '记录创建时间截',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='行驶证数据记录表';

-- ----------------------------
--  View structure for `police_blacklist_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_blacklist_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_blacklist_view` AS select `police_blacklist`.`id` AS `id`,`police_blacklist`.`idcard_id` AS `idcard_id`,`police_idcard`.`name` AS `idcard_realname`,`police_idcard`.`id_number` AS `id_number`,`police_idcard`.`birthday` AS `idcard_birthday`,`police_idcard`.`sex` AS `idcard_sex`,`police_idcard`.`people` AS `people`,`police_idcard`.`address` AS `idcard_address`,`police_idcard`.`type` AS `type`,`police_idcard`.`headimg` AS `headimg`,if((`police_idcard`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_idcard`.`headimg`))) AS `headimg_url`,`police_idcard`.`idcard_1` AS `idcard_1`,if((`police_idcard`.`idcard_1` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_idcard`.`idcard_1`))) AS `idcard_url_1`,`police_idcard`.`idcard_2` AS `idcard_2`,if((`police_idcard`.`idcard_2` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_idcard`.`idcard_2`))) AS `idcard_url_2`,`police_blacklist`.`driving_id` AS `driving_id`,`police_driving_license`.`name` AS `driving_realname`,`police_driving_license`.`driving_license_main_number` AS `driving_license_main_number`,`police_driving_license`.`birthday` AS `driving_birthday`,`police_driving_license`.`sex` AS `driving_sex`,`police_driving_license`.`driving_license_main_nationality` AS `driving_license_main_nationality`,`police_driving_license`.`driving_license_main_valid_from` AS `driving_license_main_valid_from`,`police_driving_license`.`driving_license_main_valid_end` AS `driving_license_main_valid_end`,`police_driving_license`.`issue_date` AS `driving_issue_date`,`police_driving_license`.`drivetype` AS `drivetype`,`police_driving_license`.`driving_license_type` AS `driving_license_type`,`police_driving_license`.`driving_license` AS `driving_license`,if((`police_driving_license`.`driving_license` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_blacklist`.`vehicle_id`))) AS `driving_license_url`,`police_blacklist`.`vehicle_id` AS `vehicle_id`,`police_vehicle_license`.`vehicle_license_main_vehicle_type` AS `vehicle_license_main_vehicle_type`,`police_vehicle_license`.`vehicle_license_main_plate_num` AS `vehicle_license_main_plate_num`,`police_vehicle_license`.`vehicle_license_main_owner` AS `vehicle_license_main_owner`,`police_vehicle_license`.`address` AS `vehicle_address`,`police_vehicle_license`.`vehicle_license_main_user_character` AS `vehicle_license_main_user_character`,`police_vehicle_license`.`vehicle_license_main_model` AS `vehicle_license_main_model`,`police_vehicle_license`.`vehicle_license_main_vin` AS `vehicle_license_main_vin`,`police_vehicle_license`.`vehicle_license_main_engine_no` AS `vehicle_license_main_engine_no`,`police_vehicle_license`.`vehicle_license_main_register_date` AS `vehicle_license_main_register_date`,`police_vehicle_license`.`issue_date` AS `vehicle_issue_date`,`police_vehicle_license`.`vehicle_license` AS `vehicle_license`,if((`police_vehicle_license`.`vehicle_license` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_blacklist`.`vehicle_id`))) AS `vehicle_license_url`,`police_blacklist`.`status` AS `status` from (((`police_blacklist` left join `police_idcard` on((`police_blacklist`.`idcard_id` = `police_idcard`.`id`))) left join `police_driving_license` on((`police_blacklist`.`driving_id` = `police_driving_license`.`id`))) left join `police_vehicle_license` on((`police_blacklist`.`vehicle_id` = `police_vehicle_license`.`id`)));

-- ----------------------------
--  View structure for `police_driving_license_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_driving_license_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_driving_license_view` AS select `police_driving_license`.`id` AS `id`,`police_driving_license`.`uid` AS `uid`,`police_driving_license`.`name` AS `name`,`police_driving_license`.`driving_license_main_number` AS `driving_license_main_number`,`police_driving_license`.`birthday` AS `birthday`,`police_driving_license`.`sex` AS `sex`,`police_driving_license`.`driving_license_main_nationality` AS `driving_license_main_nationality`,`police_driving_license`.`driving_license_main_valid_from` AS `driving_license_main_valid_from`,`police_driving_license`.`driving_license_main_valid_end` AS `driving_license_main_valid_end`,`police_driving_license`.`issue_date` AS `issue_date`,`police_driving_license`.`drivetype` AS `drivetype`,`police_driving_license`.`driving_license_type` AS `driving_license_type`,`police_driving_license`.`driving_license` AS `driving_license`,if((`police_driving_license`.`driving_license` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_driving_license`.`driving_license`))) AS `driving_license_url`,`police_user`.`police_name` AS `police_name`,`police_user`.`police_sn` AS `police_sn`,`police_user`.`mobile` AS `police_mobile`,`police_user`.`headimg` AS `police_headimg`,if((`police_user`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_user`.`headimg`))) AS `police_headimg_url`,`police_driving_license`.`recip` AS `recip`,`police_driving_license`.`longitude` AS `longitude`,`police_driving_license`.`latitude` AS `latitude`,`police_driving_license`.`dateline` AS `dateline`,date_format(from_unixtime(`police_driving_license`.`dateline`),'%Y-%m-%d %h:%i:%s') AS `create_time` from (`police_driving_license` left join `police_user` on((`police_driving_license`.`uid` = `police_user`.`id`)));

-- ----------------------------
--  View structure for `police_face_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_face_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_face_view` AS select `police_face`.`id` AS `id`,`police_face`.`uid` AS `uid`,`police_face`.`name` AS `realname`,`police_face`.`id_number` AS `id_number`,`police_face`.`face_headimg` AS `face_headimg`,if((`police_face`.`face_headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_face`.`face_headimg`))) AS `face_headimg_url`,`police_face`.`resultcount` AS `resultcount`,`police_face`.`result` AS `result`,`police_user`.`police_name` AS `police_name`,`police_user`.`police_sn` AS `police_sn`,`police_user`.`mobile` AS `police_mobile`,`police_user`.`headimg` AS `police_headimg`,if((`police_user`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_user`.`headimg`))) AS `police_headimg_url`,`police_face`.`recip` AS `recip`,`police_face`.`longitude` AS `longitude`,`police_face`.`latitude` AS `latitude`,`police_face`.`dateline` AS `dateline`,date_format(from_unixtime(`police_face`.`dateline`),'%Y-%m-%d %h:%i:%s') AS `create_time` from (`police_face` left join `police_user` on((`police_face`.`uid` = `police_user`.`id`)));

-- ----------------------------
--  View structure for `police_idcard_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_idcard_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_idcard_view` AS select `police_idcard`.`id` AS `id`,`police_idcard`.`uid` AS `uid`,`police_idcard`.`name` AS `realname`,`police_idcard`.`id_number` AS `id_number`,`police_idcard`.`birthday` AS `birthday`,`police_idcard`.`sex` AS `sex`,`police_idcard`.`people` AS `people`,`police_idcard`.`address` AS `address`,`police_idcard`.`type` AS `type`,`police_idcard`.`headimg` AS `headimg`,if((`police_idcard`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_idcard`.`headimg`))) AS `headimg_url`,`police_idcard`.`idcard_1` AS `idcard_1`,if((`police_idcard`.`idcard_1` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_idcard`.`idcard_1`))) AS `idcard_url_1`,`police_idcard`.`idcard_2` AS `idcard_2`,if((`police_idcard`.`idcard_2` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_idcard`.`idcard_2`))) AS `idcard_url_2`,`police_user`.`police_name` AS `police_name`,`police_user`.`police_sn` AS `police_sn`,`police_user`.`mobile` AS `police_mobile`,`police_user`.`headimg` AS `police_headimg`,if((`police_user`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_user`.`headimg`))) AS `police_headimg_url`,`police_idcard`.`recip` AS `recip`,`police_idcard`.`longitude` AS `longitude`,`police_idcard`.`latitude` AS `latitude`,`police_idcard`.`dateline` AS `dateline`,date_format(from_unixtime(`police_idcard`.`dateline`),'%Y-%m-%d %h:%i:%s') AS `create_time` from (`police_idcard` left join `police_user` on((`police_idcard`.`uid` = `police_user`.`id`)));

-- ----------------------------
--  View structure for `police_log_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_log_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_log_view` AS select `police_log`.`id` AS `id`,`police_log`.`uid` AS `uid`,`police_user`.`police_name` AS `police_name`,`police_user`.`police_sn` AS `police_sn`,`police_user`.`mobile` AS `police_mobile`,`police_user`.`headimg` AS `police_headimg`,if((`police_user`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_user`.`headimg`))) AS `police_headimg_url`,`police_log`.`related` AS `related`,`police_log`.`related_pk` AS `related_pk`,`police_log`.`changes` AS `changes`,`police_log`.`info` AS `info`,`police_log`.`recip` AS `recip`,`police_log`.`remark` AS `remark`,`police_log`.`checkuid` AS `checkuid`,`police_log`.`checkip` AS `checkip`,`police_log`.`dateline` AS `dateline`,date_format(from_unixtime(`police_log`.`dateline`),'%Y-%m-%d %h:%i:%s') AS `create_time` from (`police_log` left join `police_user` on((`police_log`.`uid` = `police_user`.`id`)));

-- ----------------------------
--  View structure for `police_vehicle_exceptional_log_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_vehicle_exceptional_log_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_vehicle_exceptional_log_view` AS select `police_vehicle_exceptional_log`.`id` AS `id`,`police_vehicle_exceptional_log`.`uid` AS `uid`,`police_vehicle_exceptional_log`.`driving_id` AS `driving_id`,`police_driving_license`.`name` AS `realname`,`police_driving_license`.`driving_license_main_number` AS `driving_license_main_number`,`police_driving_license`.`birthday` AS `birthday`,`police_driving_license`.`sex` AS `sex`,`police_driving_license`.`driving_license_main_nationality` AS `driving_license_main_nationality`,`police_driving_license`.`driving_license_main_valid_from` AS `driving_license_main_valid_from`,`police_driving_license`.`driving_license_main_valid_end` AS `driving_license_main_valid_end`,`police_driving_license`.`issue_date` AS `driving_issue_date`,`police_driving_license`.`drivetype` AS `drivetype`,`police_driving_license`.`driving_license_type` AS `driving_license_type`,if((`police_driving_license`.`driving_license` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_driving_license`.`driving_license`))) AS `driving_license_url`,`police_vehicle_exceptional_log`.`vehicle_id` AS `vehicle_id`,`police_vehicle_license`.`vehicle_license_main_vehicle_type` AS `vehicle_license_main_vehicle_type`,`police_vehicle_license`.`vehicle_license_main_plate_num` AS `vehicle_license_main_plate_num`,`police_vehicle_license`.`vehicle_license_main_owner` AS `vehicle_license_main_owner`,`police_vehicle_license`.`address` AS `address`,`police_vehicle_license`.`vehicle_license_main_user_character` AS `vehicle_license_main_user_character`,`police_vehicle_license`.`vehicle_license_main_model` AS `vehicle_license_main_model`,`police_vehicle_license`.`vehicle_license_main_vin` AS `vehicle_license_main_vin`,`police_vehicle_license`.`vehicle_license_main_engine_no` AS `vehicle_license_main_engine_no`,`police_vehicle_license`.`vehicle_license_main_register_date` AS `vehicle_license_main_register_date`,`police_vehicle_license`.`issue_date` AS `vehicle_issue_date`,`police_vehicle_license`.`vehicle_license` AS `vehicle_license`,if((`police_vehicle_license`.`vehicle_license` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_vehicle_exceptional_log`.`vehicle_id`))) AS `vehicle_license_url`,`police_vehicle_exceptional_log`.`idcard_ids` AS `idcard_ids`,if(isnull(`police_vehicle_exceptional_log`.`idcard_ids`),'img/no-astar.jpg',(select group_concat(`police_picture`.`url` separator ',') from `police_picture` where (`police_picture`.`id` = `police_vehicle_exceptional_log`.`idcard_ids`))) AS `idcards_url`,`police_vehicle_exceptional_log`.`face_ids` AS `face_ids`,if(isnull(`police_vehicle_exceptional_log`.`face_ids`),'img/no-astar.jpg',(select group_concat(`police_picture`.`url` separator ',') from `police_picture` where (`police_picture`.`id` = `police_vehicle_exceptional_log`.`face_ids`))) AS `faces_url`,`police_vehicle_exceptional_log`.`photos` AS `photos`,if(isnull(`police_vehicle_exceptional_log`.`photos`),'img/no-astar.jpg',(select group_concat(`police_picture`.`url` separator ',') from `police_picture` where (`police_picture`.`id` = `police_vehicle_exceptional_log`.`photos`))) AS `photos_url`,`police_vehicle_exceptional_log`.`description` AS `description`,`police_user`.`police_name` AS `police_name`,`police_user`.`police_sn` AS `police_sn`,`police_user`.`mobile` AS `police_mobile`,`police_user`.`headimg` AS `police_headimg`,if((`police_user`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_user`.`headimg`))) AS `police_headimg_url`,`police_vehicle_exceptional_log`.`longitude` AS `longitude`,`police_vehicle_exceptional_log`.`latitude` AS `latitude`,`police_vehicle_exceptional_log`.`recip` AS `recip`,`police_vehicle_exceptional_log`.`dateline` AS `dateline`,date_format(from_unixtime(`police_vehicle_license`.`dateline`),'%Y-%m-%d %h:%i:%s') AS `create_time` from (((`police_vehicle_exceptional_log` left join `police_user` on((`police_vehicle_exceptional_log`.`uid` = `police_user`.`id`))) left join `police_driving_license` on((`police_vehicle_exceptional_log`.`driving_id` = `police_driving_license`.`id`))) left join `police_vehicle_license` on((`police_vehicle_exceptional_log`.`vehicle_id` = `police_vehicle_license`.`id`)));

-- ----------------------------
--  View structure for `police_vehicle_license_view`
-- ----------------------------
DROP VIEW IF EXISTS `police_vehicle_license_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`police`@`localhost` SQL SECURITY DEFINER VIEW `police_vehicle_license_view` AS select `police_vehicle_license`.`id` AS `id`,`police_vehicle_license`.`uid` AS `uid`,`police_vehicle_license`.`vehicle_license_main_vehicle_type` AS `vehicle_license_main_vehicle_type`,`police_vehicle_license`.`vehicle_license_main_plate_num` AS `vehicle_license_main_plate_num`,`police_vehicle_license`.`vehicle_license_main_owner` AS `vehicle_license_main_owner`,`police_vehicle_license`.`address` AS `address`,`police_vehicle_license`.`vehicle_license_main_user_character` AS `vehicle_license_main_user_character`,`police_vehicle_license`.`vehicle_license_main_model` AS `vehicle_license_main_model`,`police_vehicle_license`.`vehicle_license_main_vin` AS `vehicle_license_main_vin`,`police_vehicle_license`.`vehicle_license_main_engine_no` AS `vehicle_license_main_engine_no`,`police_vehicle_license`.`vehicle_license_main_register_date` AS `vehicle_license_main_register_date`,`police_vehicle_license`.`issue_date` AS `issue_date`,`police_vehicle_license`.`vehicle_license` AS `vehicle_license`,if((`police_vehicle_license`.`vehicle_license` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_vehicle_license`.`vehicle_license`))) AS `vehicle_license_url`,`police_user`.`police_name` AS `police_name`,`police_user`.`police_sn` AS `police_sn`,`police_user`.`mobile` AS `police_mobile`,`police_user`.`headimg` AS `police_headimg`,if((`police_user`.`headimg` = 0),'img/no-astar.jpg',(select `police_picture`.`url` from `police_picture` where (`police_picture`.`id` = `police_user`.`headimg`))) AS `police_headimg_url`,`police_vehicle_license`.`recip` AS `recip`,`police_vehicle_license`.`longitude` AS `longitude`,`police_vehicle_license`.`latitude` AS `latitude`,`police_vehicle_license`.`dateline` AS `dateline`,date_format(from_unixtime(`police_vehicle_license`.`dateline`),'%Y-%m-%d %h:%i:%s') AS `create_time` from (`police_vehicle_license` left join `police_user` on((`police_vehicle_license`.`uid` = `police_user`.`id`)));

-- ----------------------------
--  Triggers structure for table police_blacklist
-- ----------------------------
DROP TRIGGER IF EXISTS `police_blacklist_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_blacklist_BEFORE_INSERT` BEFORE INSERT ON `police_blacklist` FOR EACH ROW BEGIN
    SET NEW.checkdateline = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_blacklist_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_blacklist_AFTER_INSERT` AFTER INSERT ON `police_blacklist` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    IF (NEW.idcard_id <> 0) THEN
      SELECT id_number INTO @id_number FROM police_idcard WHERE police_idcard.id = NEW.idcard_id;
      UPDATE police_idcard SET is_blacklist = 1 WHERE police_idcard.id_number = @id_number ;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定身份证「', @id_number, '」黑名单数据记录！');
    ELSEIF (NEW.driving_id <> 0) THEN
      SELECT driving_license_main_number INTO @driving_id FROM police_driving_license WHERE police_driving_license.id = NEW.driving_id;
      UPDATE police_driving_license SET is_blacklist = 1 WHERE police_driving_license.driving_license_main_number = @driving_id;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定驾驶证「', @driving_id, '」黑名单数据记录！');
    ELSEIF (NEW.vehicle_id <> 0) THEN
      SELECT vehicle_license_main_plate_num INTO @vehicle_id FROM police_vehicle_license WHERE police_vehicle_license.id = NEW.vehicle_id;
      UPDATE police_vehicle_license SET is_blacklist = 1 WHERE police_vehicle_license.vehicle_license_main_plate_num = @vehicle_id;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定车辆「', @vehicle_id, '」黑名单数据记录！');
    ELSEIF (NEW.vehicle_exceptional_id <> 0) THEN
      SELECT driving_license_main_number, vehicle_license_main_plate_num INTO @driving_id, @vehicle_id 
        FROM police_vehicle_exceptional_log_view WHERE id = NEW.vehicle_exceptional_id;
      SELECT id_number INTO @id_number FROM police_vehicle_together_men_view WHERE exceptional_id = NEW.vehicle_exceptional_id;
      UPDATE police_idcard SET is_blacklist = 1 WHERE id_number = @id_number;
      UPDATE police_vehicle_exceptional_log SET is_blacklist = 1 WHERE id = NEW.vehicle_exceptional_id;
      UPDATE police_driving_license SET is_blacklist = 1 WHERE driving_license_main_number = @driving_id;
      UPDATE police_vehicle_license SET is_blacklist = 1 WHERE vehicle_license_main_plate_num = @vehicle_id;
      SET @info = CONCAT('警官用户【', @mobile, '】成功设定车辆稽查数据ID「', NEW.vehicle_exceptional_id, '」黑名单数据记录！');
    END IF ;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'blacklist', NEW.id, 'set', @info, NEW.recip, NEW.checkdateline);
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_blacklist_BEFORE_UPDATE`;
delimiter ;;
CREATE TRIGGER `police_blacklist_BEFORE_UPDATE` BEFORE UPDATE ON `police_blacklist` FOR EACH ROW BEGIN
    IF(NEW.status <> OLD.status) THEN
      IF(NEW.status = 1) THEN
        SET @set = '设定';
      ELSE 
        SET @set = '取消设定';
      END IF;
      SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
      IF (NEW.idcard_id <> 0) THEN
        SELECT id_number INTO @id_number FROM police_idcard WHERE police_idcard.id = NEW.idcard_id;
        UPDATE police_idcard SET is_blacklist = NEW.status WHERE police_idcard.id_number = @id_number ;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '身份证「', @id_number, '」黑名单数据记录！');
      ELSEIF (NEW.driving_id <> 0) THEN
        SELECT driving_license_main_number INTO @driving_num FROM police_driving_license WHERE police_driving_license.id = NEW.driving_id;
        UPDATE police_driving_license SET is_blacklist = NEW.status WHERE police_driving_license.driving_license_main_number = @driving_num;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '驾驶证「', @driving_num, '」黑名单数据记录！');
      ELSEIF (NEW.vehicle_id <> 0) THEN
        SELECT vehicle_license_main_plate_num INTO @vehicle_num FROM police_vehicle_license WHERE police_vehicle_license.id = NEW.vehicle_id;
        UPDATE police_vehicle_license SET is_blacklist = NEW.status WHERE police_vehicle_license.vehicle_license_main_plate_num = @vehicle_num;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '车辆「', @vehicle_num, '」黑名单数据记录！');
      ELSEIF (NEW.vehicle_exceptional_id <> 0) THEN
        SELECT driving_license_main_number, vehicle_license_main_plate_num INTO @driving_id, @vehicle_id
          FROM police_vehicle_exceptional_log_view WHERE id = NEW.vehicle_exceptional_id;
        SELECT id_number INTO @id_number FROM police_vehicle_together_men_view WHERE exceptional_id = NEW.vehicle_exceptional_id;
        UPDATE police_idcard SET is_blacklist = NEW.status WHERE id_number = @id_number;
        UPDATE police_vehicle_exceptional_log SET is_blacklist = NEW.status WHERE id = NEW.vehicle_exceptional_id;
        UPDATE police_driving_license SET is_blacklist = NEW.status WHERE driving_license_main_number = @driving_id;
        UPDATE police_vehicle_license SET is_blacklist = NEW.status WHERE vehicle_license_main_plate_num = @vehicle_id;
        SET @info = CONCAT('警官用户【', @mobile, '】成功', @set, '车辆稽查数据ID「', NEW.vehicle_exceptional_id, '」黑名单数据记录！');
      END IF ;
    ELSE
      SET @info = '用户未做任何操作!';
    END IF;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'blacklist', NEW.id, 'changeSet', @info, NEW.recip, NEW.dateline);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_driving_license
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_driving_license_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_driving_license_BEFORE_INSERT` BEFORE INSERT ON `police_driving_license` FOR EACH ROW BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_driving_license_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_driving_license_AFTER_INSERT` AFTER INSERT ON `police_driving_license` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的驾驶证「', NEW.driving_license_main_number, '」图片成功进行OCR证件识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'ocr', NEW.id, 'driving', @info, NEW.recip, NEW.dateline);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_face
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_face_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_face_BEFORE_INSERT` BEFORE INSERT ON `police_face` FOR EACH ROW BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_face_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_face_AFTER_INSERT` AFTER INSERT ON `police_face` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的人脸图片成功进行OCR识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'face', NEW.id, 'ocr', @info, NEW.recip, NEW.dateline);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_idcard
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_idcard_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_idcard_BEFORE_INSERT` BEFORE INSERT ON `police_idcard` FOR EACH ROW BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_idcard_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_idcard_AFTER_INSERT` AFTER INSERT ON `police_idcard` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的身份证「', NEW.id_number, '」图片成功进行OCR证件识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'ocr', NEW.id, 'idcard', @info, NEW.recip, NEW.dateline);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_picture
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_picture_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_picture_BEFORE_INSERT` BEFORE INSERT ON `police_picture` FOR EACH ROW BEGIN
    SET NEW.create_time = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_picture_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_picture_AFTER_INSERT` AFTER INSERT ON `police_picture` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    CASE NEW.type 
      WHEN 'headimg' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传头像图片!');
      WHEN 'face' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象人脸图片!');
      WHEN 'idcard' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象身份证图片!');
      WHEN 'driving' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象驾驶证图片!');
      WHEN 'vehicle' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象行驶证图片!');
      WHEN 'picture' THEN SET @info = CONCAT('警官用户【', @mobile, '】成功上传稽查对象嫌疑物品图片!');
      ELSE SET @info = CONCAT('警官用户【', @mobile, '】什么都没有操作!');
    END CASE;
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'picture', NEW.id, NEW.type, @info, NEW.recip, NEW.create_time);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_user
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_user_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_user_BEFORE_INSERT` BEFORE INSERT ON `police_user` FOR EACH ROW BEGIN
    SET NEW.password = password(NEW.clear_password);
    SET NEW.register_dateline = UNIX_TIMESTAMP(now());
    SET NEW.register_recip = NEW.last_recip;
    SET NEW.last_related = 'user';
    SET NEW.last_changes = 'register';
    SET NEW.last_info = CONCAT('新警官用户【', NEW.mobile, '】成功注册成为用户!');
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_user_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_user_AFTER_INSERT` AFTER INSERT ON `police_user` FOR EACH ROW BEGIN
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.id, NEW.last_related, NEW.id, NEW.last_changes, NEW.last_info, NEW.last_recip, NEW.register_dateline);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_vehicle_exceptional_log
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_vehicle_exceptional_log_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_vehicle_exceptional_log_BEFORE_INSERT` BEFORE INSERT ON `police_vehicle_exceptional_log` FOR EACH ROW BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_vehicle_exceptional_log_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_vehicle_exceptional_log_AFTER_INSERT` AFTER INSERT ON `police_vehicle_exceptional_log` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SELECT vehicle_license_main_plate_num INTO @vehicle_num FROM police_vehicle_license WHERE police_vehicle_license.id = NEW.vehicle_id;
    SET @info = CONCAT('警官用户【', @mobile, '】成功创建车辆「', @vehicle_num, '」稽查情况数据记录！');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'exceptional', NEW.id, 'vehicle', @info, NEW.recip, NEW.dateline);
  END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table police_vehicle_license
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_vehicle_license_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `police_vehicle_license_BEFORE_INSERT` BEFORE INSERT ON `police_vehicle_license` FOR EACH ROW BEGIN
    SET NEW.dateline = UNIX_TIMESTAMP(now());
  END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `police_vehicle_license_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `police_vehicle_license_AFTER_INSERT` AFTER INSERT ON `police_vehicle_license` FOR EACH ROW BEGIN
    SELECT mobile INTO @mobile FROM police_user WHERE police_user.id = NEW.uid;
    SET @info = CONCAT('系统对警官用户【', @mobile, '】所上传的行驶证「', NEW.vehicle_license_main_plate_num, '」图片成功进行OCR证件识别!');
    INSERT police_log (uid, related, related_pk, changes, info, recip, dateline)
    VALUES (NEW.uid, 'ocr', NEW.id, 'vehicle', @info, NEW.recip, NEW.dateline);
  END
 ;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
