DROP TABLE IF EXISTS police_user;
CREATE TABLE police_user (
  id SERIAL8,
  uuid VARCHAR(36) DEFAULT NULL,
  nickname VARCHAR(45) DEFAULT NULL,
  headimg int8 DEFAULT 0,
  mobile  VARCHAR(11) NOT NULL,
  password VARCHAR(41) DEFAULT NULL,
  status SMALLINT DEFAULT 1,
  PRIMARY KEY (id),
  UNIQUE (mobile)
);
ALTER TABLE "public"."police_user" OWNER TO "postgres";
COMMENT ON COLUMN "public"."police_user"."id" IS '主键id自增';
COMMENT ON COLUMN "public"."police_user"."uuid" IS '用户UUID';
COMMENT ON COLUMN "public"."police_user"."nickname" IS '用户姓名';
COMMENT ON COLUMN "public"."police_user"."headimg" IS '用户头像';
COMMENT ON COLUMN "public"."police_user"."mobile" IS '手机号码';
COMMENT ON COLUMN "public"."police_user"."password" IS '用户密码';
COMMENT ON COLUMN "public"."police_user"."status" IS '用户启用状态：0-->禁用， 1-->启用';

DROP TABLE IF EXISTS police_picture;
CREATE TABLE police_picture(
  id SERIAL8,
  path VARCHAR(255) NOT NULL,
  url VARCHAR(255) NOT NULL,
  md5 CHAR(32)  NOT NULL,
  sha1 CHAR(40) NOT NULL,
  status SMALLINT DEFAULT 1,
  create_time INT(10) unsigned DEFAULT 0,
  PRIMARY KEY (`id`)
  );
ALTER TABLE public.police_picture OWNER TO "postgres";
COMMENT ON COLUMN public.police_picture.id IS '主键id自增';
COMMENT ON COLUMN public.police_picture.path IS '本地路径';
COMMENT ON COLUMN public.police_picture.url IS '图片链接';
COMMENT ON COLUMN public.police_picture.md5 IS '文件md5';
COMMENT ON COLUMN public.police_picture.sha1 IS '文件 sha1编码';
COMMENT ON COLUMN public.police_picture.status IS '状态';
COMMENT ON COLUMN public.police_picture.create_time IS '创建时间';