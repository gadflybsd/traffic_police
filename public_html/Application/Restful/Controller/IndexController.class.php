<?php
namespace Restful\Controller;
use Think\Controller\RestController;

class IndexController extends RestController{
	/**
	 * @param null $rsa
	 *
	 */
	public function angular($rsa=null){
		header("Content-type: json; charset=utf-8");
		header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:POST, GET, PUT, OPTIONS, DELETE');
		header('Access-Control-Allow-Headers:x-requested-with,content-type');
		header("Access-Control-Allow-Credentials: true");
		$param = I('param.');
		$param['data'] = $param['data']?json_decode(I('param.data', '', false), true):null;
		switch ($this->_method){
			case 'post':
				$op = 'insert';
				break;
			case 'put':
				$op = 'update';
				break;
			case 'delete':
				$op = 'delete';
				break;
			case 'get':
			default:
				$op = 'lists';
				break;
		}
		if(!is_null($rsa)){
			$key = $this->getRsaKey('getRsaKey', $param['uuid']);
			$decrypt = json_decode($this->_privateKeyDecode($param['rsa'], $key['client_private']), true);
			$action = (isset($decrypt['action']))?$decrypt['action']:'angular';
			$model = (isset($decrypt['model']))?ucfirst($decrypt['model']):ucfirst(CONTROLLER_NAME);
			$module = (isset($decrypt['module']))?ucfirst($decrypt['module']):'ajax_'.$this->_method;
			$param['op'] = (isset($decrypt['op']))?ucfirst($decrypt['op']):$op;
		}else{
			$action = (isset($param['action']))?$param['action']:'angular';
			$model = (isset($param['model']))?ucfirst($param['model']):ucfirst(CONTROLLER_NAME);
			$module = (isset($param['module']))?ucfirst($param['module']):'ajax_'.$this->_method;
			$param['op'] = (isset($param['op']))?ucfirst($param['op']):$op;
		}
		if($action == 'angular'){
			$msg = '系统在'.$model.'模型中没有找到'.$module.'方法';
			if(method_exists(D($model), $module))
				$return = call_user_func(array(D($model), $module), $param);
			else
				$return = array('type' => 'Error', 'msg' => $msg);
		}else{
			$msg = '系统在'.$action.'控制器中没有找到'.$module.'方法';
			if(method_exists(A($action), $module))
				$return = call_user_func(array(A($action), $module), $param);
			else
				$return = array('type' => 'Error', 'msg' => $msg);
		}
		$debug = (APP_DEBUG)?array('method' => $this->_method, 'param' => $param):array();
		$this->response(array_merge($return, $debug), 'json');
	}
	
	public function phpinfo(){
		phpinfo();
	}
	
	public function faceApi($type, $id){
		$url = 'http://119.23.148.57/face_api/';
		$haystack = array('faceImport', 'faceUpdate', 'faceDelete', 'faceMatch', 'faceSearch', 'faceCompare');
		if(in_array($type, $haystack)){
			$path = M('Picture')->where('id='.$id)->getField('path');
			$appKey = '3d05f4d371a24621b024b31903fd2752';
			$appSecret = 'dddedbdd21184289a4b8a8d2edd2f9ee';
			switch ($type){
				case 'faceImport':
					$data = array(
						'appKey'        => $appKey,
						'appExtId'      => 'police'.$id,
						'featuretype'   => 1,
						'imgType'       => 'jpg',
						'img'           => base64_encode_image('/Users/gadflybsd/PhpstormProjects'.$path),
					);
					$data['sign'] = md5('appExtId='.$data['appExtId'].'&appKey='.$appKey.'&'.$appSecret);
					break;
			}
			$result = $this->curlData($url.$type, $data, 'POST', false);
			echo json_encode($data);
			echo '<hr>';
			dump($result);
		}
	}

	/**
	 * 上传图片
	 * @author huajie <banhuajie@163.com>
	 * http://tp.cn/public_html/restful.php?s=Index/uploadPicture&type=idcard&uuid=[UUID]
	 */
	public function uploadPicture(){
		header("Content-type: json; charset=utf-8");
		header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:POST, GET, PUT, OPTIONS, DELETE');
		header('Access-Control-Allow-Headers:x-requested-with,content-type');
		header("Access-Control-Allow-Credentials: true");
		$param = I('param.');
		/* 返回标准数据 */
		$return  = array('status' => 1, 'info' => '上传成功', 'data' => '');
		/* 调用文件上传组件上传文件 */
		$Picture = D('Picture');
		$pic_driver = C('PICTURE_UPLOAD_DRIVER');
		$info = $Picture->upload(
			$_FILES,
			$param,
			C('PICTURE_UPLOAD'),
			C('PICTURE_UPLOAD_DRIVER'),
			C("UPLOAD_{$pic_driver}_CONFIG")
		); //TODO:上传到远程服务器

		/* 记录图片信息 */
		if($info){
			$return['status'] = 1;
			$return['type'] = 'success';
			$return['data'] = $info['file'];
			$return['path'] = dirname(rtrim(dirname(APP_ROOT), '/\\') . DIRECTORY_SEPARATOR).$info['file']['path'];
			switch ($param['type']){
				case 'idcard':
					$return['ocr'] = json_decode(D('Idcard')->curlData(C('OCR_API.idcard'), $return['path'], 'UPLOAD', false));
					break;
				case 'driving':
					$return['ocr'] = json_decode(D('Idcard')->curlData(C('OCR_API.driving'), $return['path'], 'UPLOAD', false));
					break;
				case 'vehicle':
					$return['ocr'] = json_decode(D('Idcard')->curlData(C('OCR_API.vehicle'), $return['path'], 'UPLOAD', false));
					break;
				case 'face':
					$return['ocr'] = array(
						'resultcount'   => 0,
						'result'        => null
					);
					//$return['ocr'] = json_decode(D('Idcard')->curlData(C('OCR_API.face'), $return['path'], 'UPLOAD', false));
					break;
			}
		} else {
			$return['status'] = 0;
			$return['info']   = $Picture->getError();
			$return['type'] = 'error';
		}
		/* 返回JSON数据 */
		$this->ajaxReturn($return);
	}

	public function getCacheData($param){
		$category = D('MustachTask')->categoryCache();
		$sitedate = D('Document')->sitedateCache();
		$localStorage = array();
		$sessionStorage = array();
		if($param['data']['category']){
			if($param['data']['category']['md5'] != $category['md5'] && $param['data']['category']['sha1'] != $category['sha1'])
				$localStorage['category'] = $category;
		}else{
			$localStorage['category'] = $category;
		}
		if($param['data']['sitedate']){
			if($param['data']['sitedate']['md5'] != $sitedate['md5'] && $param['data']['sitedate']['sha1'] != $sitedate['sha1'])
				$localStorage['sitedate'] = $sitedate;
		}else{
			$localStorage['sitedate'] = $sitedate;
		}
		if($param['data']['userInfo']){
			if(M('MustachUser')->where('uuid = "'.$param['data']['userInfo']['uuid'].'" AND uid='.$param['data']['userInfo']['uid'])->count()==0)
				$sessionStorage['userInfo'] = 'empty';
		}
		return array('type' => 'Success', 'sessionStorage' => $sessionStorage, 'localStorage' => $localStorage);
	}

	public function weixin(){
		Vendor('Weixin');
		$weixin = new \Weixin();
		$weixin->valid();
		return array('type' => 'Success', 'data' => $weixin->get_auth2_userinfo(I('post.code')));
	}

	/**
	 * 发送电信能力平台短信验证码模型
	 * @param  array $param
	 * @method sendSms
	 * @return array
	 * @example public_html/example/RestController.php 12 5 参数 $param 的数据结构
	 * @example public_html/example/RestController.php 18 7 返回值 $return 的数据结构
	 */
	public function sendSms($param){
		Vendor('CtcSms');
		$sms = new CtcSms();
		$action = ($param['action'])?$param['action']:'register';
		if(is_null($param['uuid']))
			$user['uid'] = 0;
		else
			$user = $this->getUserData($param['uuid']);
		return $sms->send($param['mobile'], $user['uid'], $param['uuid'], $action);
	}

	/**
	 * 创建生成服务器和客户端RSA密钥
	 *
	 * @return array|mixed
	 */
	protected function createRsaKey(){
		if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
			$config = array('config' => 'D:\xampp\php\extras\openssl\openssl.cnf');
		}else{
			$config = array(
				"digest_alg" => "sha512",
				"private_key_bits" => 1024,
				"private_key_type" => OPENSSL_KEYTYPE_RSA,
			);
		}
		$res1 = openssl_pkey_new($config);
		openssl_pkey_export($res1, $privateKeyService, null, $config);
		$publicKeyService = openssl_pkey_get_details($res1);
		$res2 = openssl_pkey_new($config);
		openssl_pkey_export($res2, $privateKeyClient, null, $config);
		$publicKeyClient = openssl_pkey_get_details($res2);
		return array(
			'service_public'    => $publicKeyService['key'],
			'service_private'   => $privateKeyService,
			'client_public'     => $publicKeyClient['key'],
			'client_private'    => $privateKeyClient,
			'create_key_dateline' => time(),
		);
	}

	protected function getRsaKey($change, $uuid){
		if(in_array($change, array('createRsaKey', 'changeRsaKey', 'getRsaKey'))){
			if(in_array($change, array('createRsaKey', 'changeRsaKey'))){
				S($uuid.'-RsaKey', $this->createRsaKey());
			}
			return S($uuid.'-RsaKey');
		}else{
			return S($uuid.'-RsaKey', null);
		}
	}

	/**
	 * 私钥加密
	 * @param $sourcestr    需加密的数据字符串
	 *
	 * @return string       加密后的字符串
	 */
	protected function _privateKeyEncode($sourcestr, $key, $tojs = FALSE){
		//$prikeyid = openssl_get_privatekey(file_get_contents(self::PRIVATE_KEY));
		$prikeyid = openssl_get_privatekey($key);
		$padding = $tojs?OPENSSL_NO_PADDING:OPENSSL_PKCS1_PADDING;
		if(openssl_private_encrypt($sourcestr, $crypttext, $prikeyid, $padding)){
			return base64_encode("".$crypttext);
		}
	}

	/**
	 * 公钥加密
	 *
	 * @param string 明文
	 * @param string 证书文件（.crt）
	 *
	 * @return string 密文（base64编码）
	 *
	 * //JS->PHP 测试
	 * $txt_en = $_POST['password'];
	 * $txt_en = base64_encode(pack("H*", $txt_en));
	 * $file = 'ssl/server.pem';
	 * $txt_de = $this->privateKeyDecode($txt_en, $file, TRUE);
	 * var_dump($txt_de);
	 * //PHP->PHP 测试
	$encrypt = $this->_publicKeyEncode('{"name":"公钥加密私钥解密测试","password":"dg123456"}');
	$decrypt = $this->_privateKeyDecode($encrypt);
	echo '<h2>公钥加密, 私钥解密</h2>';
	echo 'encode: <p>'.$encrypt.'</p><br>';
	echo 'dncode: '.$decrypt.'<br>';
	echo '<br><hr>';
	$encrypt = $this->_privateKeyEncode('{"name":"私钥加密公钥解密测试","password":"pw123456"}');
	$decrypt = $this->_publicKeyDecode($encrypt);
	echo '<h2>私钥加密, 公钥解密</h2>';
	echo 'encode: <p>'.$encrypt.'</p><br>';
	echo 'dncode: '.$decrypt.'<br>';
	echo '<br><hr>';
	 */
	protected function _publicKeyEncode($sourcestr, $key, $tojs = FALSE){
		//$pubkeyid = openssl_get_publickey(file_get_contents(self::PUBLIC_KEY));
		$pubkeyid = openssl_get_publickey($key);
		$padding = $tojs?OPENSSL_NO_PADDING:OPENSSL_PKCS1_PADDING;
		if(openssl_public_encrypt($sourcestr, $crypttext, $pubkeyid, $padding)){
			return base64_encode("".$crypttext);
		}
	}

	/**
	 * 私钥解密
	 *
	 * @param string    $crypttext 密文（二进制格式且base64编码）
	 * @param bool      $fromjs    密文是否来源于JS的RSA加密
	 *
	 * @return string 明文
	 */
	protected function _privateKeyDecode($crypttext, $key, $fromjs = FALSE){
		//$prikeyid = openssl_get_privatekey(file_get_contents(self::PRIVATE_KEY));
		$prikeyid = openssl_get_privatekey($key);
		$padding = $fromjs ? OPENSSL_NO_PADDING : OPENSSL_PKCS1_PADDING;
		if(openssl_private_decrypt(base64_decode($crypttext), $sourcestr, $prikeyid, $padding)){
			return $fromjs ? rtrim(strrev($sourcestr), "/0") : "".$sourcestr;
		}
		return ;
	}

	/**
	 * 公钥解密
	 * @param string    $crypttext   需解密的字符串
	 * @param bool      $fromjs      密文是否来源于JS的RSA加密
	 *
	 * @return string|void      解密后的字符串
	 */
	protected function _publicKeyDecode($crypttext, $key, $fromjs = FALSE){
		//$pubkeyid = openssl_get_publickey(file_get_contents(self::PUBLIC_KEY));
		$pubkeyid = openssl_get_publickey($key);
		$padding = $fromjs ? OPENSSL_NO_PADDING : OPENSSL_PKCS1_PADDING;
		if(openssl_public_decrypt(base64_decode($crypttext), $sourcestr, $pubkeyid, $padding)){
			return $fromjs ? rtrim(strrev($sourcestr), "/0") : "".$sourcestr;
		}
		return ;
	}

	/**
	 * 通过CURL或者封装过的Snoopy方式像微信服务器发送指令,GET或者POST方法提交数据返回结果
	 * @param        $url       提交数据的接收地址,如果是GET方法,该地址不包含?后的数据
	 * @param        $data      提交的数据GET方式为?后的部分,POST为一个表单的JSON数据
	 * @param string $method    数据提交的方法,GET(默认)或者POST
	 * @param bool   $ssl       是否SSL加密,默认为True
	 *
	 * @return string   返回服务器返回的结果
	 */
	protected function curlData($url, $data, $method='GET', $ssl=true){
		switch($method){
			case 'GET':
				$getUrl = $url.'?'.$data;
				$ch = curl_init($getUrl);
				curl_setopt($ch, CURLOPT_URL, $getUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				break;
			case 'POST':
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				break;
			case 'UPLOAD':
				$ch = curl_init($url);
				$file = array('file' => new \CurlFile(realpath($data)));
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				break;
		}
		curl_setopt($ch, CURLOPT_TIMEOUT, 500);
		if($ssl){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		$results= curl_exec($ch);
		curl_close($ch);
		return $results;
	}
}