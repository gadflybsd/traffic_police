<?php
return array(
	//'配置项'=>'配置值'

	/* 系统数据加密设置 */
	'DATA_AUTH_KEY' => '~{TU!Y,)oRuZJQMNbz&A`]jWK1Xe}@wODv>t(r9=', //默认数据加密KEY
	'DATA_CACHE_KEY'=> '~{TU!Y,)oRuZJQMNbz&A`]jWK1Xe}@wODv>t(r9=',

	/* 用户相关设置 */
	'USER_MAX_CACHE'     => 1000, //最大缓存用户数
	'USER_ADMINISTRATOR' => 1, //管理员用户ID

	/* URL配置 */
	'URL_CASE_INSENSITIVE' => true, //默认false 表示URL区分大小写 true则表示不区分大小写
	'URL_MODEL'            => 3, //URL模式
	'VAR_URL_PARAMS'       => '', // PATHINFO URL参数变量
	'URL_PATHINFO_DEPR'    => '/', //PATHINFO URL分割符

	/* 全局过滤配置 */
	'DEFAULT_FILTER' => '', //全局过滤函数

	/* 数据库配置 */
	'DB_TYPE'   => 'mysql', // 数据库类型
	'DB_HOST'   => 'localhost', // 服务器地址
	'DB_NAME'   => 'police', // 数据库名
	'DB_USER'   => 'police', // 用户名
	'DB_PWD'    => 'police',  // 密码
	'DB_PORT'   => '3306', // 端口
	'DB_PREFIX' => 'police_', // 数据库表前缀
	
	'DATA_CACHE_PREFIX'    => 'police_', // 缓存前缀
	'DATA_CACHE_TYPE'      => 'File', // 数据缓存类型
	'URL_MODEL'            => 3, //URL模式

	/* 文件上传相关配置 */
	'DOWNLOAD_UPLOAD' => array(
		'mimes'    => '', //允许上传的文件MiMe类型
		'maxSize'  => 5*1024*1024, //上传的文件大小限制 (0-不做限制)
		'exts'     => 'jpg,gif,png,jpeg,zip,rar,tar,gz,7z,doc,docx,txt,xml', //允许上传的文件后缀
		'autoSub'  => true, //自动子目录保存文件
		'subName'  => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
		'rootPath' => '../../Uploads/Download/', //保存根路径
		'savePath' => '', //保存路径
		'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
		'saveExt'  => '', //文件保存后缀，空则使用原后缀
		'replace'  => false, //存在同名是否覆盖
		'hash'     => true, //是否生成hash编码
		'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
	), //下载模型上传配置（文件上传类配置）

	/* 图片上传相关配置 */
	'PICTURE_UPLOAD' => array(
		'mimes'    => '', //允许上传的文件MiMe类型
		'maxSize'  => 2*1024*1024, //上传的文件大小限制 (0-不做限制)
		'exts'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
		'autoSub'  => true, //自动子目录保存文件
		'subName'  => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
		'rootPath' => '../../Uploads/Picture/', //保存根路径
		'savePath' => '', //保存路径
		'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
		'saveExt'  => '', //文件保存后缀，空则使用原后缀
		'replace'  => false, //存在同名是否覆盖
		'hash'     => true, //是否生成hash编码
		'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
	), //图片上传相关配置（文件上传类配置）

	'PICTURE_UPLOAD_URL' => 'http://222.219.73.36/Uploads/Picture',
	//'PICTURE_UPLOAD_URL' => 'http://home.hzxiansheng.com/Uploads/Picture',
	//'PICTURE_UPLOAD_URL' => 'http://tp.cn/Uploads/Picture',

	'PICTURE_UPLOAD_DRIVER'	=>'local',
	//本地上传文件驱动配置
	'UPLOAD_LOCAL_CONFIG'=>array(),
	'UPLOAD_BCS_CONFIG'=>array(
		'AccessKey'	=> '',
		'SecretKey'	=> '',
		'bucket'	=> '',
		'rename'	=> false
	),
	'UPLOAD_QINIU_CONFIG'=>array(
		'accessKey'		=> '__ODsglZwwjRJNZHAu7vtcEf-zgIxdQAY-QqVrZD',
		'secrectKey'	=> 'Z9-RahGtXhKeTUYy9WCnLbQ98ZuZ_paiaoBjByKv',
		'bucket'		=> 'onethinktest',
		'domain'		=> 'onethinktest.u.qiniudn.com',
		'timeout'		=> 3600,
	),

	'OCR_API'    => array(
		'idcard'        => 'http://imgs-sandbox.intsig.net/icr/recognize_id_card?user=xxx&pass=xxx&head_portrait=1',
		'driving'       => 'http://imgs-sandbox.intsig.net/icr/recognize_driving_license',
		'vehicle'       => 'http://imgs-sandbox.intsig.net/icr/recognize_vehicle_license',
		'face'          => 'http://119.23.148.57/face_api/',
	),

	'CTCSMS'	=> array(
		'app_id'		=> '187412130000250368',
		'pp_secret'		=> '3e2a8a70a0fb2b47c45549924128d4cc',
		'model_table'	=> 'MustachSmsLog',
	),

	/* 模板相关配置 */
	'TMPL_PARSE_STRING' => array(
		'__STATIC__' => __ROOT__ . '/Public/static',
		'__ADDONS__' => __ROOT__ . '/Public/Admin/Addons',
		'__IMG__'    => __ROOT__ . '/Public/Admin/images',
		'__CSS__'    => __ROOT__ . '/Public/Admin/css',
		'__JS__'     => __ROOT__ . '/Public/Admin/js',
	),
);