<?php
/**
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/12
 * Time: 16:39
 */
namespace Restful\Model;

class UserModel extends CommonModel {
	/**
	 * 用户注册
	 * @param $param	array('uuid' => 'UUID', 'mobile' => '用户手机号码'， 'password' => '用户密码', 'sms_code' => '短信验证码', 'repassword' => '重复密码')
	 * @return array
	 */
	public function register($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['mobile'], 'msg' => '登录用户手机必须填写!'),
			array('type' => 'mobile', 'value' => $param['data']['mobile'], 'msg' => '登录用户手机格式不正确!'),
			array('type' => 'unique', 'value' => $param['data']['mobile'], 'msg' => '系统中已经存在相同的手机号码!', 'rule' => array('field' => 'mobile')),
			//array('type' => 'require', 'value' => $param['sms_code'], 'msg' => '手机验证码必须填写!'),
			array('type' => 'require', 'value' => $param['data']['password'], 'msg' => '用户密码必须填写!'),
			array('type' => 'require', 'value' => $param['data']['repassword'], 'msg' => '用户再次输入密码必须填写'),
			array('type' => 'notequal', 'value' => $param['data']['password'], 'msg' => '两次密码输入不一致', 'rule' => $param['data']['repassword']),
			//array('type' => 'smsCode', 'value' => array('mobile' => $param['mobile'], 'code' => $param['sms_code'], 'action' => 'register'), 'msg' => '手机验证码验证失败!'),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$data = array(
				'police_name'	=> $param['data']['police_name'],
				'police_sn'	    => $param['data']['police_sn'],
				'uuid'			=> isset($param['data']['uuid'])?$param['data']['uuid']:array('exp', 'uuid()'),
				'mobile'		    => $param['data']['mobile'],
				'clear_password'=> $param['data']['password'],
				'last_recip'    	=> get_client_ip(),
			);
			$curd = $this->curd(array(
				'type'  => 'add',
				'data'  => $data,
				'msg'	=> 'APP用户注册成功！',
			));
			if($curd['type'] == 'Success'){
				return array_merge($curd, $this->_getStorage($param['data']['uuid']));
			}else{
				return $curd;
			}
		}
	}

	/**
	 * 获取用户基本信息数据
	 * @param $param
	 * @return array
	 */
	public function getUserInfo($param){
		return $this->curd(array(
			'type'  => 'find',
			'where'	=> 'mobile="'.$param['data']['mobile'].'" AND uuid="'.$param['data']['uuid'].'"',
			'msg'	=> '获取APP用户信息成功！',
		));
	}

	/**
	 * 用户登录
	 * @param $param	array('uuid' => 'UUID', 'mobile' => '用户手机号码'， 'password' => '用户密码')
	 * @return array
	 */
	public function login($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['mobile'], 'msg' => '登录用户手机必须填写!'),
			array('type' => 'mobile', 'value' => $param['data']['mobile'], 'msg' => '登录用户手机格式不正确!'),
			array('type' => 'require', 'value' => $param['data']['password'], 'msg' => '登录密码必须填写!'),
			array('type' => 'equal', 'value' => $param['data']['mobile'], 'msg' => '该用户手机没有注册!', 'rule' => array('field' => 'mobile')),
			array('type' => 'isPassword', 'value' => $param['data']['password'], 'msg' => '用户密码输入不正确!', 'rule' => $param['data']['mobile'], 'table' => 'User'),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$where = array('mobile' => $param['data']['mobile']);
			$ip = get_client_ip();
			$data = array(
				'uuid'				=> $param['data']['uuid'],
				'login_recip'		=> $ip,
				'login_dateline'	=> time(),
				'last_recip' 		=> $ip,
				'last_changes' 		=> 'loginSystem'
			);
			$curd =  $this->curd(array(
				'type' 	=> 'save',
				'data' 	=> $data,
				'msg' 	=> '用户成功登录系统!',
				'where' => $where,
			));
			if($curd['type'] == 'Success'){
				return array_merge($curd, $this->_getStorage($param['data']['uuid']));
			}else{
				return $curd;
			}
		}
	}

	/**
	 * 用户退出
	 * @param $param	array('uuid' => 'UUID')
	 * @return array
	 */
	public function logout($param){
		$data = array(
			'uuid'          => '',
			'last_recip'	=> get_client_ip(),
			'last_changes'	=> 'logoutSystem'
		);
		$del = $this->curd(array(
			'type' 	=> 'save',
			'data' 	=> $data,
			'msg' 	=> '用户成功退出系统!',
			'where' => 'uuid="'.$param['data']['uuid'].'"',
		));
		return $del;
	}

	/**
	 * 设置用户昵称
	 * @param $param	array('uuid' => 'UUID', 'nickname' => '用户昵称')
	 * @return array
	 */
	public function setNickname($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['nickname'], 'msg' => '登录用户昵称必须填写!'),
			array('type' => 'unique', 'value' => $param['data']['nickname'], 'msg' => '系统中已经存在相同的用户昵称!', 'rule' => array('field' => 'nickname')),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$data = array(
				'nickname'		=> $param['data']['nickname'],
				'last_recip'	=> get_client_ip(),
				'last_changes'	=> 'setNickname'
			);
			$curd = $this->curd(array(
				'type'  => 'save',
				'data'  => $data,
				'where' => 'uuid="'.$param['data']['uuid'].'"',
			));
			if($curd['type'] == 'Success')
				return array_merge($curd, $this->_getStorage($param['data']['uuid']));
			else
				return $curd;
		}
	}

	/**
	 * 设置用户密码
	 * @param $param	array('uuid' => 'UUID', 'mobile' => '用户手机号码'， 'old_password' => '原密码', 'new_password' => '新密码', 'repassword' => '重复密码')
	 * @return array
	 */
	public function setPassword($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['old_password'], 'msg' => '用户原密码必须填写!'),
			array('type' => 'require', 'value' => $param['data']['new_password'], 'msg' => '用户新密码必须填写!'),
			array('type' => 'require', 'value' => $param['data']['repassword'], 'msg' => '用户再次输入密码必须填写!'),
			array('type' => 'notequal', 'value' => $param['data']['new_password'], 'msg' => '两次密码输入不一致!', 'rule' => $param['data']['repassword']),
			array('type' => 'isPassword', 'value' => $param['data']['old_password'], 'msg' => '用户老密码输入不正确!', 'rule' => $param['data']['mobile']),
			array('type' => 'require', 'value' => $param['data']['sms_code'], 'msg' => '手机验证码必须填写!'),
			array('type' => 'smsCode', 'value' => array('mobile' => $param['data']['mobile'], 'code' => $param['data']['sms_code'], 'action' => 'changePassword'), 'msg' => '手机验证码验证失败!'),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$data = array(
				'clear_password'=> $param['data']['new_password'],
				'last_recip'	=> get_client_ip(),
				'last_changes'	=> 'setPassword',
			);
			return $this->curd(array(
				'type'  => 'save',
				'data'  => $data,
				'where' => 'uuid="'.$param['data']['uuid'].'"',
			));
		}
	}

	/**
	 * 用户忘记密码
	 * @param $param
	 * @return array
	 */
	public function forgotPassword($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['new_password'], 'msg' => '用户新密码必须填写!'),
			array('type' => 'require', 'value' => $param['data']['repassword'], 'msg' => '用户再次输入密码必须填写!'),
			array('type' => 'notequal', 'value' => $param['data']['new_password'], 'msg' => '两次密码输入不一致!', 'rule' => $param['data']['repassword']),
			array('type' => 'require', 'value' => $param['data']['sms_code'], 'msg' => '手机验证码必须填写!'),
			array('type' => 'smsCode', 'value' => array('mobile' => $param['data']['mobile'], 'code' => $param['data']['sms_code'], 'action' => 'changePassword'), 'msg' => '手机验证码验证失败!'),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$data = array(
				'clear_password'=> $param['data']['new_password'],
				'last_recip'	=> get_client_ip(),
				'last_changes'	=> 'forgotPassword',
			);
			return $this->curd(array(
				'type'  => 'save',
				'data'  => $data,
				'where' => 'uuid="'.$param['data']['uuid'].'"',
			));
		}
	}

	/**
	 * 变更用户手机号码
	 * @param $param	array('uuid' => 'UUID', 'mobile' => '用户手机号码')
	 * @return array
	 */
	public function setMobile($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['mobile'], 'msg' => '登录用户手机必须填写!'),
			array('type' => 'mobile', 'value' => $param['data']['mobile'], 'msg' => '登录用户手机格式不正确!'),
			array('type' => 'unique', 'value' => $param['data']['mobile'], 'msg' => '系统中已经存在相同的手机号码!', 'rule' => array('field' => 'mobile')),
			array('type' => 'require', 'value' => $param['data']['sms_code'], 'msg' => '手机验证码必须填写!'),
			array('type' => 'smsCode', 'value' => array('mobile' => $param['data']['mobile'], 'code' => $param['data']['sms_code'], 'action' => 'changeMobile'), 'msg' => '手机验证码验证失败!'),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$data = array(
				'mobile'		=> $param['data']['mobile'],
				'last_recip'	=> get_client_ip(),
				'last_changes'	=> 'setMobile'
			);
			$curd = $this->curd(array(
				'type'  => 'save',
				'data'  => $data,
				'where' => 'uuid="'.$param['data']['uuid'].'"',
			));
			if($curd['type'] == 'Success')
				return array_merge($curd, $this->_getStorage($param['data']['uuid']));
			else
				return $curd;
		}
	}

	/**
	 * 设置用户头像
	 * @param $param	array('uuid' => 'UUID', 'headimg' => '用户自定义头像地址')
	 * @return array
	 */
	public function setHeadimg($param){
		$validator = $this->validators(array(
			array('type' => 'require', 'value' => $param['data']['headimg'], 'msg' => '用户自定义头像必须上传!'),
		));
		if($validator['type'] != 'Success'){
			return $validator;
		}else{
			$data = array(
				'headimg' => $param['data']['headimg'],
				'last_recip' => get_client_ip(),
				'last_changes' => 'setHeadimg'
			);
			$curd = $this->curd(array(
				'type' 	=> 'save',
				'data' 	=> $data,
				'msg' 	=> '系统已更新用户自定义头像!',
				'where' => 'uuid="'.$param['data']['uuid'].'"',
			));
			if($curd['type'] == 'Success')
				return array_merge($curd, $this->_getStorage($param['data']['uuid']));
			else
				return $curd;
		}
	}

	/**
	 * 获取APP端用户本地存取数据
	 * @param $uuid
	 * @return array
	 */
	private function _getStorage($uuid){
		$user = $this->where('uuid="'.$uuid.'"')->find();
		return array(
			'localStorage'	=> array('device'	=> array('mobile' => $user['mobile'])),
			'localStorage'	=> array('userInfo'	=> $user)
		);
	}
}