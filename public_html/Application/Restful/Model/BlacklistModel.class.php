<?php
/**
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/13
 * Time: 18:32
 */

namespace Restful\Model;

class BlacklistModel extends CurdModel{
	protected $tableName = 'Blacklist';

	/**
	 * 获取数据列表
	 * @param $param
	 *
	 * @return array
	 */
	public function lists($param){
		$list = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'type'  => 'select',
		));
		return $list;
	}

	/**
	 * 获取指定ID的信息数据
	 * @param $param
	 * @return array
	 */
	public function info($param){
		$info = $this->curd(array(
			'model' => $this->tableName.'_view',
			'where' => 'id=' . $param['pk'],
			'type'  => 'find',
		));
		return $info;
	}

	/**
	 * 行驶证查询列表
	 * @param $param
	 * @return array
	 */
	public function search($param){
		switch ($param['data']['type']){
			case 'driving':
				$driving_id = M('DrivingLicense')->where('driving_license_main_number LIKE "%' . $param['data']['search'] . '%"')->getField('id');
				$where = 'driving_id IN (' . implode(', ', $driving_id) . ')';
				break;
			case 'vehicle':
				$vehicle_id = M('VehicleLicense')->where('vehicle_license_main_plate_num LIKE "%' . $param['data']['search'] . '%"')->getField('id');
				$where = 'vehicle_id IN (' . implode(', ', $vehicle_id) . ')';
				break;
			case 'idcard':
				$idcard = M('VehicleTogetherMenView')->where('id_number LIKE "%' . $param['data']['search'] . '%"')->getField('exceptional_id');
				$where = 'idcard_id IN (' . implode(', ', $idcard) . ')';
				break;
			default:
				$where = 'id <> 0';
		}
		$search = $this->curd(array(
			'model' => $this->tableName.'View',
			'page'  => $param['data']['page'],
			'where' => $where,
			'type'  => 'select',
		));
		return $search;
	}

	/**
	 * 数据验证
	 * @param $param
	 * @return array
	 */
	protected function valide($param){
		return array();
	}

	/**
	 * 构造数据
	 * @param $param
	 * @return array
	 */
	protected function curdData($param){
		return array();
	}
}