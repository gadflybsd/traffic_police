<?php
/**
 * 车辆稽查情况数据模型
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/13
 * Time: 19:33
 */

namespace Restful\Model;
use Think\Log;

class VehicleExceptionalModel extends CurdModel{
	protected $tableName = 'vehicle_exceptional_log';

	/**
	 * 数列列表
	 * @param $param
	 * @return array
	 */
	public function lists($param){
		$list = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'type'  => 'select',
		));
		/*foreach ($list['data'] AS $key => $val){
			$list['data'][$key]['idcards_count'] = (is_null($val['idcard_ids']) || empty($val['idcard_ids']))?0:count(explode(',', $val['idcard_ids']));
			$ids = (is_null($val['idcard_ids']) || empty($val['idcard_ids']))?0:M('Idcard')->where('id IN ('.$val['idcard_ids'].')')->getField('GROUP_CONCAT(idcard_1)');
			$list['data'][$key]['idcards_url'] = M('Picture')->where('id IN ('.$ids.')')->getField('GROUP_CONCAT(url)');
			$faces = (is_null($val['face_ids']) || empty($val['face_ids']))?0:M('Face')->where('id IN ('.$val['face_ids'].')')->getField('GROUP_CONCAT(face_license)');
			$list['data'][$key]['faces_count'] = (is_null($val['face_ids']) || empty($val['face_ids']))?0:count(explode(',', $val['face_ids']));
			$list['data'][$key]['faces_url'] = (is_null($val['face_ids']) || empty($val['face_ids']))?null:M('Picture')->where('id IN ('.$faces.')')->getField('GROUP_CONCAT(url)');
			$list['data'][$key]['photos_count'] = (is_null($val['photos']) || empty($val['photos']))?0:count(explode(',', $val['photos']));
			$list['data'][$key]['photos_url'] = (is_null($val['photos']) || empty($val['photos']))?null:M('Picture')->where('id IN ('.$val['photos'].')')->getField('GROUP_CONCAT(url)');
		}*/
		return $list;
	}

	/**
	 * 获取指定ID的信息数据
	 * @param $param
	 * @return array
	 */
	public function info($param){
		$info = $this->curd(array(
			'model' => $this->tableName.'_view',
			'where' => 'id=' . $param['pk'],
			'type'  => 'find',
		));
		return $info;
	}

	/**
	 * 新增一个信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function add($param){
		$idcard_ids = array();
		$face_ids = array();
		if(count($param['data']['idcard']) > 0){
			foreach ($param['data']['idcard'] AS $key => $val){
				$val['recip'] = get_client_ip();
				$idcard = $this->curd(array(
					'model' => 'Idcard',
					'type'  => 'add',
					'data'  => $val
				));
				$idcard_ids[] = $idcard['pk'];
				$idcard_url[] = M('IdcardView')->where('id='.$idcard['pk'])->getField('idcard_url_1');
			}
		}
		if(count($param['data']['face']) > 0){
			foreach ($param['data']['face'] AS $key => $val){
				$val['recip'] = get_client_ip();
				/*$img = M('Picture')->where('id='.$val['face_license'])->getField('path');
				$val['face_headimg'] = base64_encode_image('/www/web/home_hzxiansheng_com'.$img);*/
				$face = $this->curd(array(
					'model' => 'Face',
					'type'  => 'add',
					'data'  => $val
				));
				$face_ids[] = $face['pk'];
				$face_url[] = M('FaceView')->where('id='.$face['pk'])->getField('face_headimg_url');
			}
		}
		if(count($idcard_ids) == count($param['data']['idcard']) && count($face_ids) == count($param['data']['face'])){
			$param['data']['idcard_ids'] = implode(', ', $idcard_ids);
			$param['data']['idcard_url'] = implode(', ', $idcard_url);
			$param['data']['idcard_count'] = count($idcard_ids);
			$param['data']['face_ids'] = implode(', ', $face_ids);
			$param['data']['face_url'] = implode(', ', $face_url);
			$param['data']['face_count'] = count($face_ids);
			if(count($param['data']['driving']) > 0){
				$param['data']['driving']['recip'] = get_client_ip();
				$driving = $this->curd(array(
					'model' => 'DrivingLicense',
					'type'  => 'add',
					'data'  => $param['data']['driving']
				));
			}else{
				$driving['pk'] = 0;
			}
			if(count($param['data']['vehicle']) > 0){
				$param['data']['driving_id'] = $driving['pk'];
				$param['data']['vehicle']['recip'] = get_client_ip();
				$vehicle = $this->curd(array(
					'model' => 'VehicleLicense',
					'type'  => 'add',
					'data'  => $param['data']['vehicle']
				));
			}else{
				$vehicle['pk'] = 0;
			}
			$param['data']['vehicle_id'] = $vehicle['pk'];
			$param['data']['recip'] = get_client_ip();
			$validator = $this->validators($this->valide($param));
			if ($validator['type'] != 'Success') {
				return $validator;
			} else {
				$add = $this->curd(array(
					'model'=> 'VehicleExceptionalLog',
					'type' => 'add',
					'data' => $param['data']
				));
				return array_merge($add, array('idcard' => $idcard));
			}
		}else{
			return $idcard;
		}
	}

	/**
	 * 编辑指定信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function save($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$save = $this->curd(array(
				'type' => 'save',
				'data' => $this->curdData($param),
				'where' => 'id=' . $param['pk']
			));
			return $save;
		}
	}

	/**
	 * 查询列表
	 * @param $param
	 * @return array
	 */
	public function search($param){
		$type = ($param['data']['search']['type'])?$param['data']['search']['type']:$param['data']['search']['value'];
		switch ($type){
			case 'vehicle': //车牌号
				$where = 'vehicle_license_main_plate_num LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'owner': //车辆所有人
				$where = 'vehicle_license_main_owner LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'name': //驾驶人员
				$where = 'realname LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'driving': //驾驶证号
				$where = 'driving_license_main_number LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			default:
				$where = 'id <> 0';
		}
		$search = $this->curd(array(
			'model' => $this->tableName.'View',
			'page'  => $param['data']['page'],
			'where' => $where,
			'type'  => 'select',
		));
		return $search;
	}

	protected function valide($param){
		return array(
			array('type' => 'require', 'value' => $param['data']['uid'], 'msg' => '操作警官uid必须填写！'),
			//array('type' => 'require', 'value' => $param['data']['driving_id'], 'msg' => '驾驶证信息ID必须填写！'),
			//array('type' => 'require', 'value' => $param['data']['vehicle_id'], 'msg' => '行驶证信息ID必须填写！'),
			//array('type' => 'require', 'value' => $param['data']['photos'], 'msg' => '可疑物品照片必须上传！'),
			array('type' => 'require', 'value' => $param['data']['longitude'], 'msg' => '数据记录点经度值必须填写！'),
			array('type' => 'require', 'value' => $param['data']['latitude'], 'msg' => '数据记录点维度值必须填写！'),
		);
	}

	protected function curdData($param){
		return array(
			//'uid'           => isset($param['data']['uid']) ? $param['data']['uid'] : $this->uuid2uid($param['data']['uuid']),
			'uid' =>'1',
			'driving_id'    => $param['data']['driving_id'],
			'vehicle_id'    => $param['data']['vehicle_id'],
			//'idcard_id'    => $param['data']['idcard_id'],
			'photos'        => $param['data']['photos'],
			'description'   => $param['data']['description'],
			'recip'         => get_client_ip(),
			'longitude'     => $param['data']['longitude'],
			'latitude'      => $param['data']['latitude'],
		);
	}
}