<?php
/**
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/13
 * Time: 18:32
 */

namespace Restful\Model;

class DrivingModel extends CurdModel{
	protected $tableName = 'driving_license';

	/**
	 * 获取数据列表
	 * @param $param
	 *
	 * @return array
	 */
	public function lists($param){
		$list = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'type'  => 'select',
		));
		return $list;
	}

	/**
	 * 获取指定ID的信息数据
	 * @param $param
	 * @return array
	 */
	public function info($param){
		$info = $this->curd(array(
			'model' => $this->tableName.'_view',
			'where' => 'id=' . $param['pk'],
			'type'  => 'find',
		));
		return $info;
	}

	/**
	 * 新增一个信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function add($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$add = $this->curd(array(
				'model' => $this->tableName,
				'type' => 'add',
				'data' => $this->curdData($param),
			));
			return $add;
		}
	}

	/**
	 * 编辑指定信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function save($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$save = $this->curd(array(
				'model' => $this->tableName,
				'type' => 'save',
				'data' => $this->curdData($param),
				'where' => 'id=' . $param['pk']
			));
			return $save;
		}
	}

	/**
	 * 驾驶证查询列表
	 * @param $param
	 * @return array
	 */
	public function search($param){
		$type = ($param['data']['search']['type'])?$param['data']['search']['type']:$param['data']['search']['value'];
		switch ($type){
			case 'num':
				$where = 'driving_license_main_number LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'name':
				$where = 'name LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			default:
				$where = 'id <> 0';
		}
		$search = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'where' => $where,
			'type'  => 'select',
		));
		return $search;
	}

	/**
	 * 数据验证
	 * @param $param
	 * @return array
	 */
	protected function valide($param){
		return array(
			array('type' => 'require', 'value' => $param['data']['name'], 'msg' => '真实姓名必须填写！'),
			array('type' => 'require', 'value' => $param['data']['driving_license_main_number'], 'msg' => '驾驶证号码必须填写！'),
			//array('type' => 'idcard', 'value' => $param['data']['driving_license_main_number'], 'msg' => '驾驶证号码格式不正确！'),
			array('type' => 'require', 'value' => $param['data']['birthday'], 'msg' => '出生年月日必须填写！'),
			array('type' => 'require', 'value' => $param['data']['sex'], 'msg' => '性别必须填写！'),
			array('type' => 'require', 'value' => $param['data']['driving_license_main_nationality'], 'msg' => '国籍必须填写！'),
			array('type' => 'require', 'value' => $param['data']['driving_license_main_valid_from'], 'msg' => '有效期限必须填写！'),
			array('type' => 'require', 'value' => $param['data']['driving_license_main_valid_end'], 'msg' => '到期日期必须填写！'),
			array('type' => 'require', 'value' => $param['data']['issue_date'], 'msg' => '初次领证日期必须填写！'),
			array('type' => 'require', 'value' => $param['data']['driving_license_type'], 'msg' => '驾驶证证件名称必须填写！'),
			array('type' => 'require', 'value' => $param['data']['driving_license'], 'msg' => '驾驶证图片必须上传！'),
			array('type' => 'require', 'value' => $param['data']['drivetype'], 'msg' => '准驾类型必须填写！'),
			array('type' => 'require', 'value' => $param['data']['longitude'], 'msg' => '数据记录点经度值必须填写！'),
			array('type' => 'require', 'value' => $param['data']['latitude'], 'msg' => '数据记录点维度值必须填写！'),
		);
	}

	/**
	 * 构造数据
	 * @param $param
	 * @return array
	 */
	protected function curdData($param){
		return array(
			//'uid'                               => isset($param['data']['uid']) ? $param['data']['uid'] : $this->uuid2uid($param['data']['uuid']),
			'uid'                               => 1,
			'name'                              => $param['data']['name'],
			'driving_license_main_number'       => $param['data']['driving_license_main_number'],
			'birthday'                          => $param['data']['birthday'],
			'sex'                               => $param['data']['sex'],
			'driving_license_main_nationality'  => $param['data']['driving_license_main_nationality'],
			'driving_license_main_valid_from'   => $param['data']['driving_license_main_valid_from'],
			'driving_license_main_valid_end'    => $param['data']['driving_license_main_valid_end'],
			'issue_date'                        => $param['data']['issue_date'],
			'driving_license_type'              => $param['data']['driving_license_type'],
			'driving_license'                   => $param['data']['driving_license'],
			'drivetype'                         => $param['data']['drivetype'],
			'recip'                             => get_client_ip(),
			'longitude'                         => $param['data']['longitude'],
			'latitude'                          => $param['data']['latitude'],
		);
	}
}