<?php
/**
 * 身份证数据模型
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/12
 * Time: 17:03
 */
namespace Restful\Model;

class IdcardModel extends CurdModel{
	protected $tableName = 'Idcard';

	/**
	 * 获取数据列表
	 * @param $param
	 *
	 * @return array
	 */
	public function lists($param){
		$list = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'type'  => 'select',
		));
		return $list;
	}

	/**
	 * 新增一个信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function add($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$add = $this->curd(array(
				'model' => $this->tableName,
				'type' => 'add',
				'data' => $this->curdData($param),
			));
			return $add;
		}
	}

	/**
	 * 获取指定ID的信息数据
	 * @param $param
	 * @return array
	 */
	public function info($param){
		$info = $this->curd(array(
			'model' => $this->tableName.'_view',
			'where' => 'id=' . $param['pk'],
			'type'  => 'find',
		));
		return $info;
	}

	/**
	 * 编辑指定信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function save($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$save = $this->curd(array(
				'model' => $this->tableName,
				'type' => 'save',
				'data' => $this->curdData($param),
				'where' => 'id=' . $param['pk']
			));
			return $save;
		}
	}

	/**
	 * 身份证查询列表
	 * @param $param
	 * @return array
	 */
	public function search($param){
		$type = ($param['data']['search']['type'])?$param['data']['search']['type']:$param['data']['search']['value'];
		switch ($type){
			case 'name': //姓名
				$where = 'realname LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'num': //身份证号
				$where = 'id_number LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			default:
				$where = 'id <> 0';
		}
		$search = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'where' => $where,
			'type'  => 'select',
		));
		return $search;
	}

	/**
	 * 数据验证
	 * @param $param
	 * @return array
	 */
	protected function valide($param){
		return array(
			array('type' => 'require', 'value' => $param['data']['name'], 'msg' => '真实姓名必须填写！'),
			array('type' => 'require', 'value' => $param['data']['id_number'], 'msg' => '身份证号码必须填写！'),
			//array('type' => 'idcard', 'value' => $param['data']['id_number'], 'msg' => '身份证号码格式不正确！'),
			array('type' => 'require', 'value' => $param['data']['birthday'], 'msg' => '出生年月日必须填写！'),
			array('type' => 'require', 'value' => $param['data']['sex'], 'msg' => '性别必须填写！'),
			array('type' => 'require', 'value' => $param['data']['people'], 'msg' => '民族必须填写！'),
			array('type' => 'require', 'value' => $param['data']['address'], 'msg' => '地址必须填写！'),
			array('type' => 'require', 'value' => $param['data']['type'], 'msg' => '身份证类型必须填写！'),
			//array('type' => 'require', 'value' => $param['data']['headimg'], 'msg' => '身份证头像必须设定！'),
			array('type' => 'require', 'value' => $param['data']['idcard_1'], 'msg' => '身份证正面图片上传ID必须填写！'),
			array('type' => 'require', 'value' => $param['data']['longitude'], 'msg' => '数据记录点经度值必须填写！'),
			array('type' => 'require', 'value' => $param['data']['latitude'], 'msg' => '数据记录点维度值必须填写！'),
		);
	}

	/**
	 * 构造数据
	 * @param $param
	 * @return array
	 */
	protected function curdData($param){
		return array(
			//'uid'       => isset($param['data']['uid']) ? $param['data']['uid'] : $this->uuid2uid($param['data']['uuid']),
			'uid'       => '1',
			'name'      => $param['data']['name'],
			'id_number' => $param['data']['id_number'],
			'birthday'  => $param['data']['birthday'],
			'sex'       => $param['data']['sex'],
			'people'    => $param['data']['people'],
			'address'   => $param['data']['address'],
			'type'      => $param['data']['type'],
			'headimg'   => $param['data']['headimg'],
			'idcard_1'  => $param['data']['idcard_1'],
			//'idcard_2'  => $param['data']['idcard_2'],
			'idcard_2'  => $param['data']['idcard_1'],
			'recip'     => get_client_ip(),
			'longitude' => $param['data']['longitude'],
			'latitude'  => $param['data']['latitude'],
		);
	}

}