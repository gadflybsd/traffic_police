<?php
/**
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/13
 * Time: 18:24
 */

namespace Restful\Model;


class CurdModel extends CommonModel{
	/**
	 * 数列列表
	 * @param $param
	 * @return array
	 */
	public function lists($param){
		$list = $this->curd(array(
			'model' => $this->tableName.'View',
			'page'  => $param['data']['page'],
			'type'  => 'select',
		));
		return $list;
	}

	/**
	 * 获取指定ID的信息数据
	 * @param $param
	 * @return array
	 */
	public function info($param){
		$info = $this->curd(array(
			'model' => $this->tableName.'View',
			'where' => 'id=' . $param['data']['id'],
			'type'  => 'find',
		));
		return $info;
	}

	/**
	 * 公共搜索模型
	 * @param $where
	 * @param $page
	 * @return array
	 */
	public function search($where, $page){
		return $this->curd(array(
			'model' => $this->tableName.'View',
			'page'  => $page,
			'where' => $where,
			'type'  => 'select',
		));
	}

	/**
	 * 新增一个信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function add($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$add = $this->curd(array(
				'type' => 'add',
				'data' => $this->curdData($param)
			));
			return $add;
		}
	}

	/**
	 * 编辑指定信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function save($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$save = $this->curd(array(
				'type' => 'save',
				'data' => $this->curdData($param),
				'where' => 'id=' . $param['pk']
			));
			return $save;
		}
	}

	/**
	 * 删除指定的信息数据
	 * @param $param
	 * @return array
	 */
	public function del($param){
		$del = $this->curd(array(
			'type' => 'delete',
			'where' => 'id=' . $param['data']['id'],
		));
		return $del;
	}

	/**
	 * 新增黑名单操作
	 * @param $id
	 * @return array
	 */
	public function blacklist($id){
		$param = $this->where('id='.$id)->find();
		switch ($this->tableName){
			case 'idcard':
				$field = 'idcard_id';
				break;
			case 'DrivingLicense':
				$field = 'driving_id';
				break;
			case 'VehicleLicense':
				$field = 'vehicle_id';
				break;
			case 'VehicleExceptionalLog':
				$field = 'vehicle_exceptional_id';
				break;
		}
		$add = $this->curd(array(
			'model' => 'Blacklist',
			'type'  => 'add',
			'data'  => array(
				'uid'       => $param['uid'],
				$field      => $param['id'],
				'recip'     => $param['recip'],
				'dateline'  => $param['dateline'],
				'checkuid'  => isset($param['uid']) ? $param['uid'] : $this->uuid2uid($param['uuid']),
				'checkip'   => get_client_ip(),
			)
		));
		return $add;
	}

	/**
	 * 取消黑名单操作
	 * @param $id
	 * @return array
	 */
	public function remove_blacklist($id){
		$black = M('Blacklist')->where('id='.$id)->find('idcard_id, driving_id, vehicle_id, vehicle_exceptional_id');
		if($black['idcard_id'] != 0){
			$number = M('Idcard')->where('id='.$black['idcard_id'])->getField('id_number');
			$idcard_id = M('Idcard')->where('id_number="'.$number.'"')->getField('id');
			$where = 'idcard_id IN ('.implode(',', $idcard_id).')';
		}elseif ($black['driving_id'] != 0){
			$number = M('DrivingLicense')->where('id='.$black['driving_id'])->getField('driving_license_main_number');
			$driving_id = M('DrivingLicense')->where('driving_license_main_number="'.$number.'"')->getField('id');
			$where = 'driving_id IN ('.implode(',', $driving_id).')';
		}elseif ($black['vehicle_id'] != 0){
			$number = M('VehicleLicense')->where('id='.$black['vehicle_id'])->getField('vehicle_license_main_plate_num');
			$vehicle_id = M('VehicleLicense')->where('vehicle_license_main_plate_num="'.$number.'"')->getField('id');
			$where = 'vehicle_id IN ('.implode(',', $vehicle_id).')';
		}elseif ($black['vehicle_exceptional_id'] != 0){
			$where = 'vehicle_exceptional_id = '.$black['vehicle_exceptional_id'];
		}else{
			$where = 'id=0';
		}
		return $this->curd(array(
			'model' => 'Blacklist',
			'type'  => 'save',
			'where' => $where,
			'data'  => array('status' => 0)
		));
	}

	/**
	 * 数据验证
	 * @return array
	 */
	protected function valide(){
		return array();
	}

	/**
	 * 构造数据
	 * @param $param
	 * @return mixed
	 */
	protected function curdData($param){
		return $param['data'];
	}
}