<?php
/**
 * 行驶证数据模型
 * Created by PhpStorm.
 * User: gadflybsd
 * Date: 2017/5/12
 * Time: 13:42
 */
namespace Restful\Model;

class VehicleModel extends CurdModel{
	protected $tableName = 'vehicle_license';

	/**
	 * 获取数据列表
	 * @param $param
	 *
	 * @return array
	 */
	public function lists($param){
		$list = $this->curd(array(
			'model' => $this->tableName.'_view',
			'page'  => $param['data']['page'],
			'type'  => 'select',
		));
		return $list;
	}

	/**
	 * 新增一个信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function add($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$add = $this->curd(array(
				'model' => $this->tableName,
				'type' => 'add',
				'data' => $this->curdData($param),
			));
			return $add;
		}
	}

	/**
	 * 获取指定ID的信息数据
	 * @param $param
	 * @return array
	 */
	public function info($param){
		$info = $this->curd(array(
			'model' => $this->tableName.'_view',
			'where' => 'id=' . $param['pk'],
			'type'  => 'find',
		));
		return $info;
	}

	/**
	 * 编辑指定信息数据
	 * @param mixed|string $param
	 * @return array
	 */
	public function save($param){
		$validator = $this->validators($this->valide($param));
		if ($validator['type'] != 'Success') {
			return $validator;
		} else {
			$save = $this->curd(array(
				'model' => $this->tableName,
				'type' => 'save',
				'data' => $this->curdData($param),
				'where' => 'id=' . $param['pk']
			));
			return $save;
		}
	}

	/**
	 * 行驶证查询列表
	 * @param $param
	 * @return array
	 */
	public function search($param){
		$type = ($param['data']['search']['type'])?$param['data']['search']['type']:$param['data']['search']['value'];
		switch ($type){
			case 'num':
				$where = 'vehicle_license_main_plate_num LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'owner':
				$where = 'vehicle_license_main_owner LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'vin':
				$where = 'vehicle_license_main_vin LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			case 'no':
				$where = 'vehicle_license_main_engine_no LIKE "%' . $param['data']['search']['field'] . '%"';
				break;
			default:
				$where = 'id <> 0';
		}
		$search = $this->curd(array(
			'model' => $this->tableName.'View',
			'page'  => $param['data']['page'],
			'where' => $where,
			'type'  => 'select',
		));
		return $search;
	}

	/**
	 * 数据验证
	 * @param $param
	 * @return array
	 */
	protected function valide($param){
		return array(
			array('type' => 'require', 'value' => $param['data']['name'], 'msg' => '真实姓名必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_vehicle_type'], 'msg' => '车辆类型必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_plate_num'], 'msg' => '车辆号牌号码必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_owner'], 'msg' => '车辆所有人必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_user_character'], 'msg' => '使用性质必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_model'], 'msg' => '品牌型号必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_vin'], 'msg' => '车辆识别代号必须填写！'),
			array('type' => 'require', 'value' => $param['data']['address'], 'msg' => '地址必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_engine_no'], 'msg' => '发动机号码必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license_main_register_date'], 'msg' => '身份证头像必须设定！'),
			array('type' => 'require', 'value' => $param['data']['issue_date'], 'msg' => '初次领证日期必须填写！'),
			array('type' => 'require', 'value' => $param['data']['vehicle_license'], 'msg' => '驾驶证图片必须上传！'),
			array('type' => 'require', 'value' => $param['data']['longitude'], 'msg' => '数据记录点经度值必须填写！'),
			array('type' => 'require', 'value' => $param['data']['latitude'], 'msg' => '数据记录点维度值必须填写！'),
		);
	}

	/**
	 * 构造数据
	 * @param $param
	 * @return array
	 */
	protected function curdData($param){
		return array(
			//'uid' => isset($param['data']['uid']) ? $param['data']['uid'] : $this->uuid2uid($param['data']['uuid']),
			'uid' => '1',
			'vehicle_license_main_vehicle_type'     => $param['data']['vehicle_license_main_vehicle_type'],
			'vehicle_license_main_plate_num'        => $param['data']['vehicle_license_main_plate_num'],
			'vehicle_license_main_owner'            => $param['data']['vehicle_license_main_owner'],
			'vehicle_license_main_user_character'   => $param['data']['vehicle_license_main_user_character'],
			'vehicle_license_main_model'            => $param['data']['vehicle_license_main_model'],
			'vehicle_license_main_vin'              => $param['data']['vehicle_license_main_vin'],
			'address'                               => $param['data']['address'],
			'vehicle_license_main_engine_no'        => $param['data']['vehicle_license_main_engine_no'],
			'vehicle_license_main_register_date'    => $param['data']['vehicle_license_main_register_date'],
			'issue_date'                            => $param['data']['issue_date'],
			'vehicle_license'                       => $param['data']['vehicle_license'],
			'recip'                                 => get_client_ip(),
			'longitude'                             => $param['data']['longitude'],
			'latitude'                              => $param['data']['latitude'],
		);
	}
}