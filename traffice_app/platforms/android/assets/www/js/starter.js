var jq = $.noConflict();
(function(){
	var app = angular.module('starter', ['ionic', 'ngCordova', 'oc.lazyLoad', 'starter.services']);
	app.run(function($ionicPlatform, device) {
		$ionicPlatform.ready(function () {
			if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
				cordova.plugins.Keyboard.disableScroll(true);
			}
			window.StatusBar.styleDefault();
			window.StatusBar.styleLightContent();
			device.getDevice();
		});
	});
	app.constant('configs', {
		timeout: 30,
		sqlite: false,
		url: {
			//restful: 'http://222.219.73.36/public_html/restful.php?s=Index/angular.html',
			//upload: 'http://222.219.73.36/public_html/restful.php?s=Index/uploadPicture.html',
			restful: 'http://home.hzxiansheng.com/public_html/restful.php?s=Index/angular.html',
			upload: 'http://home.hzxiansheng.com/public_html/restful.php?s=Index/uploadPicture.html',
		},
		state: {
			home: 'home',
			login: 'login',
			scout: 'scout',
			register: 'register',
			forgot_password: 'forgot_password',
			vehicle: 'vehicle',
			vehicle_action: 'vehicle_action',
			driving: 'driving',
			driving_action: 'driving_action',
			idcard: 'idcard',
			idcard_action: 'idcard_action',
		},
		model: {
			user: 'user',
			vehicle: 'vehicle',
			driving: 'driving',
			idcard: 'idcard',
			VehicleExceptional: 'VehicleExceptional'
		},
		debug: true
	});
	app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider, configs) {
		$httpProvider.defaults.headers = {
			post: {'Content-Type': 'application/x-www-form-urlencoded'},
			get: {'Content-Type': 'application/x-www-form-urlencoded'},
			put: {'Content-Type': 'application/x-www-form-urlencoded'},
			delete: {'Content-Type': 'application/x-www-form-urlencoded'}
		}
		$httpProvider.defaults.transformRequest = function (obj) {
			var str = [];
			for (var p in obj) {
				str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
			}
			return str.join('&');
		};
		$ionicConfigProvider.platform.android.tabs.style('standard');
		$ionicConfigProvider.platform.android.tabs.position('bottom');
		$ionicConfigProvider.platform.android.navBar.alignTitle('center');
		$ionicConfigProvider.platform.android.backButton.previousTitleText('').icon('ion-android-arrow-back');
		$ionicConfigProvider.platform.android.views.transition('android');
		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: 'templates/home.html',
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'js/controller/homeCtrl.js'
						]);
					}
				},
				controller: 'homeCtrl'
			})
			.state('login', {
				url: '/login',
				templateUrl: 'templates/login.html',
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'js/controller/loginCtrl.js'
						]);
					}
				},
				controller: 'loginCtrl'
			})
			.state('register', {
				url: '/register',
				templateUrl: 'templates/register.html',
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'js/controller/registerCtrl.js'
						]);
					}
				},
				controller: 'registerCtrl'
			})
			.state('forgot_password', {
				url: '/forgot_password',
				templateUrl: 'templates/forgot_password.html',
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'js/controller/forgot_passwordCtrl.js'
						]);
					}
				},
				controller: 'forgot_passwordCtrl'
			})
			.state('scout', {
				url: '/scout',
				templateUrl: 'templates/scout.html',
				resolve: {
					des: function($ocLazyLoad) {
						return $ocLazyLoad.load([
							'js/controller/scoutCtrl.js'
						]);
					}
				},
				controller: 'scoutCtrl'
			})
			.state('result_scan', {
				url: '/result_scan',
				templateUrl: 'templates/result_scan.html'
			})
			.state('result', {
				url: '/result',
				templateUrl: 'templates/result.html'
			});
		$urlRouterProvider.otherwise('/'+configs.state.login);
	});
}())

