/**
 * Created by gadflybsd on 2017/5/16.
 */
angular.module('starter').controller('scoutCtrl', function($scope, $ionicModal, $state, $cordovaGeolocation, unit, imgPicker, service, device, configs) {
	//service.userState('login', function(user, identity){
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		position = {
			coords:{
				longitude:99.1760956443,
				latitude:25.1316674869
			}
		};
		$cordovaGeolocation.getCurrentPosition(posOptions)
			.then(function (position1) {
				position.coords.longitude = position1.coords.longitude;
				position.coords.latitude = position1.coords.latitude;
			}, function(error){
				unit.log('cordova Geolocation Error:', error);
			});
		$scope.vehicle_exceptional = {
			vehicleSn: '',
			idcard: [],
			driving: {},
			vehicle: {},
			face: [],
			uid: 1,
			longitude: position.coords.longitude,
			latitude: position.coords.latitude
		};
		$scope.pictureImg = [];
		$scope.pictureFace = [];
		$ionicModal.fromTemplateUrl('submitModal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.submitModal = modal;
		});
		$scope.ocr_scan = function(type){
			switch (type){
				case 'idcard':
					imgPicker.camera(function(url){
						imgPicker.upload_ocr(url, '上传身份证证图片', {type: 'idcard', uid: 1}, function(result){
							var idcard = result.ocr;
							idcard.uid = 1;
							jq('#pictureIdcard>div').append('<img src="'+url+'" style="width:180px;height: 120px; padding:5px;">');
							idcard.headimg = idcard.head_portrait.image;
							idcard.longitude = position.coords.longitude;
							idcard.latitude = position.coords.latitude;
							idcard.idcard_1 = result.data.id;
							$scope.vehicle_exceptional.idcard.push(idcard)
							//$scope.idcardModal.show();
						});
					});
					break;
				case 'driving':
					imgPicker.camera(function(url){
						imgPicker.upload_ocr(url, '上传驾驶证图片', {type: 'driving', uid: 1}, function(result){
							var driving = result.ocr;
							var valid = driving.driving_license_main_valid_from.split(' 至 ');
							driving.uid = 1;
							driving.driving_license = result.data.id;
							driving.driving_license_main_valid_end = valid[1];
							driving.driving_license_type = driving.type;
							driving.longitude = position.coords.longitude;
							driving.latitude = position.coords.latitude;
							$scope.vehicle_exceptional.driving = driving;
							//$scope.drivingModal.show();
						});
					});
					break;
				case 'vehicle':
					imgPicker.camera(function(url){
						imgPicker.upload_ocr(url, '上传行驶证图片', {type: 'vehicle', uid: 1}, function(result){
							var vehicle = result.ocr;
							vehicle.uid = 1;
							vehicle.vehicle_license = result.data.id;
							vehicle.longitude = position.coords.longitude;
							vehicle.latitude = position.coords.latitude;
							$scope.vehicle_exceptional.vehicle = vehicle;
							//$scope.vehicleModal.show();
						});
					});
					break;
				case 'face':
					imgPicker.camera(function(url){
						$scope.pictureFace.push(url);
						jq('#pictureFace>div').append('<img src="'+url+'" style="width:180px;height: 120px; padding:5px;">');
						imgPicker.upload_ocr(url, '上传行人脸图片', {type: 'face', uid: 1}, function(result){
							var face = result.ocr;
							face.uid = 1;
							face.src = url;
							face.face_license = result.data.id;
							face.longitude = position.coords.longitude;
							face.latitude = position.coords.latitude;
							$scope.vehicle_exceptional.face.push(face);
							//$scope.faceModal.show();
						});
					});
					break;
				case 'vehicleSn':
					"车牌扫描识别部分代码写在这里";
					QRScanner.scan(function(err, obj){
						console.log(err, obj);
						if(err){
							console.debug("err " + JSON.stringify(err));
						}else{
							$scope.vehicle_exceptional.vehicle.vehicle_license_main_plate_num = obj.car_number;
							imgPicker.upload(obj.img_uri, '上传车牌图片', {type: 'vehicle', uid: 1}, function(result){
								$scope.vehicle_exceptional.vehicle.vehicle_license_main_plate_ids = result.data.id;
								$scope.$apply();
							});
						}
					});
					break;
				default:
					imgPicker.camera(function(url){
						$scope.pictureImg.push(url);
						jq('#pictureImg>div').append('<img src="'+url+'" style="width:180px;height: 120px; padding:5px;">');
					});
					break;
			}
		}
		$scope.openModal = function(){
			$scope.submitModal.show();
		}
		$scope.closeModal = function(){
			$scope.submitModal.hide();
		}
		$scope.$on('$destroy', function() {
			$scope.submitModal.remove();
		});
		$scope.toggleGroup = function(item) {
			item.show = !item.show;
		};
		$scope.isGroupShown = function(item) {
			return item.show;
		};
		$scope.submit = function(){
			if($scope.pictureImg.length != 0){
				imgPicker.uploads($scope.pictureImg, '上传物品图片', {type: 'picture', uid: 1}, function(result){
					var photos = [];
					var url = [];
					angular.forEach(result, function(obj){
						photos.push(obj.data.id);
						url.push(obj.data.url);
					});
					$scope.vehicle_exceptional.photo_ids = photos.join(', ');
					$scope.vehicle_exceptional.photo_url = url.join(', ');
					$scope.vehicle_exceptional.photo_count = photos.length;
					service.restful('post', {model: configs.model.VehicleExceptional, module: 'add', data: $scope.vehicle_exceptional}, function(result){
						console.log('上传物品图片', result);
						$scope.vehicle_exceptional = {
							idcard: [],
							driving: {},
							vehicle: {},
							face: [],
							uid: 1,
							longitude: position.coords.longitude,
							latitude: position.coords.latitude
						};
						$scope.pictureImg = [];
						$scope.pictureFace = [];
						$scope.submitModal.hide();
						jq('#pictureIdcard>div').empty();
						jq('#pictureFace>div').empty();
						jq('#pictureImg>div').empty();
						//$state.go(configs.state.scout, {}, { reload: true });
					});
				});
			}else{
				service.restful('post', {model: configs.model.VehicleExceptional, module: 'add', data: $scope.vehicle_exceptional}, function(result){
					console.log('没有上传图片', result);
					$scope.vehicle_exceptional = {
						idcard: [],
						driving: {},
						vehicle: {},
						face: [],
						uid: 1,
						longitude: position.coords.longitude,
						latitude: position.coords.latitude
					};
					$scope.pictureImg = [];
					$scope.pictureFace = [];
					$scope.submitModal.hide();
					jq('#pictureIdcard>div').empty();
					jq('#pictureFace>div').empty();
					jq('#pictureImg>div').empty();
					//$state.go(configs.state.scout, {}, { reload: true });
				});
			}
		}
	//}, true);
});