/**
 * Created by gadflybsd on 2017/5/15.
 */
angular.module('starter').controller('registerCtrl', function($scope, $state, unit, service, configs) {
	"手机App用户注册";
	$scope.register = {}
	$scope.submit = function(){
		service.restful('post', {model: configs.model.user, module: 'register', data: $scope.register}, function(result){
			if(result.type == 'Success') $state.go(configs.state.scout, {}, { reload: true });;
		})
	}
});