
angular.module("starter.controllers", [])

    .controller("MainCtrl", ["$scope", "$stateParams", "$ionicModal", "$location", "$rootScope", "$ionicPopup", function(e, t, i, o, a, s) {
        e.historyBack = function() {
            window.history.back()
        },
        a.newRecordName = "",
        e.createNewRecord = function(t) {
            s.alert({
                    title: "选择类型",
                    template: "必须选择违章类型",
                    okType: "button-clear"
                });
        },
        
        i.fromTemplateUrl("templates/modal/new-record.html", {
            scope: e,
            animation: "slide-in-up"
        }).then(function(t) {
            e.newRecordModal = t
        }),
        e.openNewRecord = function() {
            e.newRecordModal.show()
        },
        e.closeNewRecord = function() {
            e.newRecordModal.hide()
        },
        
        e.$on("$stateChangeStart", function() {
            e.newGroupModal && e.closeNewGroup()
        })
    }])
   
    .controller("DisableCtrl", ["$scope", function($scope) {
        $scope.thetext = "",
        e.b1 = function() {
            console.log("B1")
        }
        ,
        e.b2 = function() {
            console.log("B2")
        }
    }
]);