var app = angular.module('starter.services', []);
app.factory('unit', function(configs){
	var wait=60;
	return {
		sendSmsTimer: function(ele){
			if (wait == 0) {
				ele.removeAttr('disabled').text("重新获取验证码");
				wait = 60;
			} else {
				ele.attr('disabled', 'disabled').text(wait + "秒后重发验证码");
				wait--;
				setTimeout(function(){this.sendSmsTimer(ele)}, 1000)
			}
		},
		isEmpty: function (e) {
			if(!e || angular.isUndefined(e) || e === null)
				return true;
			else
				return false;
		},
		isEmptyObject: function(e){
			if(angular.isUndefined(e) || e === null){
				return true;
			}else{
				var t;
				for (t in e)
					return !true;
				return !false;
			}
		},
		in_array: function(search, array) {
			for(var i in array){
				if(array[i].toString() == search) return true;
			}
			return false;
		},
		is_array: function (value) {
			return value &&
				typeof value === 'object' &&
				typeof value.length === 'number' &&
				typeof value.splice === 'function' &&
				!(value.propertyIsEnumerable('length'));
		},
		uuid: function(){
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
				return v.toString(16);
			});
		},
		log: function(title, msg, callback){
			if(configs.debug){
				console.log(title, msg);
				if(typeof callback == 'function') callback();
			}
		}
	}
});
app.factory('device', function(unit, $window, $cordovaDevice){
	return {
		getDevice: function(){
			if(unit.isEmpty($window.localStorage.getItem("device"))){
				document.addEventListener("deviceready", function (){
					var device = $cordovaDevice.getDevice();
					$window.localStorage.setItem("device", JSON.stringify(device));
				});
			}
			return JSON.parse($window.localStorage.getItem("device"));
		},
		getUUID: function () {
			//return unit.uuid();
			var device = this.getDevice();
			return device.uuid;
		},
		getPlatform: function(){
			return this.getDevice().platform;
		},
		getVersion: function(){
			return this.getDevice().version;
		}
	}
});
app.factory('ngSwal', function($ionicActionSheet, unit){
	var defaults = {
		allowOutsideClick: false,		// 如果设置为“true”，用户可以通过点击警告框以外的区域关闭警告框。
		confirmButtonColor: "#DD6B55",	// 该参数用来改变确认按钮的背景颜色（必须是一个HEX值）。
		confirmButtonText: "确定",		// 该参数用来改变确认按钮上的文字。如果设置为"true"，那么确认按钮将自动将"Confirm"替换为"OK"。
		type: 'info',					// 窗口的类型。有4种类型的图标动画："warning", "error", "success" 和 "info".可以将它放在"type"数组或通过第三个参数传递。
		title: null,					// 窗口的名称。可以通过对象的"title"属性或第一个参数进行传递。
		text: null,						// 窗口的描述。可以通过对象的"text"属性或第二个参数进行传递。
		showCancelButton: false,		// 如果设置为“true”，“cancel”按钮将显示，点击可以关闭警告框。
		showConfirmButton: true,		// 如果设置为“false”，“Confirm”按钮将不显示。
		cancelButtonText: '取消',		// 该参数用来改变取消按钮的文字。
		closeOnConfirm: true,			// 如果希望以后点击了确认按钮后模态窗口仍然保留就设置为"false"。该参数在其他SweetAlert触发确认按钮事件时十分有用。
		timer: null						// 警告框自动关闭的时间。单位是ms。
	}
	function getTitle(type, title){
		if(unit.isEmptyObject(title)){
			switch(type){
				case 'success':
					return '操作成功！';
				case 'error':
					return '操作失败！';
				case 'warning':
					return '操作警告！';
				case 'info':
					return '温馨提示！';
			}
		}else{
			return title;
		}
	}
	return {
		alert: function(param, callback){
			var config = {}
			config.type = (unit.isEmptyObject(param.type))?'success':param.type.toLowerCase();
			config.title = getTitle(config.type, param.title)
			swal(angular.extend(defaults, param, config), function(){
				if(typeof(callback) == 'function') callback();
			});
		},
		confirm: function(param, callback_ok, callback_cancel){
			var config = {}
			config.type = (unit.isEmptyObject(param.type))?'warning':param.type.toLowerCase();
			config.title = getTitle(config.type, param.title);
			config.showCancelButton = true;
			swal(angular.extend(defaults, param, config), function(isConfirm){
				if (isConfirm) {
					if(typeof(callback_ok) == 'function') callback_ok();
				}else{
					if(typeof(callback_cancel) == 'function') callback_cancel();
				}
			});
		},
		progress: function(param){
			var config = {
				type: 'info',
				html: true,
				title: getTitle('info'),
				text: '<h5 style="color: #797979">正在 <span class="progressText">'+param.text+'</span> 请等待！</h5><div class = "progress progress-striped active">' +
				'<div class = "progress-bar progress-bar-success" role="progressbar" style="width: '+param.width+'%;"></div></div>',
				showLoaderOnConfirm: true,
			};
			swal(angular.extend(defaults, config));
			swal.disableButtons();
		},
		prompt: function (param, callback) {
			var config ={}
			config.type = (unit.isEmptyObject(param.type))?'input':param.type.toLowerCase();
			config.showCancelButton = true;
			config.closeOnConfirm= false;
			config.disableButtonsOnConfirm = true;
			config.animation = "slide-from-top";
			swal(angular.extend(defaults, config, param), function(inputValue){
				if(inputValue) if(typeof(callback) == 'function') callback(inputValue);
			});
		},
		close: function(){
			swal.close();
		},
		enableButtons: function(){
			swal.enableButtons();
		},
		disableButtons: function(){
			swal.disableButtons();
		},
		sheetCamera: function(Camera, defaults, callback){
			$ionicActionSheet.show({
				buttons: [
					{text: '拍照'},
					{text: '从相册选择'}
				],
				titleText: '选择照片',
				cancelText: '取消',
				cancel: function(){},
				buttonClicked: function(index){
					if(index == 0){
						defaults.sourceType = Camera.PictureSourceType.CAMERA;
					}else if(index == 1){
						defaults.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
					}
					if(typeof(callback) == 'function') callback(defaults);
					return true;
				}
			});
		}
	}
});
app.factory('imgPicker', function($cordovaCamera, $cordovaBarcodeScanner, $cordovaFileTransfer, $state, configs, unit, ngSwal){
	var Camera = {
		DestinationType:{
			DATA_URL : 0,   //返回Base64编码字符串的图像数据
			FILE_URI : 1    //返回图像文件的URI
		},
		PictureSourceType: {
			PHOTOLIBRARY : 0,
			CAMERA : 1,
			SAVEDPHOTOALBUM : 2
		},
		EncodingType:{
			JPEG : 0,       // 返回JPEG格式图片
			PNG : 1         // 返回PNG格式图片
		},
		PopoverArrowDirection: {
			ARROW_UP: 1,
			ARROW_DOWN: 2,
			ARROW_LEFT: 4,
			ARROW_RIGHT: 8,
			ARROW_ANY: 15
		}
	}
	var defaults = {
		quality: 80,                                           //存储图像的质量，范围是[0,100]。
		destinationType: Camera.DestinationType.FILE_URI,       //选择返回数据的格式
		sourceType: Camera.PictureSourceType.CAMERA,            //设定图片来源。
		allowEdit: true,                                        //在选择图片进行操作之前允许对其进行简单编辑。
		encodingType: Camera.EncodingType.JPEG,                 //选择返回图像文件的编码方式
		targetWidth: 800,                                       //以像素为单位的图像缩放宽度，必须和targetHeight同时使用。
		targetHeight: 600,                                      //以像素为单位的图像缩放高度，必须和targetWidth同时使用。
		popoverOptions: Camera.PopoverArrowDirection.ARROW_UP,  //iOS设备弹出框的位置
		saveToPhotoAlbum: false,                                //保存进手机相册
		mediaType: 0,   //可选媒体类型：圖片=0，只允许选择图片將返回指定DestinationType的参数。 視頻格式=1，允许选择视频，最终返回 FILE_URI。ALLMEDIA= 2，允许所有媒体类型的选择。
		cameraDirection: 0,                                     //枪后摄像头类型：Back= 0,Front-facing = 1
		correctOrientation:true                                 // 设置摄像机拍摄的图像是否为正确的方向
	};
	return {
		headimg: function(callback){
			ngSwal.sheetCamera(Camera, defaults, function(defaults){
				var options = {
					quality: 75,
					cameraDirection: 1,
					targetWidth: 128,
					targetHeight: 128
				}
				$cordovaCamera.getPicture(angular.extend(defaults, options)).then(
					function (imageUrl) {
						if(typeof(callback) == 'function') callback(imageUrl);
					}, function (error) {
						unit.log('获取头像出错:', error, function(){
							ngSwal.alert({title: '获取摄像机出错啦!', text: error, type: 'error'}, function(){
								ngSwal.close();
							});
						});
					}
				);
			});
		},
		camera: function(callback){
			var options = {
				quality: 85,
				allowEdit:false,
				targetWidth: 1200,
				targetHeight: 900
			}
			$cordovaCamera.getPicture(angular.extend(defaults, options)).then(
				function (imageUrl) {
					if(typeof(callback) == 'function') callback(imageUrl);
				}, function (error) {
					unit.log('获取摄像头出错:', error, function(){
						ngSwal.alert({title: '获取摄像机出错啦!', text: error, type: 'error'}, function(){
							ngSwal.close();
						});
					});
				}
			);
		},
		photo: function(callback){
			ngSwal.sheetCamera(Camera, defaults, function(defaults){
				$cordovaCamera.getPicture(defaults).then(
					function (imageUrl) {
						if(typeof(callback) == 'function') callback(imageUrl);
					}, function (error) {
						unit.log('获取相册出错:', error, function(){
							ngSwal.alert({title: '获取摄像机出错啦!', text: error, type: 'error'}, function(){
								ngSwal.close();
							});
						});
					}
				);
			});
		},
		copyPhoto: function(img, callback){
			var targetDirName = cordova.file.dataDirectory;
			return Promise.all([
				new Promise(function (resolve, reject) {
					resolveLocalFileSystemURL(img, resolve, reject);
				}),
				new Promise(function (resolve, reject) {
					resolveLocalFileSystemURL(targetDirName, resolve, reject);
				})
			]).then(function (files) {
				var sourceFile = files[0];
				var targetDir = files[1];
				sourceFile.copyTo(targetDir, sourceFile.name, function(){
					if(typeof callback == 'function') callback(targetDirName + sourceFile.name);
				}, function(error){
					unit.log('复制图片出错:', error, function(){
						ngSwal.alert({title: '复制图片出错!', text: error, type: 'error'}, function(){
							ngSwal.close();
						});
					});
				});
			}, function(error){
				unit.log('文件操作出错:', error, function(){
					ngSwal.alert({title: '复制图片出错!', text: error, type: 'error'}, function(){
						ngSwal.close();
					});
				});
			});
		},
		upload: function(param, text, options, callback){
			document.addEventListener('deviceready', function () {
				ngSwal.progress({text: text, width: 0});
				$cordovaFileTransfer.upload(configs.url.upload, param, {params: options}).then(
					function (result) {
						angular.element('div.showSweetAlert').find('span.progressText').text(text);
						angular.element('div.showSweetAlert').find('div.progress-bar').css('width', '0%');
						var response =  (result.response)?eval('(' + result.response + ')'):result;
						ngSwal.close();
						unit.log('上传单个文件: ', result);
						if(typeof(callback) == 'function') callback(response);
					}, function (error) {
						unit.log('上传单个文件出错: ', error, function(){
							ngSwal.alert({title: '图片上传出错啦!', text: error, type: 'error'}, function(){
								ngSwal.close();
								if(typeof(callback) == 'function') callback(error);
							});
						});
					}, function (progress) {
						var width = Math.floor(parseInt(progress.loaded) / parseInt(progress.total) * 10000)/100;
						angular.element('div.showSweetAlert').find('div.progress-bar').css('width', width+'%');
					}
				);
			}, false);
		},
		upload_ocr: function(param, text, options, callback){
			document.addEventListener('deviceready', function () {
				ngSwal.progress({text: text, width: 0});
				$cordovaFileTransfer.upload(configs.url.upload, param, {params: options}).then(
					function (result) {
						angular.element('div.showSweetAlert').find('span.progressText').text(text);
						angular.element('div.showSweetAlert').find('div.progress-bar').css('width', '0%');
						var response =  (result.response)?eval('(' + result.response + ')'):result;
						ngSwal.close();
						unit.log('上传单个文件进行识别: ', result);
						if(typeof(callback) == 'function') callback(response);
					}, function (error) {
						unit.log('上传单个文件进行识别出错: ', error, function(){
							ngSwal.alert({title: '图片上传出错啦!', text: error, type: 'error'}, function(){
								ngSwal.close();
								if(typeof(callback) == 'function') callback(error);
							});
						});
					}, function (progress) {
						var width = Math.floor(parseInt(progress.loaded) / parseInt(progress.total) * 10000)/100;
						angular.element('div.showSweetAlert').find('div.progress-bar').css('width', width+'%');
						if(width = 100) angular.element('div.showSweetAlert').find('span.progressText').text('正在识别证件');
					}
				);
			}, false);
		},
		uploads: function(param, text, options, callback){
			document.addEventListener('deviceready', function(){
				var uploads = function(targetPath, num, response){
					$cordovaFileTransfer.upload(configs.url.upload, targetPath[num], {params: options}).then(
						function (result) {
							++num;
							response.push((result.response)?eval('(' + result.response + ')'):null);
							angular.element('div.showSweetAlert').find('span.progressText').text(text + '（' + (num+1) + '/' + targetPath.length +'）');
							angular.element('div.showSweetAlert').find('div.progress-bar').css('width', '0%');
							if(num < targetPath.length){
								uploads(targetPath, num, response);
							}else{
								ngSwal.close();
								if(typeof(callback) == 'function') callback(response);
							}
						}, function (error) {
							if(configs.debug) console.log(error);
							ngSwal.alert({title: '图片上传出错啦!', text: error.info, type: 'error'},
								function(){ ngSwal.close(); });
						}, function (progress) {
							var width = Math.floor(parseInt(progress.loaded) / parseInt(progress.total) * 10000)/100;
							angular.element('div.showSweetAlert').find('div.progress-bar').css('width', width+'%');
						}
					);
				}
				if(unit.isEmptyObject(param)){
					ngSwal.alert({text: '没有指定需要上传的图片', type: 'error'});
				}else{
					if(typeof param == 'string')
						var targetPath = new Array(param);
					else
						var targetPath = param;
					ngSwal.progress({text: text + '（1/' + targetPath.length +'）', width: 0});
					uploads(targetPath, 0, []);
				}
			}, false);
		},
		scanQR: function(callback){
			$cordovaBarcodeScanner
				.scan()
				.then(function(barcodeData) {
					if(barcodeData.format == 'QR_CODE' && barcodeData.cancelled == 0){
						if(typeof(callback) == 'function') callback(barcodeData.text);
					}
				}, function(error) {
					ngSwal.alert({title: '扫描二维码出错啦!', text: err.message, type: 'error'},
						function(){ ngSwal.close(); });
				});
		}
	}
});
app.factory('service', function($http, $q, $window, $ionicLoading, $state, configs, unit, device, ngSwal){
	var deferred = $q.defer();
	return {
		httpRequest: function(method, url, param, callback_success, callback_error){
			$ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0 });
			var config = {
				url: url,
				cache: false,
				timeout: 30000,
			}
			if (!unit.isEmptyObject(param.data)) {
				param.data.uuid = device.getUUID();
			}else{
				param.data = {uuid: device.getUUID()}
			}
			param.data = JSON.stringify(param.data);
			switch(method.toLowerCase()){
				case 'get':
				case 'delete':
					config.method = method.toLowerCase();
					config.params = param;
					break;
				case 'post':
				case 'put':
				default:
					config.method = method.toLowerCase();
					config.data = param;
			}
			$http(config).then(function(result, status, headers, config){
				if(configs.debug) console.log('XMLHttpRequest Success: ', result, status, headers, config);
				$ionicLoading.hide();
				/*if(unit.in_array(method.toLowerCase(), ['post', 'put'])){
					ngSwal.alert({
						text: result.data.msg,
						type: result.data.type.toLowerCase(),
						closeOnConfirm: true,
						showCancelButton: false,
					},function(){
						console.log('alert', result.data);
						if(typeof callback_success == 'function') callback_success(result.data, status, headers, config);
					});
				}else{
					if(typeof callback_success == 'function') callback_success(result.data, status, headers, config);
				}*/
				if(typeof callback_success == 'function') callback_success(result.data, status, headers, config);
			}, function(result, status, headers, config){
				if(configs.debug) console.log('XMLHttpRequest Error: ', status, result, headers, config);
				$ionicLoading.hide();
				ngSwal.alert({
					text: 'HTTP 请求出现错误, 请与管理员联系!',
					type: 'error',
					closeOnConfirm: true,
					confirmButtonText: '确定',
					showCancelButton: false,
				},function(){
					if(typeof callback_error == 'function') callback_error(result, status, headers, config);
				});
			});
			return deferred.promise;
		},
		getLocalityJson: function(files, on_success, on_error){
			$ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0 });
			var sourceFileName = cordova.file.applicationDirectory + files;
			resolveLocalFileSystemURL(sourceFileName, function (fileEntry) {
				fileEntry.file(function (file) {
					var reader = new FileReader();
					reader.onloadend = function() {
						$ionicLoading.hide();
						if(configs.debug) console.log('get Locality Json File Success: ', JSON.parse(this.result));
						if(typeof on_success == 'function') on_success(JSON.parse(this.result));
					};
					reader.readAsText(file);
				}, function(error){
					$ionicLoading.hide();
					if(configs.debug) console.log('get Locality Json File Error: ', error);
					if(typeof on_error == 'function') on_error(error);
				});
			});
		},
		getJson: function (url, callback) {
			$http({url: url, method: 'get'}).success(function(resp){
				if(typeof callback == 'function') callback(resp);
			}).error(function(error){
				console.log('Error: ', error.message);
			});
		},
		restful: function(method, param, callback_success, callback_error){
			this.httpRequest(method, configs.url.restful, param, function(result, status, headers, config){
				console.log('restful', result);
				if(result.type == 'Success'){
					if (!unit.isEmptyObject(result.localStorage)){
						if(typeof result.localStorage == 'object'){
							for (var p in result.localStorage) {
								if(typeof result.localStorage[p] == 'string' && result.localStorage[p] == 'empty'){
									$window.localStorage.removeItem(p);
								}else{
									if(p == 'device'){
										var local = angular.extend(device.getDevice(), result.localStorage[p]);
									}else{
										var local = result.localStorage[p];
									}
									$window.localStorage.setItem(p, (typeof local == 'object') ? JSON.stringify(local) : local);
								}
							}
						}
					}
					if (!unit.isEmptyObject(result.sessionStorage)){
						if(typeof result.localStorage == 'object'){
							for (var p in result.sessionStorage) {
								if(typeof result.sessionStorage[p] == 'string' && result.sessionStorage[p] == 'empty'){
									$window.sessionStorage.removeItem(p);
								}else{
									var session = result.sessionStorage[p];
									$window.sessionStorage.setItem(p, (typeof session == 'object') ? JSON.stringify(session) : session);
								}
							}
						}
					}
				}
				if(typeof callback_success == 'function') callback_success(result.data, status, headers, config);
			}, function(result, status, headers, config){
				if(typeof callback_error == 'function') callback_error(result.data, status, headers, config);
			});
		},
		userState: function(rule, callback, alert){
			"验证用户现在状态";
			var config = {jump: false}
			/*console.log(alert);
			 if(unit.isEmptyObject(alert)) alert = false;
			 console.log(alert);*/
			if(unit.isEmptyObject($window.localStorage.getItem('userInfo'))){
				var user = false;
			}else{
				var user = JSON.parse($window.localStorage.getItem('userInfo'));
			}
			var identity = {
				isLogin: (!user || unit.isEmptyObject(user.mobile))?false:true,
				isRealname: (!user || unit.isEmptyObject(user.is_realname) || user.is_realname == 0)?false:true,
				isBindweixin: (!user || unit.isEmptyObject(user.is_bindweixin) || user.is_bindweixin == 0)?false:true,
				isBindweibo: (!user || unit.isEmptyObject(user.is_bindweibo) || user.is_bindweibo == 0)?false:true,
				isBindqq: (!user || unit.isEmptyObject(user.is_bindqq) || user.is_bindqq == 0)?false:true,
				isEngineer: (!user || unit.isEmptyObject(user.is_engineer) || user.is_engineer == 0)?false:true,
			}
			if(!identity.isLogin)
				user = {headimg: 'img/no-astar.jpg'};
			switch(rule){
				case 'notLogin':
					if(identity.isLogin){
						config = {
							jump: true,
							text: '您已经注册或登录！'
						}
					}
					break;
				case 'login':
					if(!identity.isLogin){
						config = {
							jump: true,
							text: '您尚未注册或登录！',
							button: '现在登录',
							to: 'login'
						}
					}
					break;
				case 'register':
					if(!identity.isLogin){
						config = {
							jump: true,
							text: '您尚未注册或登录！',
							button: '现在登录',
							to: 'login'
						}
					}
					break;
				case 'realname':
					if(!identity.isLogin){
						config = {
							jump: true,
							text: '您尚未注册或登录！',
							button: '现在登录',
							to: 'login'
						}
					}else{
						if(!identity.isRealname){
							config = {
								jump: true,
								text: '您尚未进行实名认证！',
								button: '现在申请',
								to: 'app.realname'
							}
						}else{
							if(user.is_realname == 1){
								config = {
									jump: true,
									text: '您的实名认证正在审核中！',
									button: '等待审核',
									to: 'app.my'
								}
							}
						}
					}
					break;
				case 'list':
					config = { jump: false};
			}
			if(config.jump){
				if(alert){
					ngSwal.confirm({text: config.text, type: "warning", confirmButtonText: config.button, cancelButtonText: "立即注册"},
						function(){
							$state.go(config.to, {}, {reload: true});
						}, function(){
							$state.go(configs.state.register, {}, { reload: true });
						});
				}else{
					if(typeof(callback) == 'function') callback(user, identity);
				}
			}else{
				if(typeof(callback) == 'function') callback(user, identity);
			}
		},
		getStorage: function(){
			var localStorage = {};
			var sessionStorage = {};
			var checkData = {};
			for(var p in $window.localStorage){
				localStorage[p] = JSON.parse($window.localStorage.getItem(p));
				if(unit.isEmptyObject(localStorage[p].md5) && unit.isEmptyObject(localStorage[p].sha1))
					checkData[p] = {md5: localStorage[p].md5, sha1: localStorage[p].sha1};
			}
			for(var p in $window.sessionStorage){
				sessionStorage[p] = JSON.parse($window.sessionStorage.getItem(p));
				if(unit.isEmptyObject(sessionStorage[p].md5) && unit.isEmptyObject(sessionStorage[p].sha1))
					checkData[p] = {md5: sessionStorage[p].md5, sha1: sessionStorage[p].sha1};
			}
			if(unit.isEmptyObject(checkData)){
				$http({
					url: url,
					cache: false,
					timeout: 30000,
					method: 'get',
					params: {action: 'Restful', module: 'getCacheData', data: checkData}
				}).success(function(resp){
					if(resp.type == 'Success'){
						if(!unit.isEmptyObject(resp.sessionStorage)){
							for (var p in resp.sessionStorage) {
								if(typeof resp.sessionStorage[p] == 'string' && resp.sessionStorage[p] == 'empty'){
									$window.sessionStorage.removeItem(p);
								}else {
									var local = resp.sessionStorage[p];
									$window.sessionStorage.setItem(p, (typeof local == 'object') ? JSON.stringify(local) : local);
								}
							}
						}
						if(!unit.isEmptyObject(resp.localStorage)){
							for (var p in resp.localStorage) {
								var local = resp.localStorage[p];
								$window.localStorage.setItem(p, (typeof local == 'object') ? JSON.stringify(local) : local);
							}
						}

					}
				});
			}
			deferred.resolve(localStorage, sessionStorage);
			return deferred.promise;
		},
		setInitialRun: function (initial) {
			$window.localStorage.setItem("initialRun", (initial ? "true" : "false"));
		},
		isInitialRun: function () {
			var value = $window.localStorage.getItem("initialRun") || "true";
			return value == "true";
		},
		isSetPassword: function(){
			return unit.isEmpty($window.localStorage.getItem("app_password"));
		},
		getLastTimestamp: function(){
			return parseInt($window.localStorage.getItem('lastTimestamp'));
		},
		setLastTimestamp: function(){
			$window.localStorage.setItem('lastTimestamp', new Date().getTime());
		},
		isLockScreen: function(callback){
			if(this.isSetPassword){
				$state.go('app.appset_password')
			}else{
				if(new Date().getTime() > (this.getLastTimestamp() + configs.timeout))
					$state.go('app.lock_screen');
				else
				if(typeof callback == 'function') callback();
			}
		}
	}
});