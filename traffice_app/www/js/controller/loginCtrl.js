/**
 * Created by gadflybsd on 2017/5/15.
 */
angular.module('starter').controller('loginCtrl', function($scope, $state, unit, service, configs) {
	"手机App用户登录";
	//service.userState('notLogin', function(user, identity){
		$scope.login = {}
		//console.log(user, identity);
		$scope.submit = function(){
			service.restful('post', {model: configs.model.user, module: 'login', data: $scope.login}, function(result){
				console.log(result);
				$state.go(configs.state.scout, {}, { reload: true });
			});
		}
	//}, true);
});